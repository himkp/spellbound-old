//
//  CCTextField.h
//  
//
//  Created by Ignacio Inglese on 12/27/11.
//  Copyright 2011 Ignacio Inglese. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class CCTextField;

@protocol CCTextFieldDelegate <NSObject>

- (void)textFieldDidReturn:(CCTextField *)textField;
- (BOOL)textField:(CCTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

@interface CCTextField : CCNode <UITextFieldDelegate, CCTargetedTouchDelegate> {
	BOOL showingTicker;
	BOOL hidingTicker;
	BOOL sharedTextField;
	
	NSString * realString;
}

@property (nonatomic, assign) id<CCTextFieldDelegate> delegate;
@property (nonatomic, assign) UITextField * textField;
@property (nonatomic, assign) CCLabelBMFont *label;
@property (nonatomic, assign) NSUInteger maxLength;
@property (nonatomic, assign) BOOL password;
@property (nonatomic, assign) BOOL debugMode;
@property (nonatomic, assign) NSString * text;
@property (nonatomic, copy) void (^onReturn) (void);

+ (id)textFieldWithLabel:(CCLabelBMFont *)label andTextField:(UITextField *)textfield;
- (id)initWithLabel:(CCLabelBMFont *)label andTextField:(UITextField *)textfield;

- (void)setTextColor:(ccColor3B)color;
- (void)showKeyboard;

@end
