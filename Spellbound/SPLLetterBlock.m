//
//  SPLLetterBlock.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLLetterBlock.h"
#import "SPLTiledMap.h"
#import "SPLGameCharacter.h"

@implementation SPLLetterBlock

@synthesize isConnectedToACliffBlock = _isConnectedToACliffBlock;

-(id)initWithSprite:(SPLLetterBlockSprite *)letterSprite onMap:(SPLTiledMap*)tiledMap atLocation:(CGPoint)location
{
    if (self = [super init])
    {
        _letterSprite = [letterSprite retain];
        _letter = [[letterSprite.letter uppercaseString] retain];
        _isFrozen = NO;
        _tiledMap = tiledMap;
        _location = location;
        _isConnectedToACliffBlock = FLMaybe;
        _areAdjacentBlocksConnectedToACliffBlock = FLMaybe;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    SPLLetterBlockSprite *letterSprite = [decoder decodeObjectForKey:@"letterSprite"];
    SPLTiledMap *tiledMap = [decoder decodeObjectForKey:@"tiledMap"];
    CGPoint location = [decoder decodeCGPointForKey:@"location"];
    
    if (self = [self initWithSprite:letterSprite onMap:tiledMap atLocation:location])
    {
        _isFrozen = [decoder decodeBoolForKey:@"isFrozen"];
        _isConnectedToACliffBlock = (FLUncertainBoolean) [decoder decodeIntForKey:@"isConnectedToACliffBlock"];
        _areAdjacentBlocksConnectedToACliffBlock = (FLUncertainBoolean) [decoder decodeIntForKey:@"areAdjacentBlocksConnectedToACliffBlock"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_letterSprite forKey:@"letterSprite"];
    [encoder encodeBool:_isFrozen forKey:@"isFrozen"];
    // lets just hope cyclic encoding works
    [encoder encodeObject:_tiledMap forKey:@"tiledMap"];
    [encoder encodeCGPoint:_location forKey:@"location"];
    [encoder encodeInt:_isConnectedToACliffBlock forKey:@"isConnectedToACliffBlock"];
    [encoder encodeInt:_areAdjacentBlocksConnectedToACliffBlock forKey:@"areAdjacentBlocksConnectedToACliffBlock"];
}

-(FLUncertainBoolean)isConnectedToACliffBlock
{
    if (_isConnectedToACliffBlock == FLYes || _isConnectedToACliffBlock == FLNo)
        return _isConnectedToACliffBlock;
    
    if (self.tileTypeOfAdjacentBlockToTheBottom == SPLTileTypeCliff ||
        self.tileTypeOfAdjacentBlockToTheBottom == SPLTileTypeLetterFrozen)
        return _isConnectedToACliffBlock = FLYes;
    if (self.tileTypeOfAdjacentBlockToTheLeft == SPLTileTypeCliff ||
        self.tileTypeOfAdjacentBlockToTheLeft == SPLTileTypeLetterFrozen)
        return _isConnectedToACliffBlock = FLYes;
    if (self.tileTypeOfAdjacentBlockToTheRight == SPLTileTypeCliff ||
        self.tileTypeOfAdjacentBlockToTheRight == SPLTileTypeLetterFrozen)
        return _isConnectedToACliffBlock = FLYes;
    if (self.tileTypeOfAdjacentBlockToTheTop == SPLTileTypeCliff ||
        self.tileTypeOfAdjacentBlockToTheTop == SPLTileTypeLetterFrozen)
        return _isConnectedToACliffBlock = FLYes;
    
    if ((self.tileTypeOfAdjacentBlockToTheLeft == SPLTileTypeWater ||
         self.tileTypeOfAdjacentBlockToTheLeft == SPLTileTypeLand ||
         self.tileTypeOfAdjacentBlockToTheLeft == SPLTileTypeObstruction) &&
        (self.tileTypeOfAdjacentBlockToTheRight == SPLTileTypeWater ||
         self.tileTypeOfAdjacentBlockToTheRight == SPLTileTypeLand ||
         self.tileTypeOfAdjacentBlockToTheRight == SPLTileTypeObstruction) &&
        (self.tileTypeOfAdjacentBlockToTheTop == SPLTileTypeWater ||
         self.tileTypeOfAdjacentBlockToTheTop == SPLTileTypeLand ||
         self.tileTypeOfAdjacentBlockToTheTop == SPLTileTypeObstruction) &&
        (self.tileTypeOfAdjacentBlockToTheBottom == SPLTileTypeWater ||
         self.tileTypeOfAdjacentBlockToTheBottom == SPLTileTypeLand ||
         self.tileTypeOfAdjacentBlockToTheBottom == SPLTileTypeObstruction))
        return _isConnectedToACliffBlock = FLNo;
    
    return _isConnectedToACliffBlock = FLMaybe;
}

-(void)setIsConnectedToACliffBlock:(FLUncertainBoolean)isConnectedToACliffBlock
{
    if (isConnectedToACliffBlock == FLMaybe)
    {
        _isConnectedToACliffBlock = FLMaybe;
        return;
    }
    if (_isConnectedToACliffBlock != isConnectedToACliffBlock)
    {
        _isConnectedToACliffBlock = FLYes;
        self.areAdjacentBlocksConnectedToACliffBlock = FLYes;
    }
}

-(void)setAreAdjacentBlocksConnectedToACliffBlock:(FLUncertainBoolean)areAdjacentBlocksConnectedToACliffBlock
{
    if (self.letterBlockToTheLeft != nil)
        self.letterBlockToTheLeft.isConnectedToACliffBlock = areAdjacentBlocksConnectedToACliffBlock;
    if (self.letterBlockToTheRight != nil)
        self.letterBlockToTheRight.isConnectedToACliffBlock = areAdjacentBlocksConnectedToACliffBlock;
    if (self.letterBlockToTheTop != nil)
        self.letterBlockToTheTop.isConnectedToACliffBlock = areAdjacentBlocksConnectedToACliffBlock;
    if (self.letterBlockToTheBottom != nil)
        self.letterBlockToTheBottom.isConnectedToACliffBlock = areAdjacentBlocksConnectedToACliffBlock;
}

-(SPLTileType)tileTypeOfAdjacentBlockToTheTop
{
    tileTypeOfAdjacentBlockToTheTop = [_tiledMap tileTypeAt:ccpAdd(_location, ccp(0, -1))];
    
    NSString *tileDescription = [_tiledMap tileDescriptionAt:ccpAdd(_location, ccp(0, -1))].name;
    if ([tileDescription rangeOfString:@"Cliff"].location != NSNotFound) {
        if ([_tiledMap.mainCharacter.cliffsSteppedOn containsObject:tileDescription])
            return tileTypeOfAdjacentBlockToTheTop;
        else return tileTypeOfAdjacentBlockToTheTop = SPLTileTypeObstruction;
    }
    return tileTypeOfAdjacentBlockToTheTop;
}

-(SPLTileType)tileTypeOfAdjacentBlockToTheBottom
{
    tileTypeOfAdjacentBlockToTheBottom = [_tiledMap tileTypeAt:ccpAdd(_location, ccp(0, 1))];
    
    NSString *tileDescription = [_tiledMap tileDescriptionAt:ccpAdd(_location, ccp(0, 1))].name;
    if ([tileDescription rangeOfString:@"Cliff"].location != NSNotFound) {
        if ([_tiledMap.mainCharacter.cliffsSteppedOn containsObject:tileDescription])
            return tileTypeOfAdjacentBlockToTheBottom;
        else return tileTypeOfAdjacentBlockToTheBottom = SPLTileTypeObstruction;
    }
    return tileTypeOfAdjacentBlockToTheBottom;
}

-(SPLTileType)tileTypeOfAdjacentBlockToTheLeft
{
    tileTypeOfAdjacentBlockToTheLeft = [_tiledMap tileTypeAt:ccpAdd(_location, ccp(-1, 0))];
    
    NSString *tileDescription = [_tiledMap tileDescriptionAt:ccpAdd(_location, ccp(-1, 0))].name;
    if ([tileDescription rangeOfString:@"Cliff"].location != NSNotFound) {
        if ([_tiledMap.mainCharacter.cliffsSteppedOn containsObject:tileDescription])
            return tileTypeOfAdjacentBlockToTheLeft;
        else return tileTypeOfAdjacentBlockToTheLeft = SPLTileTypeObstruction;
    }
    return tileTypeOfAdjacentBlockToTheLeft;
}

-(SPLTileType)tileTypeOfAdjacentBlockToTheRight
{
    tileTypeOfAdjacentBlockToTheRight = [_tiledMap tileTypeAt:ccpAdd(_location, ccp(1, 0))];
    
    NSString *tileDescription = [_tiledMap tileDescriptionAt:ccpAdd(_location, ccp(1, 0))].name;
    if ([tileDescription rangeOfString:@"Cliff"].location != NSNotFound) {
        if ([_tiledMap.mainCharacter.cliffsSteppedOn containsObject:tileDescription])
            return tileTypeOfAdjacentBlockToTheRight;
        else return tileTypeOfAdjacentBlockToTheRight = SPLTileTypeObstruction;
    }
    return tileTypeOfAdjacentBlockToTheRight;
}

-(SPLLetterBlock *)letterBlockToTheLeft
{
    return [_tiledMap getLetterBlockAtPoint:ccp(_location.x - 1, _location.y)];
}

-(SPLLetterBlock *)letterBlockToTheRight
{
    return [_tiledMap getLetterBlockAtPoint:ccp(_location.x + 1, _location.y)];
}

-(SPLLetterBlock *)letterBlockToTheTop
{
    return [_tiledMap getLetterBlockAtPoint:ccp(_location.x, _location.y - 1)];
}

-(SPLLetterBlock *)letterBlockToTheBottom
{
    return [_tiledMap getLetterBlockAtPoint:ccp(_location.x, _location.y + 1)];
}

-(NSString *)verticalWord
{
    //if (verticalWord)
        //return verticalWord;
    
    NSString* word = _letter;
    
    SPLLetterBlock *letterBlock = self;
    while ((letterBlock = letterBlock.letterBlockToTheTop))
        word = [letterBlock.letter stringByAppendingString:word];
    letterBlock = self;
    while ((letterBlock = letterBlock.letterBlockToTheBottom))
        word = [word stringByAppendingString:letterBlock.letter];
    
    return word; //verticalWord = word.retain;
}

-(NSString *)horizontalWord
{
    //if (horizontalWord)
        //return horizontalWord;
    
    NSString* word = _letter;
    
    SPLLetterBlock *letterBlock = self;
    while ((letterBlock = letterBlock.letterBlockToTheLeft))
        word = [letterBlock.letter stringByAppendingString:word];
    letterBlock = self;
    while ((letterBlock = letterBlock.letterBlockToTheRight))
        word = [word stringByAppendingString:letterBlock.letter];
    
    return word; //horizontalWord = word.retain;
}

+(id)letterBlockWithSprite:(SPLLetterBlockSprite *)letterSprite onMap:(SPLTiledMap *)tiledMap atLocation:(CGPoint)location
{
    return [[[self alloc] initWithSprite:letterSprite onMap:tiledMap atLocation:location] autorelease];
}

-(void)dealloc
{
    if (verticalWord) [verticalWord release];
    if (horizontalWord) [horizontalWord release];
    [_letter release];
    [_letterSprite release];
    [super dealloc];
}

@end