//
//  SPLLetterBlockSpriteFactory.h
//  Spellbound
//
//  Created by Fleon Games on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

typedef enum {
    SPLLetterBlockSpriteBlockStyleSolid = 1,
    SPLLetterBlockSpriteBlockStyleCracked1 = 0,
    SPLLetterBlockSpriteBlockStyleCracked2 = 2,
    SPLLetterBlockSpriteBlockStyleCracked3 = 3,
    SPLLetterBlockSpriteBlockStyleCracked4 = 4,
    SPLLetterBlockSpriteBlockStyleCracked5 = 5,
} SPLLetterBlockSpriteBlockStyle;

@interface SPLLetterBlockSprite : CCTMXTiledMap <NSCoding>
{
    SPLLetterBlockSpriteBlockStyle letterBlockStyle;
}

-(id)initWithLetter:(NSString *)letter;
-(id)initWithLetter:(NSString *)letter andBlockStyle:(SPLLetterBlockSpriteBlockStyle)blockStyle;

+(id)letterBlockSpriteWithLetter:(NSString *)letter;
+(id)letterBlockSpriteWithLetter:(NSString *)letter andBlockStyle:(SPLLetterBlockSpriteBlockStyle)blockStyle;

@property (nonatomic, readonly) NSString* letter;
@property (nonatomic) NSUInteger indexInLetterPile;
@property (nonatomic, readonly) NSArray* arrayOfChildSprites;

@end
