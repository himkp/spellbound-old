//
//  FLSimpleAudioPlayer.m
//  Spellbound
//
//  Created by Fleon Games on 6/13/13.
//
//

#import "FLSimpleAudioPlayer.h"
#import "Spellbound.h"
#import <AVFoundation/AVFoundation.h>

@implementation AVAudioPlayer (FLSimpleAudioPlayer)

-(void)_setVolumeAsNSNumber:(NSNumber *)volume
{
    [self setVolume:volume.floatValue];
}

-(void)fadeIn:(NSTimeInterval)seconds
{
    float volume = 0;
    [self setVolume:0];
    [self play];
    for (NSTimeInterval i = seconds / 100.0; i < seconds; i += seconds / 100.0)
    {
        volume += [Spellbound audioPlayer].volume / 100.0 / 100.0;
        [self performSelector:@selector(_setVolumeAsNSNumber:) withObject:@(volume) afterDelay:i];
    }
}

-(void)fadeOut:(NSTimeInterval)seconds
{
    float volume = [Spellbound audioPlayer].volume / 100.0;
    for (NSTimeInterval i = seconds / 100.0; i < seconds; i += seconds / 100.0)
    {
        volume -= [Spellbound audioPlayer].volume / 100.0 / 100.0;
        [self performSelector:@selector(_setVolumeAsNSNumber:) withObject:@(volume) afterDelay:i];
    }
    [self performSelector:@selector(stop) withObject:nil afterDelay:seconds + seconds / 100];
}

-(void)fadeTo:(float)newVolume duration:(NSTimeInterval)seconds
{
    float volume = [Spellbound audioPlayer].volume / 100.0;
    for (NSTimeInterval i = seconds / 100.0; i < seconds; i += seconds / 100.0)
    {
        volume += (newVolume - [Spellbound audioPlayer].volume / 100.0) / 100.0;
        [self performSelector:@selector(_setVolumeAsNSNumber:) withObject:@(volume) afterDelay:i];
    }
}

@end

@implementation FLSimpleAudioPlayer

-(void)_releasePreviousPlayer
{
    [audioPlayers removeObjectAtIndex:0];
}

-(void)playTrack:(NSString *)trackFilename
{
    if (!audioPlayers) audioPlayers = [[NSMutableArray alloc] init];
    NSUInteger c = audioPlayers.count;
    if (c) {
        AVAudioPlayer *prevPlayer = audioPlayers[c - 1];
        [prevPlayer fadeOut:3];
        [self performSelector:@selector(_releasePreviousPlayer) withObject:nil afterDelay:3.5];
    }
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], trackFilename]];
    AVAudioPlayer *player = [[[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil] autorelease];
    player.numberOfLoops = -1;
    audioPlayers[c] = player;
    if (_nowPlaying) [_nowPlaying release];
    _nowPlaying = [trackFilename retain];
    [player prepareToPlay];
    [player fadeIn:3];
}

-(void)stop
{
    [_nowPlaying release];
    _nowPlaying = nil;
    NSUInteger c = audioPlayers.count;
    if (!c) return;
    AVAudioPlayer *prevPlayer = audioPlayers[c - 1];
    [prevPlayer fadeOut:3];
    [self performSelector:@selector(_releasePreviousPlayer) withObject:nil afterDelay:3.5];
}

-(NSUInteger)volume
{
    NSString *currentVolumeString = [[Spellbound localStorage] dataForKey:@"CurrentVolume"];
    NSInteger currentVolume;
    currentVolume = currentVolumeString ? currentVolumeString.integerValue : 100;
    if (currentVolume < 0) currentVolume = 0;
    if (currentVolume > 100) currentVolume = 100;
    return currentVolume;
}

-(void)setVolume:(NSUInteger)volume
{
    [[Spellbound localStorage] setData:@(volume).stringValue forKey:@"CurrentVolume"];
    for (AVAudioPlayer *player in audioPlayers) {
        [player fadeTo:volume / 100.0 duration:1];
    }
}

-(void)dealloc
{
    [audioPlayers release];
    [_nowPlaying release];
    [super dealloc];
}

@end