//
//  SPLGlobalActionDelegate.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGlobalActionDelegate.h"
#import "FLEvent.h"
#import "FLYAML.h"
#import "Spellbound.h"
#import "FLEventAggregator.h"

@implementation SPLGlobalActionDelegate

+(id)globalActionDelegate
{
    return [[[self alloc] init] autorelease];
}

- (id)init
{
    self = [super init];
    if (self) {
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"Root"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"Root"];
    }
    return self;
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    [super dealloc];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_ConditionCheck])
    {
        NSString* condition = (NSString*) action.data;
        [condition componentsSeparatedByString:@"="];
        
        // this is incomplete
    }
    else if ([action is:Action_WaitFor])
    {
        NSString *timeToWaitFor = [(NSString *) action.data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSTimeInterval time;
        
        // if unit is ms
        if ([[timeToWaitFor substringFromIndex:timeToWaitFor.length - 2] isEqualToString:@"ms"])
            time = [timeToWaitFor substringToIndex:timeToWaitFor.length - 2].doubleValue / 1000;
        // if the unit is s
        else if ([timeToWaitFor characterAtIndex:timeToWaitFor.length - 1] == 's')
            time = [timeToWaitFor substringToIndex:timeToWaitFor.length - 1].doubleValue;
        else
            time = timeToWaitFor.doubleValue;
        
        [action willFinishAfter:time];
    }
    else if ([action is:Action_WaitUntil] || [action is:Action_WaitUntilEither]) // WaitUntil, WaitUntilEither
    {
        if ([action.data isKindOfClass:[NSString class]])
        {
            id eventData = nil;
            FLEvent *theEvent = [[FLEventAggregator globalEventAggregator] eventWithParameterizedName:action.data eventData:&eventData];
            if (theEvent) [theEvent bindToActionAndUnbindOnDispatch:action.subsequentAction andDispatchOnlyIfDataEquals:eventData];
        }
        else if ([action.data isKindOfClass:[NSArray class]])
        {
            NSArray *yamlEventList = (NSArray *) action.data;
            FLAction *runCounterAction = [FLAction actionWithActionDelegate:action.actionDelegate andName:@"DummyAction"];
            runCounterAction.subsequentAction = action.subsequentAction;
            action.subsequentAction = runCounterAction;
            
            NSUInteger eventCount = 0;
            
            NSMutableDictionary *eventActionBindings = [NSMutableDictionary dictionary];
            
            for (id event in yamlEventList)
            {
                if ([event isKindOfClass:[NSString class]])
                {
                    id eventData = nil;
                    FLEvent *theEvent = [[FLEventAggregator globalEventAggregator] eventWithParameterizedName:event
                                                                                                    eventData:&eventData];
                    if (theEvent)
                    {
                        eventCount ++;
                        [theEvent bindToActionAndUnbindOnDispatch:runCounterAction
                                      andDispatchOnlyIfDataEquals:eventData];
                    }
                }
                else if ([event isKindOfClass:[NSDictionary class]])
                {
                    NSString* eventName = (NSString*) [[event keyEnumerator] nextObject];
                    NSArray* actionList = (NSArray*) [[event objectEnumerator] nextObject];
                    
                    FLAction* firstAction = [FLYAML linkActionSequence:actionList withActionDelegate:action.actionDelegate];
                    firstAction.priorAction = action;
                    id eventData = nil;
                    FLEvent* theEvent = [[FLEventAggregator globalEventAggregator] eventWithParameterizedName:eventName
                                                                                                    eventData:&eventData];
                    
                    if (theEvent)
                    {
                        eventCount ++;
                        NSLog(@"Event %d: [%@] bound to action [%@] and runCounter [%@]", eventCount, theEvent.name, firstAction.name, runCounterAction.name);
                        [theEvent bindToActionAndUnbindOnDispatch:firstAction
                                      andDispatchOnlyIfDataEquals:eventData];
                        [theEvent bindToActionAndUnbindOnDispatch:runCounterAction
                                      andDispatchOnlyIfDataEquals:eventData];
                        
                        eventActionBindings[eventName] = firstAction;
                    }
                }
            }
            
            if ([action is:Action_WaitUntilEither]) eventCount = 1;
            [runCounterAction willFinishAfterRunCountReaches:eventCount];
            
            runCounterAction.onRun = ^ {
                NSLog(@"runCounterAction.onRun() called; runCount: %d", runCounterAction.runCount);
            };
            
            runCounterAction.onFinish = ^ {
                NSLog(@"runCounterAction.onFinish() called");
                
                for (NSString *eventName in eventActionBindings.keyEnumerator)
                {
                    FLEvent* theEvent = [[FLEventAggregator globalEventAggregator] eventWithParameterizedName:eventName eventData:nil];
                    FLAction* theAction = eventActionBindings[eventName];
                    [theEvent unbindFromAction:theAction];
                    NSLog(@"--- Event [%@] is no longer bound to Action [%@]", theEvent.name, theAction.name);
                }
                
                runCounterAction.onFinish = ^{};
            };
        }
    }
    else if ([action is:Action_Goto] || [action is:Action_Play])
    {
        FLActionSequence *actionSequence = [[Spellbound actionSequenceManager] actionSequenceWithName:action.data];
        
        if ([action is:Action_Play])
            actionSequence.subsequentAction = action.subsequentAction;
        
        actionSequence.priorAction = action;
        [actionSequence run];
    }
    else if ([action is:Action_Set])
    {
        NSArray *data = [action.data componentsSeparatedByString:@"="];
        NSString *key = [data[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *value = [data[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [[Spellbound localStorage] setData:value forKey:key];
        
        // TODO: this needs to go in local storage for persistance
        [[Spellbound variableList] addObject:key];
        NSLog(@"%@ is %@", key, [[Spellbound localStorage] dataForKey:key]);
        [action finish];
    }
    else if ([action is:Action_With])
    {
        id<FLActionDelegate> actionDelegate = [[Spellbound actionDelegateManager] actionDelegateWithName:action.data[@"params"][0]];
        NSArray* actionList = (NSArray*) action.data[@"data"];
        FLAction* firstAction = [FLYAML linkActionSequence:actionList withActionDelegate:actionDelegate];
        firstAction.priorAction = action;
        FLActionSequence *actionSequence = [FLActionSequence actionSequenceWithActionDelegate:actionDelegate
                                                                               andFirstAction:firstAction
                                                                                      andName:@"WithActionSequence"];
        
        actionSequence.onFinish = ^ {
            [action finish];
        };
        actionSequence.subsequentAction = action.subsequentAction;
        actionSequence.priorAction = action;
        [actionSequence run];
    }
    else if ([action is:Action_PlayMusic])
    {
        NSLog(@"playing track: %@", action.data);
        [[Spellbound audioPlayer] playTrack:[action.data stringByAppendingString:@".mp3"]];
        NSLog(@"started playing track");
        [action finish];
    }
}

@end
