//
//  SPLEnvironmentLayer.m
//  Spellbound
//
//  Created by Fleon Games on 6/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLEnvironmentLayer.h"

@implementation SPLEnvironmentLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfEmitterSnow   = fl_ccp(1024,    0,    0,    0);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfEmitterSnow   = fl_ccp( 480,    0,    0,    0);
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initializePositions];
        
        spriteEmitterSnow = [CCParticleSnow node];
        
        [self addChild:spriteEmitterSnow z:99];
        
        spriteEmitterSnow.position = positionOfEmitterSnow;
        spriteEmitterSnow.life = 6;
        spriteEmitterSnow.lifeVar = 1;
        
        spriteEmitterSnow.gravity = ccp(0, -10);
        spriteEmitterSnow.speed = DEVICE_IS_IPAD ? 70 : 32;
        spriteEmitterSnow.speedVar = DEVICE_IS_IPAD ? 30 : 14;
        
        ccColor4F startColor = spriteEmitterSnow.startColor;
        startColor.r = 0.9f;
        startColor.g = 0.9f;
        startColor.b = 0.9f;
        spriteEmitterSnow.startColor = startColor;
        
        ccColor4F startColorVar = spriteEmitterSnow.startColorVar;
        startColorVar.b = 0.1f;
        spriteEmitterSnow.startColorVar = startColorVar;
        
        spriteEmitterSnow.emissionRate = spriteEmitterSnow.totalParticles / spriteEmitterSnow.life / 8;
        
        spriteEmitterSnow.texture = [[CCTextureCache sharedTextureCache] addImage:@"Assets/Common/Snow.png"];
        
    }
    return self;
}

@end
