//
//  NSString+Scrambler.m
//  Spellbound
//
//  Created by Fleon Games on 6/15/13.
//
//

#import "NSString+Spellbound.h"
#import "Spellbound.h"

@implementation NSString (Spellbound)

- (NSString *)scramble{
    NSString *toScramble = self;
    for (int i = 0; i < [toScramble length] * 15; i ++) {
        int pos = arc4random() % [toScramble length];
        int pos2 = arc4random() % ([toScramble length] - 1);
        char ch = [toScramble characterAtIndex:pos];
        NSString *before = [toScramble substringToIndex:pos];
        NSString *after = [toScramble substringFromIndex:pos + 1];
        NSString *temp = [before stringByAppendingString:after];
        before = [temp substringToIndex:pos2];
        after = [temp substringFromIndex:pos2];
        toScramble = [before stringByAppendingFormat:@"%c%@", ch, after];
    }
    return toScramble;
}

-(NSUInteger)pointValue
{
    if (self.length > 1) {
        NSUInteger pointValue = 0;
        for (int i = 0; i < self.length; i++) {
            pointValue += [self substringWithRange:NSMakeRange(i, 1)].pointValue;
        }
        return pointValue;
    }
    
    BOOL (^ isLetter) (NSString *) = ^ BOOL (NSString *l)
    {
        return ([self isEqualToString:l]);
    };
    
    if (isLetter(@"A") || isLetter(@"E") || isLetter(@"I") || isLetter(@"O") || isLetter(@"S") ||
        isLetter(@"N") || isLetter(@"R") || isLetter(@"T") || isLetter(@"L") || isLetter(@"U"))
        return 1;
    else if (isLetter(@"D") || isLetter(@"G"))
        return 2;
    else if (isLetter(@"B") || isLetter(@"C") || isLetter(@"M") || isLetter(@"P"))
        return 3;
    else if (isLetter(@"F") || isLetter(@"H") || isLetter(@"W") || isLetter(@"Y") || isLetter(@"V"))
        return 4;
    else if (isLetter(@"K"))
        return 5;
    else if (isLetter(@"J") || isLetter(@"X"))
        return 8;
    else if (isLetter(@"Q") || isLetter(@"Z"))
        return 10;
    
    return 0;
}

-(NSString *)stringByReplacingVariables
{
    NSString *string = self;
    for (NSString *key in [Spellbound variableList])
    {
        NSString *value = [[Spellbound localStorage] dataForKey:key];
        if (!value) continue;
        string = [string stringByReplacingOccurrencesOfString:key withString:value];
    }
    return string;
}

@end
