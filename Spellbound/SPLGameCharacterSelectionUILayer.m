//
//  SPLGameCharacterSelectionUILayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGameCharacterSelectionUILayer.h"
#import "SPLGameCharacterSelectionScene.h"
#import "SimpleAudioEngine.h"
#import "CCTextField.h"

@implementation SPLGameCharacterSelectionUILayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfBackground            = fl_ccp(   0,    0, 2048, 1536);
        
        positionOfDrCyrus               = fl_ccp( 139,   38,  554,  939);
        positionOfElric                 = fl_ccp(1379,  274,  552,  703);
        positionOfAmy                   = fl_ccp( 797,  274,  550,  703);
        positionOfAmyGlow               = fl_ccp( 832,  274,  484,  648);
        positionOfElricGlow             = fl_ccp(1405,  274,  484,  648);
        positionOfDrCyrusIntially       = fl_ccp( 747,   38,  554,  939);
        positionOfAmyInitially          = fl_ccp(2048,  274,  550,  703);
        positionOfElricInitially        = fl_ccp(2048,  274,  552,  703);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfBackground            = fl_ccp(   0,    0,   960,  640);
        
        positionOfDrCyrus               = fl_ccp(  75,    0,  223,  387);
        positionOfElric                 = fl_ccp( 692,  106,  224,  283);
        positionOfElricGlow             = fl_ccp( 702,  106,  195,  261);
        positionOfAmy                   = fl_ccp( 427,  106,  222,  283);
        positionOfAmyGlow               = fl_ccp( 441,  106,  195,  261);
        positionOfDrCyrusIntially       = fl_ccp( 369,    0,  223,  387);
        positionOfAmyInitially          = fl_ccp( 960,  106,  222,  283);
        positionOfElricInitially        = fl_ccp( 960,  106,  224,  283);
    }
}

- (id)init {
    self = [super init];
    if (self) {
        [self initializePositions];
        
        self.isTouchEnabled = YES;
        
        spriteBackground = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_Background.png"];
        spriteBackground.position = positionOfBackground;
        [self addChild:spriteBackground];
        
        spriteDrCyrus = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_DrCyrus.png"];
        spriteDrCyrus.position = positionOfDrCyrusIntially;
        
        spriteAmy = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_Amy.png"];
        spriteAmy.position = positionOfAmyInitially;
        
        spriteElric = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_Elric.png"];
        spriteElric.position = positionOfElricInitially;
        
        spriteDrCyrus.visible = NO;
        
        spriteElricGlow = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_Glow.png"];
        spriteElricGlow.position = positionOfElricGlow;
        spriteElricGlow.visible = NO;
        
        spriteAmyGlow = [CCSprite spriteWithSpriteFrameName:@"CharacterSelection_Glow.png"];
        spriteAmyGlow.position = positionOfAmyGlow;
        spriteAmyGlow.visible = NO;
        
        [self addChild:spriteDrCyrus];
        [self addChild:spriteAmy];
        [self addChild:spriteElric];
        [self addChild:spriteAmyGlow];
        [self addChild:spriteElricGlow];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_ShowCharacterDrCyrus])
    {
        [spriteDrCyrus runAction:[CCSequence actionOne:[CCShow action] two:[CCFadeIn actionWithDuration:0.75]]];
        [action willFinishAfter:1];
    }
    else if ([action is:Action_IntroduceCharacters])
    {
        [spriteDrCyrus runAction:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:positionOfDrCyrus]]];
        
        FLAction* nextAction = [action.subsequentAction retain];
        
        action.subsequentAction = Action_ShowCharacterAmy;
        action.subsequentAction.subsequentAction = Action_ShowCharacterElric;
        action.subsequentAction.subsequentAction.subsequentAction = nextAction;
        
        [action willFinishAfter:0.5];
        [nextAction release];
    }
    else if ([action is:Action_ShowCharacterAmy])
    {
        [spriteAmy runAction:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:positionOfAmy]]];
        
        [action willFinishAfter:0.35];
    }
    else if ([action is:Action_ShowCharacterElric])
    {
        [spriteElric runAction:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:positionOfElric]]];
        
        [action willFinishAfter:0.35];
    }
    else if ([action is:Action_HideCharacters])
    {
        [spriteAmyGlow runAction:[CCFadeOut actionWithDuration:0.35]];
        [spriteElricGlow runAction:[CCFadeOut actionWithDuration:0.35]];
        
        [spriteElric runAction:
         [CCSequence actionOne:[CCDelayTime actionWithDuration:0.35] 
                           two:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:positionOfElricInitially]]]];
        
        [spriteAmy runAction:
         [CCSequence actionOne:[CCDelayTime actionWithDuration:0.70] 
                           two:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:positionOfAmyInitially]]]];
        
        [spriteDrCyrus runAction:
         [CCSequence actionOne:[CCDelayTime actionWithDuration:1.05]
                           two:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(-spriteDrCyrus.boundingBox.size.width, positionOfDrCyrus.y)]]]];
        
        [action willFinishAfter:1.6];
    }
    else if ([action is:Action_ShowCharacterAmyGlow])
    {
        spriteAmyGlow.opacity = 0;
        spriteAmyGlow.visible = YES;
        [spriteAmyGlow runAction:[CCFadeIn actionWithDuration:0.35]];
        
        [action finish];
    }
    else if ([action is:Action_HideCharacterAmyGlow])
    {
        [spriteAmyGlow runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.35]
                                                   two:[CCHide action]]];
        
        [action finish];
    }
    else if ([action is:Action_ShowCharacterElricGlow])
    {
        spriteElricGlow.opacity = 0;
        spriteElricGlow.visible = YES;
        [spriteElricGlow runAction:[CCFadeIn actionWithDuration:0.35]];
        
        [action finish];
    }
    else if ([action is:Action_HideCharacterElricGlow])
    {
        [spriteElricGlow runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.35]
                                                     two:[CCHide action]]];
        
        [action finish];
    }
    else if ([action is:Action_ShowTextInputForName])
    {
        characterNameInputTextField = [[UITextField alloc] initWithFrame:CGRectMake(2000, 200, 150, 50)];
        characterNameInputTextField.returnKeyType = UIReturnKeyDone;
        characterNameInputTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        characterNameInputTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
        [[[CCDirector sharedDirector] view] addSubview:characterNameInputTextField];
        CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"Elric"
                                                      fntFile:DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt"
                                                             :@"iPhone/Fonts/DoctorSoosBold.fnt"
                                                        width:DEVICE_IS_IPAD ? 550 : 257
                                                    alignment:UITextAlignmentLeft];
        label.anchorPoint = ccp(0, 0.5);
        CCTextField *tf = [CCTextField textFieldWithLabel:label
                                              andTextField:characterNameInputTextField];
        tf.maxLength = 10;
        
        [tf showKeyboard];
        [self.parent addChild:tf z:999];
        tf.position = ccpMult(ccp(660 + 230, 768 * 2 - 1348), 0.5);
        tf.anchorPoint = ccp(0, 1);
        tf.onReturn = ^{
            FLAction *setAction = Action_Set;
            setAction.data = [@"PlName = " stringByAppendingString:tf.text];
            setAction.actionDelegate = [Spellbound globalActionDelegate];
            // todo: PlName is done to avoid conflict with Player had this variable been PlayerName
            [setAction run];
            [tf text];
            [characterNameInputTextField removeFromSuperview];
            [tf removeFromParentAndCleanup:YES];
            [action finish];
        };
    }
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    NSLog(@"Event was passed on to the UI layer");
    
    if ([self visible])
    {
        if ([FL doesSprite:spriteAmy containPoint:point])
        {
            [[(SPLGameCharacterSelectionScene*) self.parent Event_FemaleCharacterWasTouched] dispatch];
            return YES;
        }
        else if ([FL doesSprite:spriteElric containPoint:point])
        {
            [[(SPLGameCharacterSelectionScene*) self.parent Event_MaleCharacterWasTouched] dispatch];
            return YES;
        }
    }
    return NO;
}

@end
