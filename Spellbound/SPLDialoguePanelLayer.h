//
//  SPLDialoguePanelLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@class SPLDialogueSequence;

@interface SPLDialoguePanelLayer : CCLayer <FLActionDelegate>
{
    CCSprite* spriteBackground, *spriteBackground2;
    
    CCSprite* spriteBorderTop;
    CCSprite* spriteBorderRight;
    CCSprite* spriteBorderLeft;
    CCSprite* spriteContinueArrow;
    
    CCSprite* spriteThePanel;
    
    CCLabelBMFont* spriteDialogueText;
    
    CCSprite* spriteAvatar;
    
    NSMutableString *currentDialogueString;
    NSMutableString *currentAvatarFilename;
    
    FLAction* currentAction;
    
    CGPoint positionOfBackground1;
    CGPoint positionOfBackground2;
    CGPoint positionOfBorderTop;
    CGPoint positionOfBorderRight;
    CGPoint positionOfBorderLeft;
    CGPoint positionOfContinueArrow;
    CGPoint positionOfAvatar;
    CGPoint positionOfDialogueText;
    CGPoint positionOfThePanel;
    
    BOOL isCurrentActionPaused;
}

-(void)initializePositions;

-(void) typewriterEffectForAction_DisplayFirstTwoLines:(ccTime)dt;
-(void) typewriterEffectForAction_DisplayNextLine:(ccTime)dt;
-(void) scrollEffectForAction_DisplayNextLine:(ccTime)dt;

-(void) show;
-(void) hide;

-(void) show: (void(^)()) onShow;
-(void) hide: (void(^)()) onHide;

-(BOOL) canHandleAction:(FLAction *)action;

// Events
@property (retain, nonatomic) FLEvent* Event_TouchDidHappen;

// Actions
#define Action_ShowDialogueWindow       FLActionMake(Action_ShowDialogueWindow)
#define Action_HideDialogueWindow       FLActionMake(Action_HideDialogueWindow)
#define Action_BeginDialogue            FLActionMake(Action_BeginDialogue)

// Internal Actions
#define Action_SetCharacterSprite       FLActionMake(Action_SetCharacterSprite)
#define Action_DisplayFirstTwoLines     FLActionMake(Action_DisplayFirstTwoLines)
#define Action_DisplayNextLine          FLActionMake(Action_DisplayNextLine)

@end
