//
//  SPLGameLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SPLTitleScene.h"

@implementation SPLTitleScene

-(id)init
{
    if (self=[super init])
    {
        // let us load the textures
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/MainScreen_Background.plist" : @"iPhone/MainScreen_Background.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/MainScreen_Textures.plist" : @"iPhone/MainScreen_Textures.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
        
        _primaryUILayer = [SPLTitleUILayer node];
        _startGameMenuLayer = [SPLStartGameMenuLayer node];
        _creditsPanelLayer = [SPLCreditsPanelLayer node];
        _settingsPanelLayer = [SPLSettingsPanelLayer node];
        
        [self addChild:_primaryUILayer];
        [self addChild:_startGameMenuLayer];
        [self addChild:_creditsPanelLayer];
        [self addChild:_settingsPanelLayer];
    }
    return self;
}

-(void)dealloc
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/MainScreen_Background.plist" : @"iPhone/MainScreen_Background.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/MainScreen_Textures.plist" : @"iPhone/MainScreen_Textures.plist"];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile: 
     DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
    
    [[CCDirector sharedDirector] purgeCachedData];
    
    [super dealloc];
}

@end
