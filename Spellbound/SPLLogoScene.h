//
//  HelloWorldLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/18/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


#import "Spellbound.h"

// HelloWorldLayer
@interface SPLLogoScene : CCScene
{
    BOOL shouldJumpDirectlyToGameScene;
    BOOL shouldSkipTitleScreenLogos;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(SPLLogoScene *) scene;

@end
