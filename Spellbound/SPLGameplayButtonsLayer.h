//
//  SPLGameplayButtonsLayer.h
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLGameplayButtonsLayer : CCLayer <FLActionDelegate, NSCoding>
{
    CCSprite* spriteTheButtons;
    
    CGPoint positionOfButtonMenu;
    CGPoint positionOfButtonCoins;
    CGPoint positionOfButtonItems;
    CGPoint positionOfCoinsCounter;
    
    CGPoint positionOfTheButtons;
}

@property (nonatomic, readonly) CCSprite *spriteButtonMenu;
@property (nonatomic, readonly) CCSprite *spriteButtonCoins;
@property (nonatomic, readonly) CCSprite *spriteButtonItems;
@property (nonatomic, readonly) CCLabelBMFont *spriteCoinsCounter;

-(void)initializePositions;
-(void)show;
-(void)hide;
-(void)show:(void(^)())onShow;
-(void)hide:(void(^)())onHide;

@property (nonatomic) NSUInteger coinCount;

// Events
#define Action_ShowGameplayButtons      FLActionMake(Action_ShowGameplayButtons)
#define Action_HideGameplayButtons      FLActionMake(Action_HideGameplayButtons)

@end
