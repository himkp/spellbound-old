//
//  FLLocalStorage.h
//  Spellbound
//
//  Created by Fleon Games on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface FLLocalStorage : NSObject {
@private
    FMDatabase *fmdb;
}

-(id)dataForKey:(NSString *)key;
-(BOOL)dataExistsForKey:(NSString *)key;
-(void)setData:(id)data forKey:(NSString *)key;
-(void)unsetDataForKey:(NSString *)key;

@end
