//
//  SPLLetterBlockSpriteFactory.m
//  Spellbound
//
//  Created by Fleon Games on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLLetterBlockSprite.h"
#import "NSData+Base64.h"

@implementation SPLLetterBlockSprite

+(id)letterBlockSpriteWithLetter:(NSString *)letter
{
    return [[[self alloc] initWithLetter:letter] autorelease];
}

+(id)letterBlockSpriteWithLetter:(NSString *)letter andBlockStyle:(SPLLetterBlockSpriteBlockStyle)blockStyle
{
    return [[[self alloc] initWithLetter:letter andBlockStyle:blockStyle] autorelease];
}

-(id)initWithLetter:(NSString *)letter
{
    return [self initWithLetter:letter andBlockStyle:(SPLLetterBlockSpriteBlockStyle) arc4random() % 5];
}

-(id)initWithLetter:(NSString *)letter andBlockStyle:(SPLLetterBlockSpriteBlockStyle)blockStyle
{
    NSUInteger tileDimensions = DEVICE_IS_IPAD ? 64 : 40;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY) 
        tileDimensions *= 2;
    
    CGSize screenSize = DEVICE_IS_IPAD ? CGSizeMake(1024, 768) : CGSizeMake(640, 480);
    if (DEVICE_SUPPORTS_RETINA_DISPLAY) 
        screenSize.width *= 2, 
        screenSize.height *= 2;
    
    NSMutableString *tilesetPath = [NSMutableString stringWithString:DEVICE_IS_IPAD ? @"iPad" : @"iPhone"];
    [tilesetPath appendString:@"/Maps/SpellboundTiles"];
    [tilesetPath appendString:DEVICE_SUPPORTS_RETINA_DISPLAY ? @"-hd.png" : @".png"];
    
    UInt32
        blockGID1 = 145 + blockStyle,
        blockGID2 = 161 + blockStyle;
    
    letter = [letter uppercaseString];
    UniChar letterCode = [letter characterAtIndex:0];
    UInt32 letterGID;
    
    if (letterCode >= 65 /* A */ && letterCode <= 72 /* H */)
        letterGID = 103 + letterCode - 65;
    else if (letterCode >= 73 /* I */ && letterCode <= 81 /* Q */)
        letterGID = 119 + letterCode - 73;
    else if (letterCode >= 82 /* R */ && letterCode <= 90 /* Z */)
        letterGID = 135 + letterCode - 82;
    
    NSMutableData *iceBlockLayer = [NSMutableData dataWithLength:8];
    NSMutableData *letterLayer = [NSMutableData dataWithLength:8];
    
    UInt32 zeroGID = 0;
    
    [iceBlockLayer setData:[NSData dataWithBytes:&blockGID1 length:sizeof(blockGID1)]];
    [iceBlockLayer appendBytes:&blockGID2 length:sizeof(blockGID2)];
    [letterLayer setData:[NSData dataWithBytes:&letterGID length:sizeof(letterGID)]];
    [letterLayer appendBytes:&zeroGID length:sizeof(zeroGID)];
    
    NSString *iceBlockLayerBase64 = [iceBlockLayer base64EncodedString];
    NSString *letterLayerBase64 = [letterLayer base64EncodedString];
    
    NSString* xmlString = [NSString stringWithFormat:
    @"<map version=\"1.0\" orientation=\"orthogonal\" width=\"1\" height=\"2\" tilewidth=\"%d\" tileheight=\"%d\">\
       <tileset firstgid=\"1\" name=\"SpellboundTiles\" tilewidth=\"%d\" tileheight=\"%d\">\
        <image source=\"SpellboundTiles.png\" width=\"%d\" height=\"%d\"/>\
       </tileset>\
       <layer name=\"ice block\" width=\"1\" height=\"2\">\
        <data encoding=\"base64\">\
         %@ \
        </data>\
       </layer>\
       <layer name=\"letter\" width=\"1\" height=\"2\">\
        <data encoding=\"base64\">\
         %@ \
        </data>\
       </layer>\
      </map>",
                           tileDimensions,
                           tileDimensions,
                           tileDimensions,
                           tileDimensions,
                           (int) screenSize.width, 
                           (int) screenSize.height,
                           iceBlockLayerBase64,
                           letterLayerBase64];
    
    if (self = [super initWithXML:xmlString resourcePath:[tilesetPath stringByDeletingLastPathComponent]])
    {
        _letter = [letter retain];
        
        _arrayOfChildSprites = [@[[[self layerNamed:@"ice block"] tileAt:ccp(0, 0)],
                               [[self layerNamed:@"ice block"] tileAt:ccp(0, 1)],
                               [[self layerNamed:@"letter"] tileAt:ccp(0, 0)]] retain];
        
        letterBlockStyle = blockStyle;
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    SPLLetterBlockSpriteBlockStyle blockStyle = (SPLLetterBlockSpriteBlockStyle) [decoder decodeIntForKey:@"blockStyle"];
    NSString *letter = [decoder decodeObjectForKey:@"letter"];
    if (self = [self initWithLetter:letter andBlockStyle:blockStyle]) {
        self.position = [decoder decodeCGPointForKey:@"position"];
        self.anchorPoint = [decoder decodeCGPointForKey:@"anchorPoint"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInt:letterBlockStyle forKey:@"blockStyle"];
    [encoder encodeCGPoint:position_ forKey:@"position"];
    [encoder encodeCGPoint:anchorPoint_ forKey:@"anchorPoint"];
    [encoder encodeObject:_letter forKey:@"letter"];
}

- (void)dealloc
{
    [_letter release];
    [_arrayOfChildSprites release];
    
    [super dealloc];
}

@end
