//
//  SPLGameCharacterSelectionScreen.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLGameCharacterSelectionUILayer.h"
#import "FLEvent.h"
#import "FLAction.h"
#import "SPLDialoguePanelLayer.h"
#import "FLYAML.h"

@interface SPLGameCharacterSelectionScene : CCLayer <FLActionDelegate, FLYAMLDelegate>

// Layers
@property (nonatomic, readonly) SPLGameCharacterSelectionUILayer* primaryUILayer;
@property (nonatomic, readonly) SPLDialoguePanelLayer* dialoguePanelLayer;

// Events
@property (nonatomic, retain) FLEvent* Event_SceneWasLoaded;
@property (nonatomic, retain) FLEvent* Event_MaleCharacterWasTouched;
@property (nonatomic, retain) FLEvent* Event_FemaleCharacterWasTouched;
@property (nonatomic, retain) FLEvent* Event_MaleCharacterWasFinalized;
@property (nonatomic, retain) FLEvent* Event_FemaleCharacterWasFinalized;
@property (nonatomic, retain) FLEvent* Event_PlayerNameWasEntered;

#define Action_BeginGame        FLActionMake(Action_BeginGame)

@end
