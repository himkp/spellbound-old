//
//  SPLInGameMenuLayer.h
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLInGameMenuLayer : CCLayer
{
    CCSprite *spriteBackground;
    CCSprite *spriteWhiteUnderlay;
    CCSprite *spriteMainMenu;
    
    CCSprite *spriteMenuBorderTop;
    CCSprite *spriteMenuBorderBottom;
    CCSprite *spriteMenuBorderLeft;
    CCSprite *spriteMenuBorderRight;
    
    // points
    
    CGPoint positionOfBackground;
    CGPoint positionOfWhiteUnderlay;
    CGPoint positionOfMainMenu;
    CGPoint positionOfMenuBorderTop;
    CGPoint positionOfMenuBorderBottom;
    CGPoint positionOfMenuBorderLeft;
    CGPoint positionOfMenuBorderRight;
    
    CGPoint positionOfButtonResume;
    CGPoint positionOfButtonStats;
    CGPoint positionOfButtonJournal;
    CGPoint positionOfButtonSettings;
    CGPoint positionOfButtonQuitToMenu;

}

@property (nonatomic, readonly) CCSprite *spriteButtonResume;
@property (nonatomic, readonly) CCSprite *spriteButtonStats;
@property (nonatomic, readonly) CCSprite *spriteButtonJournal;
@property (nonatomic, readonly) CCSprite *spriteButtonSettings;
@property (nonatomic, readonly) CCSprite *spriteButtonQuitToMenu;

@property (nonatomic) BOOL isDisabled;

-(void)initializePositions;

-(void)show;
-(void)hide;
-(void)show:(void(^)())onShow;
-(void)hide:(void(^)())onHide;

@end
