//
//  SPLCheckPointManager.h
//  Spellbound
//
//  Created by Fleon Games on 1/22/14.
//
//

#import "FLAction.h"

@interface SPLCheckPointManager : NSObject
{
    NSMutableDictionary *checkPoints;
    NSMutableDictionary *checkPointStatuses;
}

-(NSArray *)activeCheckPoints;
-(FLAction *)checkPointWithTitle:(NSString *)title;
-(void)registerCheckPoint:(FLAction *)checkPointAction;
-(void)activateCheckPoint:(FLAction *)checkPointAction;
-(void)resumeFromCheckPointsTitled:(NSArray *)titles;

@end
