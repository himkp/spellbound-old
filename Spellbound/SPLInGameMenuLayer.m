//
//  SPLInGameMenuLayer.m
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLInGameMenuLayer.h"

@implementation SPLInGameMenuLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfBackground            = fl_ccp(   0,    0, 2048, 1536);
        positionOfMainMenu              = fl_ccp(   0,    0, 2048, 1536);
        positionOfWhiteUnderlay         = fl_ccp( 470,  220, 1140, 1240);
        
        positionOfMenuBorderTop         = fl_ccp( 457,   68, 1079,  262);
        positionOfMenuBorderBottom      = fl_ccp( 428, 1303, 1190,  166);
        positionOfMenuBorderRight       = fl_ccp(1382,  330,  154,  973);
        positionOfMenuBorderLeft        = fl_ccp( 514,  330,  144,  973);
        
        positionOfButtonResume          = fl_ccp( 684,  330,  675,  190);
        positionOfButtonStats           = fl_ccp( 685,  520,  674,  192);
        positionOfButtonJournal         = fl_ccp( 685,  712,  674,  205);
        positionOfButtonSettings        = fl_ccp( 684,  917,  684,  192);
        positionOfButtonQuitToMenu      = fl_ccp( 685, 1109,  671,  194);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfBackground            = fl_ccp(   0,    0,  960,  640);
        positionOfMainMenu              = fl_ccp(   0,    0,  960,  640);
        
        positionOfMenuBorderTop         = fl_ccp( 236,   11,  489,  110);
        positionOfMenuBorderLeft        = fl_ccp( 263,  121,   57,  439);
        positionOfMenuBorderRight       = fl_ccp( 653,  121,   62,  439);
        positionOfMenuBorderBottom      = fl_ccp( 236,  560,  505,   75);
        
        positionOfButtonResume          = fl_ccp( 338,  121,  304,   86);
        positionOfButtonStats           = fl_ccp( 338,  207,  304,   87);
        positionOfButtonJournal         = fl_ccp( 338,  294,  304,   92);
        positionOfButtonSettings        = fl_ccp( 338,  386,  308,   86);
        positionOfButtonQuitToMenu      = fl_ccp( 338,  472,  304,   88);
    }
}

-(id)init
{
    if (self=[super init])
    {
        [self initializePositions];
        
        self.visible = NO;
        
        self.isTouchEnabled = YES;
        spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteBackground.scaleX = DEVICE_IS_IPAD ? 1024 : 480;
        spriteBackground.scaleY = DEVICE_IS_IPAD ? 768 : 320;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteBackground.scaleX *= 2;
            spriteBackground.scaleY *= 2;
        }
        spriteBackground.position = positionOfBackground;
        
        spriteWhiteUnderlay = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteWhiteUnderlay.scaleX = 450;
        spriteWhiteUnderlay.scaleY = 570;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteWhiteUnderlay.scaleX *= 2;
            spriteWhiteUnderlay.scaleY *= 2;
        }
        spriteWhiteUnderlay.position = positionOfWhiteUnderlay;
        
        spriteMainMenu = [CCSprite node];
        spriteMainMenu.contentSize = [CCDirector sharedDirector].winSize;
        spriteMainMenu.position = positionOfMainMenu;
        
        spriteMenuBorderTop = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Border_Top.png"];
        spriteMenuBorderLeft = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Border_Left.png"];
        spriteMenuBorderBottom = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Border_Bottom.png"];
        spriteMenuBorderRight = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Border_Right.png"];
        
        spriteMenuBorderTop.position = positionOfMenuBorderTop;
        spriteMenuBorderRight.position = positionOfMenuBorderRight;
        spriteMenuBorderBottom.position = positionOfMenuBorderBottom;
        spriteMenuBorderLeft.position = positionOfMenuBorderLeft;
        
        [self addChild:spriteBackground z:1];
        [spriteMainMenu addChild:spriteWhiteUnderlay];
        [spriteMainMenu addChild:spriteMenuBorderTop];
        [spriteMainMenu addChild:spriteMenuBorderLeft];
        [spriteMainMenu addChild:spriteMenuBorderBottom];
        [spriteMainMenu addChild:spriteMenuBorderRight];
        [self addChild:spriteMainMenu z:5];
        
        _spriteButtonResume = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Button_Resume.png"];
        _spriteButtonStats = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Button_Stats.png"];
        _spriteButtonJournal = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Button_Journal.png"];
        _spriteButtonSettings = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Button_Settings.png"];
        _spriteButtonQuitToMenu = [CCSprite spriteWithSpriteFrameName:@"InGameMenu_Button_QuitToMenu.png"];
        
        _spriteButtonResume.position = positionOfButtonResume;
        _spriteButtonStats.position = positionOfButtonStats;
        _spriteButtonJournal.position = positionOfButtonJournal;
        _spriteButtonSettings.position = positionOfButtonSettings;
        _spriteButtonQuitToMenu.position = positionOfButtonQuitToMenu;
        
        [spriteMainMenu addChild:_spriteButtonResume z:3];
        [spriteMainMenu addChild:_spriteButtonStats z:3];
        [spriteMainMenu addChild:_spriteButtonJournal z:3];
        [spriteMainMenu addChild:_spriteButtonSettings z:3];
        [spriteMainMenu addChild:_spriteButtonQuitToMenu z:3];
        
    }
    return self;
}

-(void)show
{
    [self show:^{}];
}

-(void)show:(void (^)())onShow
{
    if (self.isDisabled) return;
    
    self.visible = YES;
    spriteBackground.opacity= 0;
    spriteMainMenu.scale = 0;
    _spriteButtonResume.scale = 0;
    _spriteButtonStats.scale = 0;
    _spriteButtonJournal.scale = 0;
    _spriteButtonQuitToMenu.scale = 0;
    _spriteButtonSettings.scale = 0;
    
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:180]];
    
    CCActionInterval *action = [CCEaseBackOut actionWithAction:[CCScaleTo actionWithDuration:0.5 scale:1]];
    
    [spriteMainMenu runAction:
     [CCEaseBackOut actionWithAction:
      [CCScaleTo actionWithDuration:0.5 scale:1]]];
    
    [_spriteButtonResume runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.35] two:[action.copy autorelease]]];
    [_spriteButtonStats runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.50] two:[action.copy autorelease]]];
    [_spriteButtonJournal runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.65] two:[action.copy autorelease]]];
    [_spriteButtonSettings runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.80] two:[action.copy autorelease]]];
    [_spriteButtonQuitToMenu runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.95],
      [action.copy autorelease],
      [CCCallBlock actionWithBlock:onShow],
      nil]];
}

-(void)hide
{
    [self hide:^{}];
}

-(void)hide:(void (^)())onHide
{
    if (self.isDisabled) return;
    
    CCActionInterval *scaleToZero = [CCScaleTo actionWithDuration:0.5 scale:0];
    
    [spriteMainMenu runAction:[CCEaseBackIn actionWithAction:scaleToZero]];
    [spriteMainMenu runAction:[CCFadeOut actionWithDuration:0.5]];
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:0]];
    
    [self runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.75],
      [CCHide action],
      [CCCallBlock actionWithBlock:onHide],
      nil]];
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (self.visible)
    {
        if ([FL doesSprite:_spriteButtonResume containPoint:point])
        {
            [FL with:_spriteButtonResume runAnimationAction:kFLAnimationActionShrinkAndGrow];
            NSLog(@"Gotta resume the game");
            [self hide];
            return YES;
        } else if ([FL doesSprite:_spriteButtonStats containPoint:point]) {
            [FL with:_spriteButtonStats runAnimationAction:kFLAnimationActionShrinkAndGrow];
            return YES;
        } else if ([FL doesSprite:_spriteButtonJournal containPoint:point]) {
            [FL with:_spriteButtonJournal runAnimationAction:kFLAnimationActionShrinkAndGrow];
            return YES;
        } else if ([FL doesSprite:_spriteButtonSettings containPoint:point]) {
            [FL with:_spriteButtonSettings runAnimationAction:kFLAnimationActionShrinkAndGrow];
            //[((SPLGameScene*) self.parent).settingsLayer show];
            return YES;
        } else if ([FL doesSprite:_spriteButtonQuitToMenu containPoint:point]) {
            [FL with:_spriteButtonQuitToMenu runAnimationAction:kFLAnimationActionShrinkAndGrow];
            @throw [NSException exceptionWithName:@"Quit" reason:@"Quitting permanently instead" userInfo:nil];
            return YES;
        }
        else if ([FL doesSprite:spriteMenuBorderTop containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderLeft containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderRight containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderBottom containPoint:point]) {
            NSLog(@"No point touching the wooden border eh?");
            return YES;
        }
        else {
            [self hide];
            return YES;
        }
    }
    else return NO;
}

@end
