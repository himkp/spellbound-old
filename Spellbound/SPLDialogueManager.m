//
//  SPLDialogueManager.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/24/12.
//  Copyright (c) 2012 __MyCompany_name_. All rights reserved.
//

#import "SPLDialogueManager.h"
#import "Spellbound.h"

@implementation SPLDialogueSpeaker

-(id)initWithName:(NSString *)name
{
    return [self initWithName:name andMoods:nil];
}

-(id)initWithName:(NSString *)name andMoods:(NSDictionary *)moods
{
    if (self = [super init])
    {
        self.name = name;
        self.moods = moods;
    }
    return self;
}

+(id)speakerWithName:(NSString *)name
{
    return [[[self alloc] initWithName:name] autorelease];
}

+(id)speakerWithName:(NSString *)name andMoods:(NSDictionary *)moods
{
    return [[[self alloc] initWithName:name andMoods:moods] autorelease];
}

-(NSString *)avatarFilenameForMood:(NSString *)mood
{
    if (_moods[mood])
        return _moods[mood];
    else return _moods[@"Normal"];
}

-(NSString *)name
{
    return [_name stringByReplacingVariables];
}

-(void)dealloc
{
    [_moods release];
    [_name release];
    
    [super dealloc];
}

@end

@implementation SPLDialogue

@synthesize text = text_, currentMoodOfSpeaker = currentMoodOfSpeaker_;

-(id)initWithText:(NSString *)text
{
    return [self initWithSpeakerName:nil andText:text];
}

-(id)initWithSpeakerName:(NSString *)speaker
{
    return [self initWithSpeakerName:speaker andText:nil];
}

-(id)initWithSpeakerName:(NSString *)speaker andText:(NSString *)text
{
    if (self = [super init])
    {
        self.speakerName = speaker;
        self.text = text;
        self.currentMoodOfSpeaker = @"Normal";
    }
    return self;
}

-(NSString *)text
{
    return [text_ stringByReplacingVariables];
}

-(NSString *)currentMoodOfSpeaker
{
    return [currentMoodOfSpeaker_ stringByReplacingVariables];
}

-(SPLDialogueSpeaker *)speaker
{
    NSString *oldSpeakerName = _speakerName;
    _speakerName = [[_speakerName stringByReplacingVariables] retain];
    NSLog(@"Accessing speaker: before: %@, after: %@", oldSpeakerName, _speakerName);
    [oldSpeakerName release];
    return [[Spellbound dialogueManager] speakerWithName:_speakerName];
}

+(id)dialogueWithText:(NSString *)text
{
    return [[[self alloc] initWithText:text] autorelease];
}

+(id)dialogueWithSpeakerName:(NSString *)speaker
{
    return [[[self alloc] initWithSpeakerName:speaker] autorelease];
}

+(id)dialogueWithSpeakerName:(NSString *)speaker andText:(NSString *)text
{
    return [[[self alloc] initWithSpeakerName:speaker andText:text] autorelease];
}

-(void)dealloc
{
    [_speakerName release];
    [text_ release];
    [currentMoodOfSpeaker_ release];
    
    [super dealloc];
}

@end

@implementation SPLDialogueSequence

@synthesize title = title_, dialogues = dialogues_;

#pragma mark Spellbound

-(id)initWithTitle:(NSString *)title
{
    return [self initWithTitle:title andDialogues:nil];
}

-(id)initWithTitle:(NSString *)title andDialogues:(NSArray *)dialogues
{
    if (self = [super init])
    {
        self.title = title;
        self.dialogues = dialogues;
    }
    return self;
}

-(SPLDialogue *)dialogueAtIndex:(NSUInteger)index
{
    return dialogues_[index];
}

+(id)dialogueSequenceWithTitle:(NSString *)title
{
    return [[[self alloc] initWithTitle:title] autorelease];
}

+(id)dialogueSequenceWithTitle:(NSString *)title andDialogues:(NSArray *)dialogues
{
    return [[[self alloc] initWithTitle:title andDialogues:dialogues] autorelease];
}

-(void)dealloc
{
    [title_ release];
    [dialogues_ release];
    
    [super dealloc];
}

@end

@implementation SPLDialogueManager

-(id)init
{
    if (self = [super init])
    {
        dialogueSequences = [[NSMutableDictionary alloc] init];
        speakers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(SPLDialogueSequence *)dialogueSequenceWithTitle:(NSString *)title
{
    return dialogueSequences[title];
}

-(void)addDialogueSequence:(SPLDialogueSequence *)dialogueSequence
{
    dialogueSequences[dialogueSequence.title] = dialogueSequence;
}

-(SPLDialogueSpeaker *)speakerWithName:(NSString *)name
{
    return speakers[name];
}

-(void)addSpeaker:(SPLDialogueSpeaker *)speaker
{
    speakers[speaker.name] = speaker;
}

- (void)dealloc {
    [dialogueSequences release];
    [speakers release];
    
    [super dealloc];
}

@end

@implementation SPLMessageManager

- (id)init
{
    self = [super init];
    if (self)
    {
        messages = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSString *)messageWithTitle:(NSString *)title
{
    return [(NSString *) messages[title] stringByReplacingVariables];
}

-(void)addMessage:(NSString *)message withTitle:(NSString *)title
{
    messages[title] = message;
}

- (void)dealloc
{
    [messages release];
    [super dealloc];
}

@end
