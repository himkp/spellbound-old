//
//  SPLConfirmPanelLayer.h
//  Spellbound
//
//  Created by Fleon Games on 5/5/13.
//
//

#import "Spellbound.h"
#import "SPLAlertPanelLayer.h"

@interface SPLConfirmPanelLayer : SPLAlertPanelLayer
{
    CCSprite *spriteBorderBottom3;
    CCSprite *spriteButtonCancel;
    
    CGPoint positionOfBorderBottom3;
    CGPoint positionOfButtonCancel;
}

@property (nonatomic, retain) FLEvent *Event_DidConfirm;
@property (nonatomic, retain) FLEvent *Event_DidNotConfirm;

#define Action_Confirm FLActionMake(Action_Confirm)

@end
