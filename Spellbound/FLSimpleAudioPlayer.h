//
//  FLSimpleAudioPlayer.h
//  Spellbound
//
//  Created by Fleon Games on 6/13/13.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AVAudioPlayer (FLSimpleAudioPlayer)

-(void)fadeIn:(NSTimeInterval)seconds;
-(void)fadeOut:(NSTimeInterval)seconds;
-(void)fadeTo:(float)newVolume duration:(NSTimeInterval)seconds;

@end

@interface FLSimpleAudioPlayer : NSObject
{
    NSMutableArray *audioPlayers;
}

-(void)playTrack:(NSString *)trackFilename;
-(void)stop;

@property (nonatomic) NSUInteger volume;
@property (nonatomic, readonly) NSString *nowPlaying;

@end
