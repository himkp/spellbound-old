//
//  SPLNewGameMenuLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLStartGameMenuLayer : CCLayer
{
    CCSprite *spriteBackground;
    CCSprite *spriteMainMenu;
    
    CCSprite *spriteWhiteUnderlay;
    
    CCSprite *spriteMenuBorderTop;
    CCSprite *spriteMenuBorderBottom;
    CCSprite *spriteMenuBorderLeft;
    CCSprite *spriteMenuBorderRight;
    
    // points
    
    CGPoint positionOfBackground;
    CGPoint positionOfWhiteUnderlay;
    CGPoint positionOfMainMenu;
    CGPoint positionOfMenuBorderTop;
    CGPoint positionOfMenuBorderBottom;
    CGPoint positionOfMenuBorderLeft;
    CGPoint positionOfMenuBorderRight;
    
    CGPoint positionOfButtonContinue;
    CGPoint positionOfButtonNewGame;
    CGPoint positionOfButtonSettings;
    CGPoint positionOfButtonCredits;
    CGPoint positionOfButtonMoreGames;
}

@property (nonatomic, readonly) CCSprite *spriteButtonContinue;
@property (nonatomic, readonly) CCSprite *spriteButtonNewGame;
@property (nonatomic, readonly) CCSprite *spriteButtonSettings;
@property (nonatomic, readonly) CCSprite *spriteButtonCredits;
@property (nonatomic, readonly) CCSprite *spriteButtonMoreGames;

-(void)initializePositions;

-(void)show;
-(void)hide;

@end
