//
//  SPLConnectorBlockSprite.h
//  Spellbound
//
//  Created by Fleon Games on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

typedef enum {
    SPLConnectorBlockDirectionTop,
    SPLConnectorBlockDirectionRight,
    SPLConnectorBlockDirectionBottom,
    SPLConnectorBlockDirectionLeft,
    SPLConnectorBlockDirectionNone
} SPLConnectorBlockDirection;

typedef struct {
    SPLConnectorBlockDirection directionStart;
    SPLConnectorBlockDirection directionEnd;
    BOOL isDirectionStartValid;
    BOOL isDirectionEndValid;
} SPLConnectorBlockInfo;

SPLConnectorBlockInfo 
    SPLConnectorBlockSpriteInfoMake(SPLConnectorBlockDirection directionStart, 
                                    SPLConnectorBlockDirection directionEnd,
                                    BOOL isDirectionStartValid, 
                                    BOOL isDirectionEndValid);

@interface SPLConnectorBlockSprite : CCTMXTiledMap
{
    SPLConnectorBlockInfo blockInfo;
}

+(id)connectorBlockSpriteWithInfo:(SPLConnectorBlockInfo)info;

-(id)initWithInfo:(SPLConnectorBlockInfo)info;

@end
