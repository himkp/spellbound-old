//
//  SPLLetterBlock.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLLetterBlockSprite.h"

@class SPLTiledMap;

@interface SPLLetterBlock : NSObject <NSCoding>
{
    SPLTileType tileTypeOfAdjacentBlockToTheRight;
    SPLTileType tileTypeOfAdjacentBlockToTheLeft;
    SPLTileType tileTypeOfAdjacentBlockToTheTop;
    SPLTileType tileTypeOfAdjacentBlockToTheBottom;
    
    NSString *verticalWord;
    NSString *horizontalWord;
}

@property (nonatomic, readonly) NSString* letter;
@property (nonatomic) BOOL isFrozen;
@property (nonatomic) FLUncertainBoolean isConnectedToACliffBlock;
@property (nonatomic) FLUncertainBoolean areAdjacentBlocksConnectedToACliffBlock;
@property (nonatomic, readonly) SPLTiledMap* tiledMap;
@property (nonatomic) CGPoint location;

@property (nonatomic, readonly) SPLLetterBlockSprite *letterSprite;

-(id)initWithSprite:(SPLLetterBlockSprite*)letterSprite onMap:(SPLTiledMap*)tiledMap atLocation:(CGPoint)location;

-(SPLTileType)tileTypeOfAdjacentBlockToTheLeft;
-(SPLTileType)tileTypeOfAdjacentBlockToTheRight;
-(SPLTileType)tileTypeOfAdjacentBlockToTheTop;
-(SPLTileType)tileTypeOfAdjacentBlockToTheBottom;

-(SPLLetterBlock*)letterBlockToTheLeft;
-(SPLLetterBlock*)letterBlockToTheRight;
-(SPLLetterBlock*)letterBlockToTheTop;
-(SPLLetterBlock*)letterBlockToTheBottom;

-(NSString*)verticalWord;
-(NSString*)horizontalWord;

+(id)letterBlockWithSprite:(SPLLetterBlockSprite*)letterSprite onMap:(SPLTiledMap*)tiledMap atLocation:(CGPoint)location;
@end