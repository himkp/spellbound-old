//
//  FLAStarPathfinder.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FLAStarPathfinder.h"

@implementation FLAStarPathfinder

-(id)initWithPathfinderDelegate:(id<FLAStarPathfinderDelegate>)delegate
{
    if (self = [super init])
    {
        self.delegate = delegate;
    }
    return self;
}

+(id)pathfinderWithPathfinderDelegate:(id<FLAStarPathfinderDelegate>)delegate
{
    return [[[self alloc] initWithPathfinderDelegate:delegate] autorelease];
}

-(NSArray*)findPathFromSource:(CGPoint)source toDestination:(CGPoint)destination
{   
    NSMutableSet *closedSet, *openSet;
    NSMutableDictionary *cameFrom;
    NSMutableDictionary *gScores, *hScores, *fScores;
    
    closedSet = [NSMutableSet set];
    openSet = [NSMutableSet setWithObject:[NSValue valueWithCGPoint:source]];
    cameFrom = [NSMutableDictionary dictionary];
    
    gScores = [NSMutableDictionary dictionary];
    hScores = [NSMutableDictionary dictionary];
    fScores = [NSMutableDictionary dictionary];
    
    NSNumber* gScoreStart = @0;
    NSNumber* hScoreStart = @([self heuristicCostEstimateFromSource:source toDestination:destination]);
    NSNumber* fScoreStart = @([gScoreStart integerValue] + [hScoreStart integerValue]);
    
    gScores[gScoreStart] = [NSValue valueWithCGPoint:source];
    hScores[hScoreStart] = [NSValue valueWithCGPoint:source];
    fScores[fScoreStart] = [NSValue valueWithCGPoint:source];
    
    while ([openSet count]) {
        CGPoint current;
        NSInteger minimumFScore = NSIntegerMax;
        for (NSValue *currentValue in openSet)
        {
            NSInteger currentFScore = [fScores[currentValue] integerValue];
            minimumFScore = MIN(currentFScore, minimumFScore);
            if (minimumFScore == currentFScore)
                current = [currentValue CGPointValue];
        }
        
        if (CGPointEqualToPoint(current, destination))
        {
            NSValue *currentValue = [NSValue valueWithCGPoint:current];
            NSMutableArray *returnValue = [@[currentValue].mutableCopy autorelease];
            while ((currentValue = cameFrom[currentValue]))
                [returnValue addObject:currentValue];
            NSMutableArray *returnValue2 = [NSMutableArray arrayWithCapacity:[returnValue count]];
            for (id item in [returnValue reverseObjectEnumerator])
                [returnValue2 addObject:item];
            return [NSArray arrayWithArray:returnValue2];
        }
        
        [openSet removeObject:[NSValue valueWithCGPoint:current]];
        [closedSet addObject:[NSValue valueWithCGPoint:current]];
        
        for (NSValue* neighbour in [self neighbouringNodesForPoint:current])
        {
            if ([closedSet containsObject:neighbour])
                continue;
            
            NSInteger tentativeGScore = [gScores[[NSValue valueWithCGPoint:current]] integerValue] + [_delegate costOfNodeAtPoint:[neighbour CGPointValue]];
            
            BOOL tentativeIsBetter = NO;
            
            if (![openSet containsObject:neighbour])
            {
                [openSet addObject:neighbour];
                
                NSNumber* hScoreNeighbour = @([self heuristicCostEstimateFromSource:[neighbour CGPointValue] toDestination:destination]);
                
                hScores[neighbour] = hScoreNeighbour;
                
                tentativeIsBetter = YES;
            }
            else if (tentativeGScore < [gScores[neighbour] integerValue])
                tentativeIsBetter = YES;
            else
                tentativeIsBetter = NO;
            
            if (tentativeIsBetter)
            {
                cameFrom[neighbour] = [NSValue valueWithCGPoint:current];
                gScores[neighbour] = @(tentativeGScore);
                fScores[neighbour] = @(((NSNumber *)gScores[neighbour]).integerValue + ((NSNumber *) hScores[neighbour]).integerValue);
            }
        }
    }
    
    return nil;
}

-(NSInteger)heuristicCostEstimateFromSource:(CGPoint)source toDestination:(CGPoint)destination
{
    return [self distanceBetweenPointA:source andPointB:destination];
}

-(NSSet *)neighbouringNodesForPoint:(CGPoint)point
{
    return [_delegate validNeighbouringNodesForPoint:point];
}

-(NSInteger)distanceBetweenPointA:(CGPoint)a andPointB:(CGPoint)b
{
    return abs(a.x - b.x) + abs(a.y - b.y);
}

-(void)dealloc
{
    [_delegate release];
    [super dealloc];
}

@end
