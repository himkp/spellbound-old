//
//  SPLPrimaryUILayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLTitleUILayer : CCLayer
{
    CCSprite *spriteBackground;
    CCSprite *spriteBag;
    CCSprite *spriteBagGlowing;
    CCSprite *spriteLetters[10];
    
    CCParticleSystem *spriteEmitterSnow;
    CCParticleSystem *spriteEmitterGlitter;
    // start button 306, 547
    
    int currentLetter;
    
    // points
    
    CGPoint positionOfBackground;
    CGPoint positionOfLettersInitially;
    CGPoint positionOfLetters[10];
    CGPoint positionOfBag;
    CGPoint positionOfBagGlowing;
    CGPoint positionOfButtonStart;
    CGPoint positionOfButtonCredits;
    CGPoint positionOfButtonSettings;
    CGPoint positionOfButtonSound;
    
    CGPoint positionOfEmitterSnow;
    CGPoint positionOfEmitterGlitter;
}

@property (nonatomic, readonly) CCSprite* spriteButtonStart;
@property (nonatomic, readonly) CCSprite* spriteButtonCredits;
@property (nonatomic, readonly) CCSprite* spriteButtonSound;
@property (nonatomic, readonly) CCSprite* spriteButtonSettings;

@property (nonatomic, readonly) CGPoint* letterPoints;

-(void)initializePositions;

-(void)animateLetters:(ccTime)dt;
-(void)teeterLetter:(id)sender number:(int)index;
-(void)show:(id)sender sprite:(CCSprite*)sprite;
-(void)toggleVolume;

@end
