//
//  SPLGameCharacter.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGameCharacter.h"
#import "SPLTiledMap.h"
#import "FLAStarPathfinder.h"

@implementation CCNode (SetTransform)

-(void)setTransform:(CGAffineTransform)transform
{
    transform_ = transform;
    isTransformDirty_ = isInverseDirty_ = NO;
}

@end

@implementation SPLGameCharacter

@synthesize Event_SteppedOn, Event_IsWithinRadius, Event_IsWithinDiagonalRadius;

-(id)initWithCharacterName:(NSString *)name
{
    return [self initWithCharacterName:name andInitialAnimation:SPLGameCharacterAnimationIdleDown];
}

-(id)initWithCharacterName:(NSString *)name andInitialAnimation:(SPLGameCharacterAnimation)animation
{
    FLEventMake(Event_SteppedOn);
    FLEventMake(Event_IsWithinRadius);
    FLEventMake(Event_IsWithinDiagonalRadius);
    
    Event_IsWithinDiagonalRadius.eventDataComparator = ^BOOL (id dispatchedEventData, id conditionalActionData) {
        return [dispatchedEventData integerValue] <= [conditionalActionData integerValue];
    };
    
    _characterName = [name retain];
    
    CGPoint anchorPoint = ccp(0, 0);
    
    NSString *characterRigPlist = [NSString stringWithFormat:@"iPad/Animations/%@_Rig.plist", name];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:characterRigPlist];
    
    if (self = [super init])
    {
        self.anchorPoint = anchorPoint;
        self.contentSize = CGSizeMake(64, 64);
        
        CCSprite *spriteWhiteUnderlay = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteWhiteUnderlay.anchorPoint = ccp(0, 0);
        spriteWhiteUnderlay.scaleX = self.boundingBox.size.width;
        spriteWhiteUnderlay.scaleY = self.boundingBox.size.height;
        spriteWhiteUnderlay.position = ccp(0, 0);
        spriteWhiteUnderlay.opacity = 0 * 255; // for debugging purposes only
        [self addChild:spriteWhiteUnderlay];
        
        lastSteppedOn = [[NSMutableString alloc] initWithString:@""];
        
        // hack: initialize with Island1Cliff so that the player can make words
        // connected to Island1 to begin with
        cliffsSteppedOn = @[@"Island1Cliff"].mutableCopy;
        
        _charactersFollowing = [[NSMutableArray alloc] init];
        
        spriteExclamationCallout = [CCSprite spriteWithSpriteFrameName:@"CharacterCallout_Exclamation.png"];
        spriteExclamationCallout.visible = NO;
        [self addChild:spriteExclamationCallout];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:name];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:name];
        
        [self runAnimation:animation];
        
        animationData = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    SPLGameCharacterAnimation animation = [decoder decodeIntegerForKey:@"currentAnimation"];
    if (self = [self initWithCharacterName:[decoder decodeObjectForKey:@"characterName"] andInitialAnimation:animation])
    {
        lastSteppedOn.string = [decoder decodeObjectForKey:@"lastSteppedOn"];
        cliffsSteppedOn.array = [decoder decodeObjectForKey:@"cliffsSteppedOn"];
        self.position = [decoder decodeCGPointForKey:@"position"];
        self.anchorPoint = [decoder decodeCGPointForKey:@"anchorPoint"];
        _charactersFollowing.array = [decoder decodeObjectForKey:@"charactersFollowing"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_characterName forKey:@"characterName"];
    [encoder encodeObject:cliffsSteppedOn forKey:@"cliffsSteppedOn"];
    [encoder encodeObject:lastSteppedOn forKey:@"lastSteppedOn"];
    [encoder encodeObject:_charactersFollowing forKey:@"charactersFollowing"];
    [encoder encodeCGPoint:position_ forKey:@"position"];
    [encoder encodeCGPoint:anchorPoint_ forKey:@"anchorPoint"];
    [encoder encodeInteger:(NSInteger)_currentAnimation forKey:@"currentAnimation"];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_WalkOneTileUp] ||
        [action is:Action_WalkOneTileDown] ||
        [action is:Action_WalkOneTileLeft] ||
        [action is:Action_WalkOneTileRight])
    {
        if (stopWalkingAction) // should the character stop walking?
        {
            if (_currentAnimation == SPLGameCharacterAnimationWalkingDown)
                [Action_StopFacingDown run];
            else if (_currentAnimation == SPLGameCharacterAnimationWalkingLeft)
                [Action_StopFacingLeft run];
            else if (_currentAnimation == SPLGameCharacterAnimationWalkingRight)
                [Action_StopFacingRight run];
            else if (_currentAnimation == SPLGameCharacterAnimationWalkingUp)
                [Action_StopFacingUp run];
            
            FLAction *_stopWalkingAction = stopWalkingAction;
            stopWalkingAction = nil;
            [_stopWalkingAction finish];
            [_stopWalkingAction release];
            
            // this action will never finish, thereby pausing all subsequent actions
            return;
        }
        
        CGPoint position = [self currentTilePositionInPoints];
        
        /*
        SPLLetterBlock *letterBlock = _tiledMap.letterBlocks[[NSValue valueWithCGPoint:[_tiledMap convertPositionToTilePosition:position]]];
        [letterBlock.letterSprite runAction:[CCMoveBy actionWithDuration:0.45 position:ccp(0, 5)]];
         */
        
        SPLTileType currentTileType = [self.tiledMap tileTypeAt:[self.tiledMap convertPositionToTilePosition:position]];
        SPLGameCharacterAnimation animationToRun, alternateAnimationToRun;
        if ([action is:Action_WalkOneTileUp])
        {
            animationToRun = SPLGameCharacterAnimationWalkingUp;
            alternateAnimationToRun = SPLGameCharacterAnimationJumpingUp;
            position.y += _tiledMap.tileSize.height;
        }
        else if ([action is:Action_WalkOneTileDown])
        {
            animationToRun = SPLGameCharacterAnimationWalkingDown;
            alternateAnimationToRun = SPLGameCharacterAnimationJumpingDown;
            position.y -= _tiledMap.tileSize.height;
        }
        else if ([action is:Action_WalkOneTileLeft])
        {
            animationToRun = SPLGameCharacterAnimationWalkingLeft;
            alternateAnimationToRun = SPLGameCharacterAnimationJumpingLeft;
            position.x -= _tiledMap.tileSize.width;
        }
        else // if ([action is:Action_WalkOneTileRight])
        {
            animationToRun = SPLGameCharacterAnimationWalkingRight;
            alternateAnimationToRun = SPLGameCharacterAnimationJumpingRight;
            position.x += _tiledMap.tileSize.width;
        }
        
        CGPoint tilePosition = [self.tiledMap convertPositionToTilePosition:position];
        SPLTileType nextTileType = [self.tiledMap tileTypeAt:tilePosition];
        
        double actionDuration = 0.50; // seconds
        BOOL isJumpEnding = NO;
        CCActionInterval *moveAction = [CCMoveTo actionWithDuration:actionDuration position:position];
        
        if (currentTileType == SPLTileTypeCliff || nextTileType == SPLTileTypeCliff)
        {
            [self runAnimation:alternateAnimationToRun];
            if (currentTileType == SPLTileTypeCliff) {// tile we are jumping across
                actionDuration = (0.20) + 0.25 + 0.5 + 0.333; // hack: 1.33/2 seconds (80/2 = 40 frames for half-jump)
                isJumpEnding = YES;
                moveAction = [CCEaseSineOut actionWithAction:moveAction];
            } else {
                actionDuration = 0.25;
                moveAction = [CCEaseSineIn actionWithAction:moveAction];
            }
        }
        else
        {
            [self runAnimation:animationToRun];
        }
        
        /*
        letterBlock = _tiledMap.letterBlocks[[NSValue valueWithCGPoint:[_tiledMap convertPositionToTilePosition:position]]];
        [letterBlock.letterSprite runAction:[CCMoveBy actionWithDuration:0.45 position:ccp(0, -5)]];
         */

        SPLTileDescription *tileDescription = [self.tiledMap tileDescriptionAt:[self.tiledMap convertPositionToTilePosition:position]];
        if (![tileDescription.name isEqualToString:lastSteppedOn])
        {
            /* NSLog(@"stepped on: %@, current position: %@",
                  tileDescription.name,
                  NSStringFromCGPoint([self.tiledMap convertPositionToTilePosition:position])); */
            
            if ([tileDescription.name rangeOfString:@"Cliff"].location != NSNotFound)
                [cliffsSteppedOn addObject:tileDescription.name];
            
            [lastSteppedOn setString:tileDescription.name];

            if ([[tileDescription.name substringToIndex:4] isEqualToString:@"Exit"]) {
                // todo: come up with logic to figure out what direction the player will face
                // when entering another map
                if ([action is:Action_WalkOneTileDown])
                    [Action_StopFacingDown run];
                else if ([action is:Action_WalkOneTileUp])
                    [Action_StopFacingUp run];
                else if ([action is:Action_WalkOneTileLeft])
                    [Action_StopFacingLeft run];
                else if ([action is:Action_WalkOneTileRight])
                    [Action_StopFacingRight run];
                _isAnimating = NO;
                [Event_SteppedOn dispatchWithData:tileDescription.name];
                return;
            }
            // NSLog(@"dispatching stepped on event");
            [Event_SteppedOn dispatchWithData:tileDescription.name];
        }
        
        if ([self.characterName isEqualToString:[@"Player" stringByReplacingVariables]])
            for (SPLGameCharacter *character in _tiledMap.characters)
            {
                if ([character.characterName isEqualToString:[@"Player" stringByReplacingVariables]]) continue;
                CGPoint distanceInPoints = ccpSub(character.currentTilePosition, [_tiledMap convertPositionToTilePosition:position]);
                NSUInteger distance = max(abs(distanceInPoints.x), abs(distanceInPoints.y));
                // NSLog(@"about to dispatch IsWithinDiagonalRadius on characters: %@, %@, distance: %d", character.characterName, self.characterName, distance);
                [character.Event_IsWithinDiagonalRadius dispatchWithData:@(distance)];
            }
        
        [self runAction:
         [CCSequence actions:
          moveAction,
          [CCCallFunc actionWithTarget:action selector:@selector(finish)],
         nil]];
        _isAnimating = YES;
    }
    else if ([action is:Action_StopFacingUp] ||
             [action is:Action_StopFacingDown] ||
             [action is:Action_StopFacingLeft] ||
             [action is:Action_StopFacingRight] ||
             
             [action is:Action_BeExcitedFacingDown] /* flippers only */)
    {
        if ([action is:Action_StopFacingUp])
            [self runAnimation:SPLGameCharacterAnimationIdleUp];
        else if ([action is:Action_StopFacingRight])
            [self runAnimation:SPLGameCharacterAnimationIdleRight];
        else if ([action is:Action_StopFacingDown])
            [self runAnimation:SPLGameCharacterAnimationIdleDown];
        else if ([action is:Action_StopFacingLeft])
            [self runAnimation:SPLGameCharacterAnimationIdleLeft];
        else if ([action is:Action_BeExcitedFacingDown])
            [self runAnimation:SPLGameCharacterAnimationExcitedDown];

        [action finish];
        _isAnimating = NO;
        
        if (stopWalkingAction) {
            FLAction *_stopWalkingAction = stopWalkingAction;
            stopWalkingAction = nil;
            [_stopWalkingAction finish];
            [_stopWalkingAction release];
        }
    }
    else if ([action is:Action_WalkNear])
    {
        NSArray *allCharacters = [_tiledMap.characters arrayByAddingObject:[_tiledMap mainCharacter]];
        for (SPLGameCharacter *character in allCharacters)
        {
            if ([character.characterName isEqualToString:[action.data stringByReplacingVariables]])
            {
                CGPoint positionToMoveTo = ccpAdd(character.currentTilePosition, ccp(0, -1));
                [self walkToTile:positionToMoveTo andThen:^{
                    [action finish];
                }];
            }
        }
    }
    else if ([action is:Action_CircleAround])
    {
        NSArray *allCharacters = [_tiledMap.characters arrayByAddingObject:[_tiledMap mainCharacter]];
        for (SPLGameCharacter *character in allCharacters)
        {
            if ([character.characterName isEqualToString:[action.data stringByReplacingVariables]])
            {
                
                NSArray *actions = @[Action_WalkOneTileLeft, Action_StopFacingLeft /* character */, Action_WalkOneTileDown,
                                     Action_WalkOneTileDown, Action_StopFacingDown /* character */, Action_WalkOneTileRight,
                                     Action_WalkOneTileRight, Action_StopFacingRight /* character */, Action_WalkOneTileUp,
                                     Action_WalkOneTileUp, Action_StopFacingUp /* character */, Action_WalkOneTileLeft,
                                     Action_StopFacingDown];
                
                // todo: account for all directions and blocked paths
                FLAction *firstAction = actions[0];
                for (NSUInteger i = 0; i < actions.count; i++)
                {
                    FLAction *action = actions[i];
                    if (i % 3 == 1) action.actionDelegate = character;
                    if (i) [actions[i - 1] setSubsequentAction:actions[i]];
                }
                FLActionSequence *sequence = [FLActionSequence actionSequenceWithActionDelegate:self andFirstAction:firstAction andName:@"CircleAround"];
                sequence.onFinish = ^{
                    [action finish];
                };
                [sequence run];
                break;
            }
        }
    }
    else if ([action is:Action_StartFollowing])
    {
        NSArray *allCharacters = [_tiledMap.characters arrayByAddingObject:[_tiledMap mainCharacter]];
        for (SPLGameCharacter *character in allCharacters)
        {
            if ([character.characterName isEqualToString:[action.data stringByReplacingVariables]])
            {
                [character.charactersFollowing addObject:self];
                [_tiledMap.characters removeObject:self];
            }
        }
        [action finish];
    }
    else if ([action is:Action_StopWalking])
    {
        if (_isAnimating)
            stopWalkingAction = [action retain];
        else [action finish];
        for (SPLGameCharacter *character in _charactersFollowing)
        {
            // bubble the action to each character following you
            FLAction *action = Action_StopWalking;
            action.actionDelegate = character;
            [action run];
        }
    }
    else if ([action is:Action_Exclaim])
    {
        CGPoint initialCalloutPosition = ccp(- (90 - 64) / 2, (90 - 64) / 2);
        
        spriteExclamationCallout.opacity = 0;
        spriteExclamationCallout.anchorPoint = ccp(0, 0);
        spriteExclamationCallout.position = initialCalloutPosition;
        
        [spriteExclamationCallout runAction:
         [CCSequence actions:
          [CCShow action],
          [CCFadeIn actionWithDuration:0.20],
          [CCDelayTime actionWithDuration:1.0],
          [CCFadeOut actionWithDuration:0.20],
          [CCHide action],
          [CCCallBlock actionWithBlock:^ {
             [action finish];
         }],
          nil]];
        
        [spriteExclamationCallout runAction:
         [CCSequence actions:
          [CCEaseBackOut actionWithAction:
           [CCMoveTo actionWithDuration:0.35 position:ccp(initialCalloutPosition.x, initialCalloutPosition.y + 64 + 16)]],
          [CCDelayTime actionWithDuration:0.7],
          [CCEaseBackIn actionWithAction:
           [CCMoveTo actionWithDuration:0.35 position:ccp(initialCalloutPosition.x, initialCalloutPosition.y + 128 + 16)]],
          nil]];
    }
    else [action forwardToActionDelegate:[Spellbound globalActionDelegate]];
}

-(NSArray *)cliffsSteppedOn
{
    return [NSArray arrayWithArray:cliffsSteppedOn];
}

-(void)setParent:(CCNode *)parent
{
    // the character is added in a container sprite on the tiled map
    _tiledMap = (SPLTiledMap *) parent.parent;
    NSLog(@"tiled map is: %@, parent pos: %@, pos: %@", _tiledMap, NSStringFromCGPoint(parent.position), NSStringFromCGPoint(position_));
    
    super.parent = parent;
}

-(CGPoint)currentTilePosition
{
    return [_tiledMap convertPositionToTilePosition:self.position];
}

-(CGPoint)currentTilePositionInPoints
{
    return ccp(ceil(self.position.x / _tiledMap.tileSize.width) * _tiledMap.tileSize.width,
               floor(self.position.y / _tiledMap.tileSize.height) * _tiledMap.tileSize.height);
}

-(BOOL)canMoveToTile:(CGPoint)tile
{
    return [_tiledMap tileTypeAt:tile] == SPLTileTypeLand;
}

-(void)teleportToTile:(CGPoint)tile
{
    
}

-(void)walkToTile:(CGPoint)tile andThen:(void (^)(void))block
{
    FLAStarPathfinder *pathfinder = [FLAStarPathfinder pathfinderWithPathfinderDelegate:_tiledMap];
    
    NSArray* path = [pathfinder findPathFromSource:self.currentTilePosition toDestination:tile];
    
    [self walkAlongPath:[path arrayByAddingObject:path[path.count - 1]] andThen:block];
}

-(void)walkToTile:(CGPoint)tile
{
    [self walkToTile:tile andThen:^{}];
}

-(void)walkAlongPath:(NSArray *)path
{
    [self walkAlongPath:path andThen:^{}];
}

-(void)walkAlongPath:(NSArray *)path andThen:(void (^)(void))block
{
    NSLog(@"Walking along path: %@", path);
    FLAction* firstAction = nil;
    FLAction* currentAction = nil, *previousAction = nil;
    CGPoint currentPosition, previousPosition = ccp(INT_MAX, INT_MAX);
    // if the last item is a cliff
    
    NSUInteger l = path.count;
    
    // the last item is just for the direction
    // if the second last item is a cliff, set l-- because
    // your destination point cannot be a cliff
    if (l >= 2 && [self.tiledMap tileTypeAt:[path[l - 2] CGPointValue]] == SPLTileTypeCliff) l--;
    
    for (NSUInteger i = 0; i < l; i++)
    {
        NSValue *node = path[i];
        
        currentPosition = [node CGPointValue];
        
        if (previousPosition.x == INT_MAX && previousPosition.y == INT_MAX) {
            previousPosition = currentPosition;
            continue;
        }
        
        if (i <= l - 2) { // for all items except the last
            if (currentPosition.x > previousPosition.x)
                currentAction = Action_WalkOneTileRight;
            else if (currentPosition.x < previousPosition.x)
                currentAction = Action_WalkOneTileLeft;
            else if (currentPosition.y > previousPosition.y)
                currentAction = Action_WalkOneTileDown;
            else if (currentPosition.y < previousPosition.y)
                currentAction = Action_WalkOneTileUp;
        }
        
        if (i >= l - 2) // last item is just for the direction,
            // and this is where the loop terminates
        {
            if (!currentAction) {
                currentAction = [FLAction actionWithActionDelegate:self];
                currentAction.onRun = ^{
                    [currentAction finish];
                };
            }
            i = l - 1;
            node = path[i];
            currentPosition = [node CGPointValue];
            if (currentPosition.x > previousPosition.x)
                currentAction.subsequentAction = Action_StopFacingRight;
            else if (currentPosition.x < previousPosition.x)
                currentAction.subsequentAction = Action_StopFacingLeft;
            else if (currentPosition.y > previousPosition.y)
                currentAction.subsequentAction = Action_StopFacingDown;
            else if (currentPosition.y < previousPosition.y)
                currentAction.subsequentAction = Action_StopFacingUp;
        }
        
        if (!firstAction)
            firstAction = currentAction;
        
        if (previousAction)
            previousAction.subsequentAction = currentAction;
        previousAction = currentAction;
        previousPosition = currentPosition;
        
    }
    
    if (firstAction) {
        [firstAction run];
        for (SPLGameCharacter *character in _charactersFollowing)
        {
            // old logic
            
            /*
            // last point is just for the direction, hence using point# count - 2
            CGPoint targetPoint = [path[path.count - 2] CGPointValue];
            CGPoint altPoints[8] = {
                {0, 1}, {0, -1}, {1, 0}, {-1, 0},
                {1, 1}, {-1, 1}, {1, -1}, {-1, -1}
            };
            
            // todo: determine priority depending on the end direction - start direction
            
            for (int i = 0; i < 8; i++) {
                if (![_tiledMap isPointAnObstruction:ccpAdd(targetPoint, altPoints[i])]) {
                    targetPoint = ccpAdd(targetPoint, altPoints[i]);
                    break;
                }
            }
            
            [character walkToTile:targetPoint];
            NSLog(@"character bounding box: %@", NSStringFromCGRect(character.boundingBox));
             */
            
            NSMutableArray *newPath = [NSMutableArray array];
            [newPath addObject:[NSValue valueWithCGPoint:character.currentTilePosition]];
            [newPath addObjectsFromArray:path];
            [newPath removeObjectAtIndex:newPath.count - 1];
            [character walkAlongPath:newPath];
        }
    }
    
    FLAction *lastAction = nil;
    while ((firstAction = firstAction.subsequentAction)) {
        lastAction = firstAction;
    }
    
    if (lastAction) {
        lastAction.onFinish = block;
    }
}

-(void)runAnimation:(SPLGameCharacterAnimation)animation
{
    if (animation == _currentAnimation) return;
    _currentAnimation = animation;
    
    [self stopAllActions];
    
    NSString *jsonPath;
    switch (animation)
    {
        default:
        case SPLGameCharacterAnimationIdleDown: jsonPath = [NSString stringWithFormat:@"%@_Idle_Down", _characterName]; break;
        case SPLGameCharacterAnimationIdleUp: jsonPath = [NSString stringWithFormat:@"%@_Idle_Up", _characterName]; break;
        case SPLGameCharacterAnimationIdleLeft: jsonPath = [NSString stringWithFormat:@"%@_Idle_Left", _characterName]; break;
        case SPLGameCharacterAnimationIdleRight: jsonPath = [NSString stringWithFormat:@"%@_Idle_Right", _characterName]; break;
            
        case SPLGameCharacterAnimationWalkingDown: jsonPath = [NSString stringWithFormat:@"%@_Walk_Down", _characterName]; break;
        case SPLGameCharacterAnimationWalkingUp: jsonPath = [NSString stringWithFormat:@"%@_Walk_Up", _characterName]; break;
        case SPLGameCharacterAnimationWalkingLeft: jsonPath = [NSString stringWithFormat:@"%@_Walk_Left", _characterName]; break;
        case SPLGameCharacterAnimationWalkingRight: jsonPath = [NSString stringWithFormat:@"%@_Walk_Right", _characterName]; break;
            
        case SPLGameCharacterAnimationJumpingDown: jsonPath = [NSString stringWithFormat:@"%@_Jump_Down", _characterName]; break;
        case SPLGameCharacterAnimationJumpingUp: jsonPath = [NSString stringWithFormat:@"%@_Jump_Up", _characterName]; break;
        case SPLGameCharacterAnimationJumpingLeft: jsonPath = [NSString stringWithFormat:@"%@_Jump_Left", _characterName]; break;
        case SPLGameCharacterAnimationJumpingRight: jsonPath = [NSString stringWithFormat:@"%@_Jump_Right", _characterName]; break;
            
        case SPLGameCharacterAnimationExcitedDown: jsonPath = [NSString stringWithFormat:@"%@_Excited_Down", _characterName]; break;
    }
    
    if (currentAnimationSprite)
    {
        [currentAnimationSprite removeFromParentAndCleanup:NO];
        currentAnimationSprite = nil;
        [animationSpriteList release];
        animationSpriteList = nil;
        [jsonData release];
        jsonData = nil;
    }
    currentAnimationSprite = [CCSprite node];
    // virtual bounding box in flash has a height of 900, and heights are considered
    // according to the previous bounding box - 500
    // 90 was the size of the sprite previously
    double boundingBoxSize = 900 * 90 / 500;
    currentAnimationSprite.position = ccp(-boundingBoxSize / 2 + self.boundingBox.size.width / 2, boundingBoxSize - self.boundingBox.size.height / 2);
    currentAnimationSprite.scaleY = -1;
    [self addChild:currentAnimationSprite];
    animationSpriteList = [[NSMutableDictionary alloc] init];
    
    if (animationData[jsonPath])
        jsonData = animationData[jsonPath];
    else
    {
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
            jsonPath = [NSString stringWithFormat:@"%@-hd", jsonPath];
        jsonPath = [[NSBundle mainBundle] pathForResource:jsonPath ofType: @"json" inDirectory:@"iPad/Animations"];
        NSData* data = [NSData dataWithContentsOfFile:jsonPath];
        jsonData = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] retain];
    }
    
    __block NSUInteger totalFrames = 0;
    for (NSString *spriteName in jsonData.keyEnumerator)
    {
        NSDictionary *spriteData = jsonData[spriteName];

        CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:spriteData[@"data"]];
        sprite.anchorPoint = ccp(0, 1);
        
        animationSpriteList[spriteName] = sprite;
        
        NSDictionary *frameData = spriteData[@"frames"][0];
        if (frameData && ![frameData isKindOfClass:[NSNull class]])
        {
            sprite.visible = YES;
            NSDictionary *affineTransform = frameData[@"transform"];
            CGAffineTransform transform = CGAffineTransformMake([affineTransform[@"a"] doubleValue],
                                                                [affineTransform[@"b"] doubleValue],
                                                                [affineTransform[@"c"] doubleValue],
                                                                [affineTransform[@"d"] doubleValue],
                                                                [affineTransform[@"tx"] doubleValue],
                                                                [affineTransform[@"ty"] doubleValue]);
            sprite.transform = transform;

            sprite.opacity = 255 * [frameData[@"alpha"] doubleValue];
            sprite.zOrder = [frameData[@"zIndex"] doubleValue];
        }
        else
        {
            sprite.visible = NO;
        }
        
        totalFrames = max(totalFrames, [spriteData[@"frames"] count]);
        
        [currentAnimationSprite addChild:sprite];
    }
    
    __block NSUInteger currentFrame = 0;
    
    void(^rigAnimationBlock)() = ^
    {
        // set the z-index of the character such that the character
        // in the lower area of the map has a higher z-index

        [self.parent reorderChild:self z:
         self.tiledMap.mapSize.width * self.tiledMap.tileSize.width *
         (self.tiledMap.mapSize.height * self.tiledMap.tileSize.height - self.position.y) + self.position.x];
        
        for (NSString *spriteName in animationSpriteList.keyEnumerator)
        {
            CCSprite *sprite = animationSpriteList[spriteName];
            NSDictionary *spriteData = jsonData[spriteName];
            if (currentFrame >= [spriteData[@"frames"] count])
            {
                sprite.visible = NO;
                continue;
            }
            NSDictionary *frameData = spriteData[@"frames"][currentFrame];
                
            if (frameData && ![frameData isKindOfClass:[NSNull class]])
            {
                NSDictionary *affineTransform = frameData[@"transform"];
                sprite.visible = YES;
                CGAffineTransform transform = CGAffineTransformMake([affineTransform[@"a"] doubleValue],
                                                                    [affineTransform[@"b"] doubleValue],
                                                                    [affineTransform[@"c"] doubleValue],
                                                                    [affineTransform[@"d"] doubleValue],
                                                                    [affineTransform[@"tx"] doubleValue],
                                                                    [affineTransform[@"ty"] doubleValue]);
                sprite.transform = transform;
                sprite.opacity = 255 * [frameData[@"alpha"] doubleValue];
                [sprite.parent reorderChild:sprite z:[frameData[@"zIndex"] doubleValue]];
            }
            else
            {
                sprite.visible = NO;
            }
        }
        
        if (++currentFrame == totalFrames) currentFrame = 0;
    };
    
    [self runAction:[CCRepeatForever actionWithAction:
                     [CCSequence actionOne:[CCCallBlock actionWithBlock:rigAnimationBlock]
                                       two:[CCDelayTime actionWithDuration:1 / 6]]]];

}

+(id)characterWithName:(NSString *)name
{
    return [[[self alloc] initWithCharacterName:name] autorelease];
}

+(id)characterWithName:(NSString *)name andInitialAnimation:(SPLGameCharacterAnimation)animation
{
    return [[[self alloc] initWithCharacterName:name andInitialAnimation:animation] autorelease];
}

-(void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    
    [Event_SteppedOn release];
    [Event_IsWithinRadius release];
    [Event_IsWithinDiagonalRadius release];
    
    [_charactersFollowing release];
    [lastSteppedOn release];
    [cliffsSteppedOn release];
    [_characterName release];
    [animationData release];
    [super dealloc];
}

@end
