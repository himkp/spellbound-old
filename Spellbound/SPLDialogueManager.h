//
//  SPLDialogueManager.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPLDialogueSpeaker : NSObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSDictionary* moods;

-(id)initWithName:(NSString*)name;
-(id)initWithName:(NSString *)name andMoods:(NSDictionary*)moods;

+(id)speakerWithName:(NSString*)name;
+(id)speakerWithName:(NSString *)name andMoods:(NSDictionary*)moods;

-(NSString*)avatarFilenameForMood:(NSString*)mood;

@end

@interface SPLDialogue : NSObject

@property (nonatomic, readonly) SPLDialogueSpeaker *speaker;
@property (nonatomic, retain) NSString* speakerName;
@property (nonatomic, retain) NSString* text;
@property (nonatomic, retain) NSString* currentMoodOfSpeaker;

-(id)initWithSpeakerName:(NSString*)speaker;
-(id)initWithText:(NSString*)text;
-(id)initWithSpeakerName:(NSString*)speaker andText:(NSString*)text;

+(id)dialogueWithSpeakerName:(NSString*)speaker;
+(id)dialogueWithText:(NSString*)text;
+(id)dialogueWithSpeakerName:(NSString*)speaker andText:(NSString*)text;

@end

@interface SPLDialogueSequence : NSObject

@property (nonatomic, retain) NSArray* dialogues;
@property (nonatomic, retain) NSString* title;

-(id)initWithTitle:(NSString*)title;
-(id)initWithTitle:(NSString *)title andDialogues:(NSArray*)dialogues;

+(id)dialogueSequenceWithTitle:(NSString*)title;
+(id)dialogueSequenceWithTitle:(NSString *)title andDialogues:(NSArray*)dialogues;

-(SPLDialogue*)dialogueAtIndex:(NSUInteger)index;

@end

@interface SPLDialogueManager : NSObject {
@private
    NSMutableDictionary* dialogueSequences;
    NSMutableDictionary* speakers;
}

-(SPLDialogueSequence*)dialogueSequenceWithTitle:(NSString*)title;
-(void)addDialogueSequence:(SPLDialogueSequence*)dialogueSequence;

-(SPLDialogueSpeaker*)speakerWithName:(NSString*)name;
-(void)addSpeaker:(SPLDialogueSpeaker*)speaker;

@end

@interface SPLMessageManager : NSObject
{
    NSMutableDictionary *messages;
}

-(NSString *)messageWithTitle:(NSString *)title;
-(void)addMessage:(NSString *)message withTitle:(NSString *)title;

@end
