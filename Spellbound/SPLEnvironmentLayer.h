//
//  SPLEnvironmentLayer.h
//  Spellbound
//
//  Created by Fleon Games on 6/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLEnvironmentLayer : CCLayer
{
    CCParticleSystem *spriteEmitterSnow;
    
    CGPoint positionOfEmitterSnow;
}

-(void)initializePositions;

@end
