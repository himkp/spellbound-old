//
//  NSString+Scrambler.h
//  Spellbound
//
//  Created by Fleon Games on 6/15/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Spellbound)

- (NSString *)scramble;

-(NSUInteger)pointValue;

-(NSString *)stringByReplacingVariables;

@end
