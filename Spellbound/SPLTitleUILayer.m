//
//  SPLPrimaryUILayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SPLTitleUILayer.h"
#import "SPLTitleScene.h"

@implementation SPLTitleUILayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfBackground            = fl_ccp(   0,    0, 2048, 1536);
        positionOfBag                   = fl_ccp( 803,  958,  420,  258);
        positionOfBagGlowing            = fl_ccp( 638,  473,  801,  806);
        positionOfButtonStart           = fl_ccp( 673, 1130,  713,  388);
        positionOfButtonCredits         = fl_ccp(   0, 1091,  409,  205);
        positionOfButtonSettings        = fl_ccp(   0, 1296,  409,  222);
        positionOfButtonSound           = fl_ccp(1622, 1314,  426,  222);
        positionOfLettersInitially      = fl_ccp( 921,  960,  231,  286);
        
        positionOfLetters[0]            = fl_ccp( 463,  135,  244,  261),     // S
        positionOfLetters[1]            = fl_ccp( 673,   92,  228,  240),     // P
        positionOfLetters[2]            = fl_ccp( 870,   42,  247,  267),     // E
        positionOfLetters[3]            = fl_ccp(1107,   92,  235,  240),     // L
        positionOfLetters[4]            = fl_ccp(1307,  137,  242,  256),     // L
        positionOfLetters[5]            = fl_ccp( 534,  380,  232,  269),     // B
        positionOfLetters[6]            = fl_ccp( 693,  293,  244,  281),     // O
        positionOfLetters[7]            = fl_ccp( 904,  293,  242,  264),     // U
        positionOfLetters[8]            = fl_ccp(1107,  293,  257,  295),     // N
        positionOfLetters[9]            = fl_ccp(1314,  355,  231,  286);     // D
        
        positionOfEmitterSnow           = fl_ccp(1024,    0,    0,    0);
        positionOfEmitterGlitter        = fl_ccp(1040, 1110,    0,    0);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfBackground            = fl_ccp(   0,    0,  960,  640);
        positionOfBag                   = fl_ccp( 382,  405,  187,  114);
        positionOfBagGlowing            = fl_ccp( 317,  200,  342,  356);
        positionOfButtonStart           = fl_ccp( 324,  471,  325,  169);
        positionOfButtonCredits         = fl_ccp(   0,  438,  209,   96);
        positionOfButtonSettings        = fl_ccp(   0,  534,  209,   96);
        positionOfButtonSound           = fl_ccp( 759,  521,  201,  119);
        positionOfLettersInitially      = fl_ccp( 433,  405,  104,  124);
        
        positionOfLetters[0]            = fl_ccp( 234,   50,  106,  117),
        positionOfLetters[1]            = fl_ccp( 324,   30,  102,  103),
        positionOfLetters[2]            = fl_ccp( 419,   13,  100,  115),
        positionOfLetters[3]            = fl_ccp( 516,   30,  105,  108),
        positionOfLetters[4]            = fl_ccp( 603,   51,  107,  114),
        positionOfLetters[5]            = fl_ccp( 258,  158,  117,  125),
        positionOfLetters[6]            = fl_ccp( 333,  119,  109,  139),
        positionOfLetters[7]            = fl_ccp( 426,  119,  108,  122),
        positionOfLetters[8]            = fl_ccp( 516,  119,  114,  136),
        positionOfLetters[9]            = fl_ccp( 607,  147,  104,  124);
        
        positionOfEmitterSnow           = fl_ccp( 480,    0,    0,    0);
        positionOfEmitterGlitter        = fl_ccp( 486,  450,    0,    0);
    }
}

-(id) init
{
    if (self=[super init])
    {
        [self initializePositions];
        
        NSUInteger currentVolume = [Spellbound audioPlayer].volume;
        if (currentVolume < 16.67) currentVolume = 0;
        else if (currentVolume >= 16.67 && currentVolume < 50) currentVolume = 33;
        else if (currentVolume >= 50 && currentVolume < 83.33) currentVolume = 66;
        else currentVolume = 100;
        
        self.isTouchEnabled = YES;
        
        spriteBackground = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Background.png"];
        spriteBackground.position = positionOfBackground;
        [self addChild:spriteBackground z:0];
        
        char* letterChars[10] = {"S", "P", "E", "L", "L2", "B", "O", "U", "N", "D"};
        
        int i = 0;
        for (; i < 10; i++)
        {
            spriteLetters[i] = [CCSprite spriteWithSpriteFrameName:
                          [NSString stringWithFormat:@"MainScreen_Letter_%s.png", letterChars[i]]];
            spriteLetters[i].position = positionOfLettersInitially;
            spriteLetters[i].opacity = 0;
            
            [self addChild:spriteLetters[i] z:i + 1];
        }
        
        spriteBagGlowing = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Bag_Glowing.png"];
        spriteBagGlowing.position = positionOfBagGlowing;
        [self addChild:spriteBagGlowing];
        
        _spriteButtonStart = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Button_Start.png"];
        _spriteButtonStart.position = positionOfButtonStart;
        _spriteButtonStart.visible = NO;
        [self addChild:_spriteButtonStart z:92];
        
        _spriteButtonCredits = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Button_Credits.png"];
        _spriteButtonCredits.position = positionOfButtonCredits;
        _spriteButtonCredits.visible = NO;
        [self addChild:_spriteButtonCredits z:93];
        
        _spriteButtonSettings = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Button_Settings.png"];
        _spriteButtonSettings.position = positionOfButtonSettings;
        _spriteButtonSettings.visible = NO;
        [self addChild:_spriteButtonSettings z:94];
        
        _spriteButtonSound = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"MainScreen_Button_Sound_%d.png", currentVolume]];
        _spriteButtonSound.position = positionOfButtonSound;
        _spriteButtonSound.visible = NO;
        [self addChild:_spriteButtonSound z:95];
        
        currentLetter = 0;
        [self schedule:@selector(animateLetters:) interval:0.25 repeat:9 delay:1];
        
        [spriteBagGlowing runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actions:
           [CCFadeTo actionWithDuration:1 opacity: .2 * 255],
           [CCFadeTo actionWithDuration:1 opacity: .8 * 255],
           nil]]];
        
        /** figure out the particle effects later **/
        spriteEmitterSnow = [CCParticleSnow node];
        
        [self addChild:spriteEmitterSnow z:99];
        
        spriteEmitterSnow.position = positionOfEmitterSnow;
        spriteEmitterSnow.life = 6;
        spriteEmitterSnow.lifeVar = 1;
        
        spriteEmitterSnow.gravity = ccp(0, -10);
        spriteEmitterSnow.speed = DEVICE_IS_IPAD ? 70 : 32;
        spriteEmitterSnow.speedVar = DEVICE_IS_IPAD ? 30 : 14;
        
        ccColor4F startColor = spriteEmitterSnow.startColor;
        startColor.r = 0.9f;
        startColor.g = 0.9f;
        startColor.b = 0.9f;
        spriteEmitterSnow.startColor = startColor;
        
        ccColor4F startColorVar = spriteEmitterSnow.startColorVar;
        startColorVar.b = 0.1f;
        spriteEmitterSnow.startColorVar = startColorVar;
        
        spriteEmitterSnow.emissionRate = spriteEmitterSnow.totalParticles / spriteEmitterSnow.life / 8;
        
        spriteEmitterSnow.texture = [[CCTextureCache sharedTextureCache] addImage:@"Assets/Common/Snow.png"];
        
        spriteEmitterGlitter = [CCParticleFireworks node];
        
        [self addChild:spriteEmitterGlitter z:90];
        
        spriteEmitterGlitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"Assets/Common/Stars.png"];
        spriteEmitterGlitter.position = positionOfEmitterGlitter;
        spriteEmitterGlitter.blendAdditive = YES;
        spriteEmitterGlitter.life = DEVICE_IS_IPAD ? 1 : 0.5;
        spriteEmitterGlitter.lifeVar = DEVICE_IS_IPAD ? 0.5 : 0.25;
        spriteEmitterGlitter.scale = DEVICE_IS_IPAD ? 2 : 1;
        spriteEmitterGlitter.speed = 70;
        spriteEmitterGlitter.speedVar = 30;
        spriteEmitterGlitter.posVar = ccp(20, 0);
        
        spriteBag = [CCSprite spriteWithSpriteFrameName:@"MainScreen_Bag.png"];
        spriteBag.position = positionOfBag;
        [self addChild:spriteBag z:91];
        
        [[Spellbound audioPlayer] playTrack:@"01_TitleTheme.mp3"];
     }
    return self;
}

-(void) animateLetters:(ccTime)dt
{
    CCSprite *spriteLetter = spriteLetters[currentLetter];
    
    CGPoint point = positionOfLetters[currentLetter];
    NSLog(@"point: %@", NSStringFromCGPoint(point));
    
    [spriteLetter runAction:
     [CCEaseBackOut actionWithAction:
      [CCMoveTo actionWithDuration:1 position:point]]];
    [spriteLetter runAction:
     [CCSequence actions:
      [CCFadeIn actionWithDuration:1], 
      [CCCallFuncND actionWithTarget:self 
                            selector:@selector(teeterLetter:number:) 
                                data:(int *)currentLetter++],
      nil]];
}

-(void) teeterLetter:(id)sender number:(int)index
{
    // handle the teeter animation here
    // schedule a timer at interval N
    // to create an action that goes to position (x1, y1)
    // if the position is close to (x1, y1) move to (x2, y2)
    // and vice versa
    // much like the magic bag glow animation
    
    CCSprite *spriteLetter = spriteLetters[index];
    
    if (index == 9 && !_spriteButtonStart.visible)
        [self show:nil sprite:_spriteButtonStart];
    
    CGPoint pt = ccpRatio(positionOfLettersInitially, positionOfLetters[index], 0.97f);
    //CGPoint compPt = [FL translatePoint:pt];
    
    if (spriteLetter.position.x == pt.x && spriteLetter.position.y == pt.y)
        pt = ccpRatio(positionOfLettersInitially, positionOfLetters[index], 1.03f);
    
    [spriteLetter runAction:
     [CCSequence actions:
      [CCMoveTo actionWithDuration:1 
                          position:pt],
      [CCCallFuncND actionWithTarget:self
                            selector:@selector(teeterLetter:number:) 
                                data:(int *)index],
      nil]];
}

-(void) show:(id)sender sprite:(CCSprite*)sprite
{
    CCSprite* nextSprite;
    
    sprite.scale = 0;
    sprite.visible = YES;
    
    if (sprite == nil) return;
    if (sprite == _spriteButtonStart) nextSprite = _spriteButtonCredits;
    if (sprite == _spriteButtonCredits) nextSprite = _spriteButtonSettings;
    if (sprite == _spriteButtonSettings) nextSprite = _spriteButtonSound;
    if (sprite == _spriteButtonSound) nextSprite = nil;
    
    [sprite runAction:
     [CCSequence actions:
      [CCEaseBackOut actionWithAction:
       [CCScaleTo actionWithDuration:0.25 scale:1]],
      [CCCallFuncND actionWithTarget:self
                            selector:@selector(show:sprite:)
                                data:nextSprite],
      nil]];
    
    return;
    
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [[CCDirector sharedDirector] touchDispatcher];
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if ([FL doesSprite:_spriteButtonStart containPoint:point]) {
        [FL with:_spriteButtonStart runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [((SPLTitleScene*) self.parent).startGameMenuLayer show];
        return YES;
    } else if ([FL doesSprite:_spriteButtonCredits containPoint:point]) {
        [FL with:_spriteButtonCredits runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [((SPLTitleScene*) self.parent).creditsPanelLayer show];
        return YES;
    } else if ([FL doesSprite:_spriteButtonSettings containPoint:point]) {
        [FL with:_spriteButtonSettings runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [((SPLTitleScene*) self.parent).settingsPanelLayer show];
        return YES;
    } else if ([FL doesSprite:_spriteButtonSound containPoint:point]) {
        [FL with:_spriteButtonSound runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [self toggleVolume];
        NSLog(@"You touched the sound button");
        return YES;
    }
    return NO;
}

-(void)toggleVolume
{
    NSString *currentVolumeString = [[Spellbound localStorage] dataForKey:@"CurrentVolume"];
    NSInteger currentVolume;
    currentVolume = currentVolumeString ? currentVolumeString.integerValue : 100;
    if (currentVolume == 100)
        currentVolume = 66;
    else if (currentVolume == 0)
        currentVolume = 100;
    else currentVolume -= 33;
    
    [Spellbound audioPlayer].volume = currentVolume;
    
    NSString *buttonName = [NSString stringWithFormat:@"MainScreen_Button_Sound_%d.png", currentVolume];
    [_spriteButtonSound setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:buttonName]];
}

-(CGPoint *)letterPoints
{
    return positionOfLetters;
}


@end
