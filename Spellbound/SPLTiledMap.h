//
//  SPLTilemap.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLLetterBlock.h"
#import "FLAStarPathfinder.h"
#import "SPLLetterBlockSprite.h"

@class SPLGameCharacter, SPLInteractiveItem;

@interface SPLTileDescription : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic) SPLTileType type;

-(id)initWithName:(NSString *)name andType:(SPLTileType)type;
+(id)tileDescriptionWithName:(NSString *)name andType:(SPLTileType)type;

@end

@interface SPLTiledMap : CCTMXTiledMap <FLAStarPathfinderDelegate, NSCoding> {
    NSMutableDictionary *tileTypesCache;
    
    CGPoint currentlyConnectedPoints[2];
    
    BOOL __selectorInProgress;
    NSMutableDictionary *connectorPathCache;
    
    NSMutableArray *landsBridged;
    
    NSMutableArray *_interactiveObjects;
}

-(id)initWithMapName:(NSString *)name;
+(id)tiledMapWithMapName:(NSString *)name;

-(SPLTileDescription *)tileDescriptionAt:(CGPoint)point;
-(SPLTileType)tileTypeAt:(CGPoint)point;
-(void)removeLetterAtPoint:(CGPoint)point;
-(BOOL)doesLetterExistAtPoint:(CGPoint)point;
-(NSString*)getLetterAtPoint:(CGPoint)point;
-(SPLLetterBlock*)getLetterBlockAtPoint:(CGPoint)point;
-(BOOL)canLetterBeAddedAtPoint:(CGPoint)point;
-(NSArray *)freezeLetters:(NSString **)errorMessage;
-(void)highlightTileUnderPoint:(CGPoint)point;

-(void)addLetterSprite:(SPLLetterBlockSprite *)letterSprite atPoint:(CGPoint)point;

-(void)createConnectorFromSource:(CGPoint)sourcePoint toDestination:(CGPoint)destinationPoint;
-(void)removeConnectors;

-(NSDictionary *)interactiveObjectUnderPoint:(CGPoint)point;
-(SPLGameCharacter *)characterUnderPoint:(CGPoint)point;

-(CGRect)boundingBoxOfCharacterNamed:(NSString *)characterName;
-(CGRect)boundingBoxOfInteractiveObjectNamed:(NSString *)interactiveObjectName;

-(CGPoint)convertTilePositionToPosition:(CGPoint)tilePosition;
-(CGPoint)convertPositionToTilePosition:(CGPoint)position;

@property (nonatomic, readonly) NSMutableDictionary *letterBlocks;
@property (nonatomic, readonly) CCSprite *lettersLayer;
@property (nonatomic, readonly) CCSprite *charactersLayer;
@property (nonatomic, readonly) CCSprite *connectorLayer;

@property (nonatomic, readonly) NSMutableArray *connectorPath;
@property (nonatomic, readonly) SPLGameCharacter *mainCharacter;
@property (nonatomic, readonly) NSMutableArray *characters; // only NPC characters (excluding the main character)
@property (nonatomic, readonly) NSMutableArray *interactiveObjects;

// position of the main character just before the map is unloaded
@property (nonatomic, readonly) CGPoint mainCharacterPosition;

@property (nonatomic, readonly) NSString *mapName;

@end
