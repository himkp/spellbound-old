//
//  SPLLetterPileLayer.m
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


// create an array of letter sprites using the tiled map
// fill in the letters from bottom
// on touch, the letter should shrink, and grow on release
// be able to drag and rearrange the letters
// be able to drop the letters onto the tilemap
// applicable areas light up in green

#import "SPLLetterPileLayer.h"
#import "SPLLetterBlockSprite.h"
#import "SPLGameScene.h"
#import "SPLGameCharacter.h"

@implementation SPLLetterPileLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfThePanel = fl_ccp(0, 1536, 0, 0);
        positionOfBackground = fl_ccp(120, 1180, 1820, 390);
        
        positionOfBorderLeft = fl_ccp(0, 1075, 176, 461);
        positionOfBorderRight = fl_ccp(1679, 1048, 369, 488);
        positionOfBorderTop = fl_ccp(176, 1095, 1503, 149);
        positionOfButtonFreeze = fl_ccp(1388, 1312 - 64, 397, 148);
        
        positionOfFirstLetterInHand = fl_ccp(160, 1312, 128, 256);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfThePanel = fl_ccp(0, 640, 0, 0);
        positionOfBackground = fl_ccp(56, 480, 853, 170);
        
        positionOfBorderLeft = fl_ccp(5, 423, 77, 217);
        positionOfBorderRight = fl_ccp(813, 411, 140, 229);
        positionOfBorderTop = fl_ccp(82, 432, 731, 69);
        positionOfButtonFreeze = fl_ccp(660, 535 - 15, 188, 73);
        
        positionOfFirstLetterInHand = fl_ccp(73 + 20, 535 + 30, 60, 120);
    }
}

-(void)initializeSprites
{
    [self setIsTouchEnabled:YES];
    
    _spriteThePanel = [CCSprite node];
    _spriteThePanel.position = positionOfThePanel;
    
    spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
    spriteBackground.scaleX = DEVICE_IS_IPAD ? 910 : 426;
    spriteBackground.scaleY = DEVICE_IS_IPAD ? 160 : 75;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY)
    {
        spriteBackground.scaleX *= 2;
        spriteBackground.scaleY *= 2;
    }
    spriteBackground.position = positionOfBackground;
    
    spriteBorderLeft = [CCSprite spriteWithSpriteFrameName:@"LetterPile_Border_Left.png"];
    spriteBorderLeft.position = positionOfBorderLeft;
    
    spriteBorderTop = [CCSprite spriteWithSpriteFrameName:@"LetterPile_Border_Top.png"];
    spriteBorderTop.position = positionOfBorderTop;
    
    spriteBorderRight = [CCSprite spriteWithSpriteFrameName:@"LetterPile_Border_Right.png"];
    spriteBorderRight.position = positionOfBorderRight;
    
    _spriteButtonFreeze = [CCSprite spriteWithSpriteFrameName:@"LetterPile_Button_Freeze.png"];
    _spriteButtonFreeze.position = positionOfButtonFreeze;
    
    [_spriteThePanel addChild:spriteBackground z:-100];
    [_spriteThePanel addChild:spriteBorderTop];
    [_spriteThePanel addChild:spriteBorderRight];
    [_spriteThePanel addChild:spriteBorderLeft];
    [_spriteThePanel addChild:_spriteButtonFreeze];
    
    [self addChild:_spriteThePanel];
    
    self.visible = NO;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        // when you stop dragging, StopDraggingSelectedLetter action is run
        // it finishes the StartDraggingSelectedLetter action and then
        // forwards the action to CoreGameplayLayer
        // CoreGameplayLayer checks whether the letter is on a valid tile or not and
        // fires either RemoveLetterFromPile -> AddLetterToTiledMapAtHighlightedPoint if valid
        // or fires ReturnLetterBackToPile, both of which are forwarded back to 
        // LetterPileLayer.
        
        [self initializePositions];
        [self initializeSprites];
        
        // 500 letters: enough to last the demo
        // todo: make this thing infinite
        _letterPile = [[SPLLetterPile letterPileWithNumberOfLettersInHand:9 andInBag:500] retain];
        _arrayOfLetterBlockSpritesInHand = [[NSMutableArray alloc] initWithCapacity:9];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"LetterPile"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"LetterPile"];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        [self initializePositions];
        [self initializeSprites];
        
        if ([decoder decodeBoolForKey:@"visible"]) [self show];
        _letterPile = [[decoder decodeObjectForKey:@"letterPile"] retain];
        _arrayOfLetterBlockSpritesInHand = [[NSMutableArray alloc] initWithCapacity:9];
        
        // add back the letters in hand that previously existed
        for (NSUInteger i = 0; i < _letterPile.lettersInHand.length; i++)
        {
            NSString *letter = [NSString stringWithFormat:@"%c", [_letterPile.lettersInHand characterAtIndex:i]];
            [Action_AddLetterToPile runWithData:letter];
        }
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"LetterPile"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"LetterPile"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeBool:self.visible forKey:@"visible"];
    [encoder encodeObject:_letterPile forKey:@"letterPile"];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_HideLetterPile])
    {
        [self hide: ^{
            [action finish];
        }];
    }
    else if ([action is:Action_ShowLetterPile])
    {
        [self show: ^{
            [action finish];
        }];
    }
    else if ([action is:Action_AddLettersToBag])
    {
        [_letterPile prependLettersInBag:action.data];
        [action finish];
    }
    else if ([action is:Action_AddLetterToPile] || [action is:Action_AddLetterToPileFrom])
    {
        NSString *letter;
        SPLLetterBlockSprite *sprite;
        int i = _arrayOfLetterBlockSpritesInHand.count;
        if ([action is:Action_AddLetterToPileFrom])
        {
            NSArray *actionData = [action.data componentsSeparatedByString:@","];
            SPLTiledMap *tiledMap = [(SPLGameScene *) parent_ coreGameplayLayer].tiledMap;
            
            letter = [actionData[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            sprite = [SPLLetterBlockSprite letterBlockSpriteWithLetter:letter];
            if ([actionData[1] isEqualToString:@"Player"])
            {
                // TODO: this does not work
                CGPoint position = tiledMap.mainCharacter.position;
                position = ccpAdd(position, ccp(tiledMap.tileSize.width / 2, tiledMap.tileSize.height / 2));
                sprite.position = [tiledMap convertToWorldSpace:position];
            }
            else
            {
                NSString *interactiveObjectName = [actionData[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *object = [[[(SPLGameScene *) parent_ coreGameplayLayer].tiledMap objectGroupNamed:@"InteractiveObjects"] objectNamed:interactiveObjectName];
                float x = ((NSString *) object[@"x"]).floatValue;
                float y = ((NSString *) object[@"y"]).floatValue;
                float w = ((NSString *) object[@"width"]).floatValue;
                float h = ((NSString *) object[@"height"]).floatValue;
                sprite.position = [tiledMap convertToWorldSpace:ccp(x + w / 2, y + h / 2)];
            }
            
            sprite.scale = 0;
            
            CGPoint targetPosition = ccp(positionOfFirstLetterInHand.x + i * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y);
            [sprite runAction:[CCScaleTo actionWithDuration:0.15 scale:1.0]];
            [sprite runAction:[CCEaseBackInOut actionWithAction:[CCMoveTo actionWithDuration:0.25 position:targetPosition]]];
        }
        else
        {
            letter = action.data;
            sprite = [SPLLetterBlockSprite letterBlockSpriteWithLetter:letter];
            sprite.position = ccp(positionOfFirstLetterInHand.x + i * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), -positionOfFirstLetterInHand.y - sprite.tileSize.height);
            
            CGPoint targetPosition = ccp(positionOfFirstLetterInHand.x + i * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y);
            [sprite runAction:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.25 position:targetPosition]]];
        }
        
        sprite.anchorPoint = ccp(0.5, 0.25);
        sprite.indexInLetterPile = i;
        
        [_spriteThePanel addChild:sprite z:100];
        [_arrayOfLetterBlockSpritesInHand addObject:sprite];
        [action willFinishAfter:0.30];
    }
    else if ([action is:Action_RefillLettersInHand])
    {
        if (_isAutoRefillDisabled) {
            [action finish];
            return;
        }
        int i = _arrayOfLetterBlockSpritesInHand.count;
        [_letterPile refillLettersInHand];
        for (; i < _letterPile.numberOfLettersInHand; i++)
        {
            NSString* letter = [_letterPile.lettersInHand substringWithRange:(NSRange) {i, 1}];
            SPLLetterBlockSprite* sprite = [SPLLetterBlockSprite letterBlockSpriteWithLetter:letter];
            
            sprite.position = ccp(positionOfFirstLetterInHand.x + i * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), -positionOfFirstLetterInHand.y - sprite.tileSize.height);
            sprite.anchorPoint = ccp(0.5, 0.25);
            sprite.indexInLetterPile = i;
            
            [sprite runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:i * 0.15]
                                                two:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.25
                                                                        position:ccp(positionOfFirstLetterInHand.x + i * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y)]]]];
            
            [_spriteThePanel addChild:sprite z:100];
            
            [_arrayOfLetterBlockSpritesInHand addObject:sprite];
        }
        
        [action willFinishAfter:i * 0.15 + 0.25];
    }
    else if ([action is:Action_StartDraggingSelectedLetter])
    {
        if (!currentActions)
        {
            currentActions = [[NSMutableArray alloc] init];
        }
        [currentActions addObject:action];
    }
    else if ([action is:Action_ContinueDraggingSelectedLetterAtPoint])
    {
        CGPoint point = [(NSValue *) action.data[@"point"] CGPointValue];
        CGPoint touchDistance = [(NSValue *) action.data[@"touchDistance"] CGPointValue];
        //NSLog(@"continue dragging letter, Point is: %@", NSStringFromCGPoint(point));
        
        if (!currentActions || ![currentActions count]) return;
        
        SPLLetterBlockSprite *selectedLetterSprite = (SPLLetterBlockSprite *) action.data[@"sprite"];
        CGPoint tileHeightDistance = ccp(0, selectedLetterSprite.tileSize.height / CC_CONTENT_SCALE_FACTOR());
        selectedLetterSprite.position = ccpSub(ccpAdd(point, tileHeightDistance), touchDistance);
        [_spriteThePanel reorderChild:selectedLetterSprite z:200];
        
        NSMutableArray *selectedLetterSprites = [NSMutableArray array];
        for (FLAction *currentAction in currentActions)
        {
            [selectedLetterSprites addObject:currentAction.data[@"sprite"]];
        }
        
        // if point.x is between (x) and (x+w) of any of the sprites
        // shift all items starting at (i) to the right
        // and bring the current item at position (i)
        
        // if point.y is less than (y) of panel
        // remove the item from the array and shift next elements to left
        if (point.y <- spriteBorderRight.position.y + spriteBorderRight.boundingBox.size.height)
        {
            FLAction *unhighlightAction = Action_UnhighlightHighlightedTile;
            unhighlightAction.data = @{@"point": [NSValue valueWithCGPoint:ccpSub(point, touchDistance)], @"touch": action.data[@"touch"]};
            [unhighlightAction run];
            for (SPLLetterBlockSprite *sprite in _arrayOfLetterBlockSpritesInHand)
            {
                if ([selectedLetterSprites containsObject:sprite]) continue;
                float spriteX = positionOfFirstLetterInHand.x + sprite.indexInLetterPile * sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR();
                if (point.x > spriteX - sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR() / 2 && point.x < spriteX + sprite.tileSize.width / CC_CONTENT_SCALE_FACTOR() / 2)
                {
                    NSUInteger indexOfSprite = sprite.indexInLetterPile;
                    [_arrayOfLetterBlockSpritesInHand removeObject:selectedLetterSprite];
                    [_arrayOfLetterBlockSpritesInHand insertObject:selectedLetterSprite atIndex:indexOfSprite];
                    break;
                }
            }
        }
        else {
            FLAction *tileHighlightAction = Action_HighlightTileUnderPoint;
            tileHighlightAction.data = @{@"point":[NSValue valueWithCGPoint:ccpSub(point, touchDistance)], @"touch": action.data[@"touch"]};
            [tileHighlightAction run];
            
            [_arrayOfLetterBlockSpritesInHand removeObject:selectedLetterSprite];
            [_arrayOfLetterBlockSpritesInHand insertObject:selectedLetterSprite atIndex:_arrayOfLetterBlockSpritesInHand.count];
        }
        for (int i = 0; i < _arrayOfLetterBlockSpritesInHand.count; i++)
        {
            SPLLetterBlockSprite *theSprite = (SPLLetterBlockSprite*) _arrayOfLetterBlockSpritesInHand[i];
            theSprite.indexInLetterPile = i;
            if ([selectedLetterSprites containsObject:theSprite]) continue;
            CCAction *previousAction = [theSprite getActionByTag:i + 10];
            if (previousAction == nil || previousAction.isDone)
            {
                CCAction *theAction = [CCMoveTo actionWithDuration:0.15 
                                                          position:ccp(positionOfFirstLetterInHand.x + i * theSprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(),
                                                                       positionOfFirstLetterInHand.y)]; 
                theAction.tag = i + 10;
                [theSprite runAction:theAction];
            }
        }
        
        [action finish];
    }
    else if ([action is:Action_RemoveLetterFromPile])
    {
        [_arrayOfLetterBlockSpritesInHand removeObject: action.data];
        [_spriteThePanel removeChild:(CCNode*) action.data cleanup:YES];
        
        [_letterPile.lettersInHand setString:@""];
        for (SPLLetterBlockSprite *sprite in _arrayOfLetterBlockSpritesInHand)
            [_letterPile.lettersInHand setString:[_letterPile.lettersInHand stringByAppendingString:sprite.letter]];
        
        [action finish];
        
        // this is done just to reset the location of letters in the pile
        // or who knows it might not even be needed
        // lets just comment it for a while
        // [Action_ReturnLetterBackToPile run];
    }
    else if ([action is:Action_ReturnLetterBackToPile])
    {
        if (action.data == nil)
        {
            for (int i = 0; i < _arrayOfLetterBlockSpritesInHand.count; i++)
            {
                SPLLetterBlockSprite *theSprite = (SPLLetterBlockSprite*) _arrayOfLetterBlockSpritesInHand[i];
                [theSprite stopAllActions];
                [FL with:theSprite runAnimationAction:kFLAnimationActionRestoreSize];
                [theSprite runAction:[CCMoveTo actionWithDuration:0.15
                                                         position:ccp(positionOfFirstLetterInHand.x + i * theSprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y)]];
            }
            return [action finish];
        }
        
        SPLLetterBlockSprite *selectedLetterBlockSprite = (SPLLetterBlockSprite *) action.data;
        
        for (int i = 0; i < _arrayOfLetterBlockSpritesInHand.count; i++)
        {
            SPLLetterBlockSprite *theSprite = (SPLLetterBlockSprite*) _arrayOfLetterBlockSpritesInHand[i];
            [theSprite stopAllActions];
            [FL with:theSprite runAnimationAction:kFLAnimationActionRestoreSize];
            if (theSprite == selectedLetterBlockSprite)
                [theSprite runAction:[CCEaseBackOut actionWithAction:[CCMoveTo actionWithDuration:0.5
                                                                                         position:ccp(positionOfFirstLetterInHand.x + i * theSprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y)]]];
            else 
                [theSprite runAction:[CCMoveTo actionWithDuration:0.15
                                                         position:ccp(positionOfFirstLetterInHand.x + i * theSprite.tileSize.width / CC_CONTENT_SCALE_FACTOR(), positionOfFirstLetterInHand.y)]];
        }
        
        [_letterPile.lettersInHand setString:@""];
        for (SPLLetterBlockSprite *sprite in _arrayOfLetterBlockSpritesInHand)
            [_letterPile.lettersInHand setString:[_letterPile.lettersInHand stringByAppendingString:sprite.letter]];
    }
    else
    {
        [action forwardToActionDelegate:[(SPLGameScene *) parent_ coreGameplayLayer]];
    }
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    
    [_letterPile release];
    [_arrayOfLetterBlockSpritesInHand release];
    
    [super dealloc];
}

-(void)show
{
    [self show:^{}];
}

-(void)show:(void (^)())onShow
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    self.visible = YES;
    
    [FL with:_spriteThePanel setPosition:ccp(0, winSize.height + spriteBorderRight.boundingBox.size.height)];
    [_spriteThePanel runAction:
     [CCEaseExponentialOut actionWithAction:
      [CCSequence actionOne:
       [FLMoveTo actionWithDuration:0.5 position:ccp(0, winSize.height + spriteBorderRight.boundingBox.size.height * (DEVICE_IS_IPAD ? 0.25 : 0.15))]
                        two:[CCCallBlock actionWithBlock:onShow]]]];
}

-(void)hide
{
    [self hide:^{}];
}

-(void)hide:(void (^)())onHide
{   
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    [_spriteThePanel runAction:
     [CCEaseExponentialIn actionWithAction:
      [FLMoveTo actionWithDuration:0.5 position:ccp(0, winSize.height + spriteBorderRight.boundingBox.size.height)]]];
    
    [self runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.5],
      [CCHide action],
      [CCCallBlock actionWithBlock:onHide],
      nil]];
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:10 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (self.isDisabled) return NO;
    
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (![self visible]) return NO;
    
    //NSLog(@"location of freeze button: %@, point: %@", NSStringFromCGRect(_spriteButtonFreeze.boundingBox), NSStringFromCGPoint(point));
    if ([FL doesSprite:_spriteButtonFreeze containPoint:point])
    {
        [FL with:_spriteButtonFreeze runAnimationAction:kFLAnimationActionShrinkAndGrow];
        
        [Action_FreezeFloatingLetters run];
        return YES;
    }
    
    for (CCSprite *letterBlockSprite in _arrayOfLetterBlockSpritesInHand)
    {
        if (CGRectContainsPoint(letterBlockSprite.boundingBox, point))
        {
            //NSLog(@"sprite bounding box: %@, point: %@", NSStringFromCGRect(letterBlockSprite.boundingBox), NSStringFromCGPoint(point));
            FLAction *action = Action_StartDraggingSelectedLetter;
            CGPoint touchDistance = [letterBlockSprite convertToNodeSpace:ccpSub(point, letterBlockSprite.anchorPointInPoints)];
            //NSLog(@"touch distance: %@, %@", NSStringFromCGPoint([letterBlockSprite convertToNodeSpace:point]), NSStringFromCGPoint(touchDistance));
            action.data = @{@"sprite": letterBlockSprite,
                            @"touch": touch,
                            @"touchDistance": [NSValue valueWithCGPoint:touchDistance]};
            [action run];
            return YES;
        }
    }
    
    if ([FL doesSprite:spriteBackground containPoint:point] ||
        [FL doesSprite:spriteBorderLeft containPoint:point] ||
        [FL doesSprite:spriteBorderRight containPoint:point] ||
        [FL doesSprite:spriteBorderTop containPoint:point])
        return YES;
    
    return NO;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    for (FLAction *action in currentActions)
    {
        if (action.data[@"touch"] == touch)
        {
            FLAction *nextAction = Action_ContinueDraggingSelectedLetterAtPoint;
            nextAction.data = @{@"sprite": action.data[@"sprite"],
                                @"touch": touch,
                                @"point":[NSValue valueWithCGPoint:point],
                                @"touchDistance":action.data[@"touchDistance"]};
            [nextAction run];
        }
    }
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (!currentActions || ![currentActions count])
    {
        [Action_StopDraggingSelectedLetter run];
        return;
    }
    
    // restore size
    SPLLetterBlockSprite *selectedLetterBlockSprite = nil;
    FLAction *currentAction;
    
    for (FLAction *action in currentActions)
    {
        if (action.data[@"touch"] == touch)
        {
            currentAction = action;
            selectedLetterBlockSprite = action.data[@"sprite"];
        }
    }
    //[FL with:selectedLetterBlockSprite runAnimationAction:kFLAnimationActionRestoreSize];
    if (!selectedLetterBlockSprite) return;
    
    [_spriteThePanel reorderChild:selectedLetterBlockSprite z:100];
    
    if (point.y <- spriteBorderRight.position.y + spriteBorderRight.boundingBox.size.height)
    {
        FLAction *action = Action_UnhighlightHighlightedTile;
        action.data = @{@"touch": touch, @"point": [NSValue valueWithCGPoint:point]};
        action.subsequentAction = Action_ReturnLetterBackToPile;
        action.subsequentAction.data = selectedLetterBlockSprite;
        [action run];
    }
    else
    {
        FLAction *action = Action_StopDraggingSelectedLetter;
        CGPoint tileHeightDistance = ccp(0, selectedLetterBlockSprite.tileSize.height / CC_CONTENT_SCALE_FACTOR());
        action.data = @{@"sprite": selectedLetterBlockSprite,
                        @"point": [NSValue valueWithCGPoint:ccpSub(point, tileHeightDistance)],
                        @"touch": touch,
                        @"touchDistance": currentAction.data[@"touchDistance"]};
        [action run];
    }
    
    [currentAction finish];
    [currentActions removeObject:currentAction];
}

@end
