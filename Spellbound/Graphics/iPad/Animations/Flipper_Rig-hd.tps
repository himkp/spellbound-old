<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <true/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d</string>
        <key>textureFileName</key>
        <filename>Flipper_Rig-hd.pvr.ccz</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">pvr2ccz</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>Flipper_Rig-hd.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>Flipper_Rig-hd.plist</filename>
        <key>multiPack</key>
        <false/>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/BoundingBox.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/flash.display.Shape.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Arm_Mid.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Beak_Excited.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Beak.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Body.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Cheeks.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_CheekShadow.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Eyes.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Foot.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Hair.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Head.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_HeadBlank.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_Scarf.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_ScarfHang_1.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_ScarfHang_2.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Down_ScarfShadow.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Shadow.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Arm.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Body.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Face.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Foot.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Hair1.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Hair2.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Head.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Scarf1.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Scarf2.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Scarf3.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_Scarf4.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_ScarfShadow1.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Side_ScarfShadow2.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_Arm.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_Body.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_Foot.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_Head.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_Scarf.png</filename>
            <filename>../../../../../../Dropbox/Spellbound/Resources/Animations in Code/out/Flippers/images-hd/Flippers_Up_ScarfShadow.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
