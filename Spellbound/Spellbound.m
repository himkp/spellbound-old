//
//  Spellbound.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLGlobalActionDelegate.h"
#import "SPLWordList.h"
#import "SPLDialogueManager.h"
#import "FLLocalStorage.h"
#import "FLActions.h"
#import "TestFlight.h"
#import "FLSimpleAudioPlayer.h"

NSString* SPLTileTypeToNSString (SPLTileType tileType)
{
    switch (tileType) {
        case SPLTileTypeLand: return @"[SPLTileTypeLand]"; break;
        case SPLTileTypeWater: return @"[SPLTileTypeWater]"; break;
        case SPLTileTypeCliff: return @"[SPLTileTypeCliff]"; break;
        case SPLTileTypeObstruction: return @"[SPLTileTypeObstruction]"; break;
        case SPLTileTypeLetterFloating: return @"[SPLTileTypeLetterFloating]"; break;
        case SPLTileTypeLetterFrozen: return @"[SPLTileTypeLetterFrozen]"; break;
    }
}

CGPoint fl_ccp(float x, float y, float width, float height)
{
    float screenHeight = DEVICE_IS_IPAD ? 768 : 320;
    return (CGPoint) {x / 2 + width / 4, screenHeight - y / 2 - height / 4};
}

@implementation Spellbound

static SPLGlobalActionDelegate* globalActionDelegate;
static SPLWordList* wordList;
static SPLDialogueManager* dialogueManager;
static SPLMessageManager *messageManager;
static SPLCheckPointManager *checkPointManager;
static FLLocalStorage* localStorage;
static FLActionSequenceManager* actionSequenceManager;
static FLActionDelegateManager *actionDelegateManager;
static FLEventDispatcherManager *eventDispatcherManager;
static NSMutableArray *variableList;
static FLSimpleAudioPlayer *audioPlayer;

+(void)begin
{
    globalActionDelegate = [[SPLGlobalActionDelegate alloc] init];
    wordList = [[SPLWordList alloc] init];
    dialogueManager = [[SPLDialogueManager alloc] init];
    messageManager = [[SPLMessageManager alloc] init];
    checkPointManager = [[SPLCheckPointManager alloc] init];
    localStorage = [[FLLocalStorage alloc] init];
    actionSequenceManager = [[FLActionSequenceManager alloc] init];
    actionDelegateManager = [[FLActionDelegateManager alloc] init];
    eventDispatcherManager = [[FLEventDispatcherManager alloc] init];
    variableList = [[NSMutableArray alloc] init];
    audioPlayer = [[FLSimpleAudioPlayer alloc] init];
    
    // hack
    variableList[0] = @"Player";
    
    //[TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
    [TestFlight takeOff:TESTFLIGHT_APP_TOKEN];
    
}

+(void)end
{
    [globalActionDelegate release];
    [wordList release];
    [dialogueManager release];
    [messageManager release];
    [checkPointManager release];
    [localStorage release];
    [actionSequenceManager release];
    [actionDelegateManager release];
    [variableList release];
    [audioPlayer release];
}

+(id<FLActionDelegate>)globalActionDelegate
{
    return globalActionDelegate;
}

+(SPLWordList *)wordList
{
    return wordList;
}

+(SPLDialogueManager*) dialogueManager
{
    return dialogueManager;
}

+(SPLMessageManager *)messageManager
{
    return messageManager;
}

+(SPLCheckPointManager *)checkPointManager
{
    return checkPointManager;
}

+(FLLocalStorage *)localStorage
{
    return localStorage;
}

+(FLActionSequenceManager *)actionSequenceManager
{
    return actionSequenceManager;
}

+(FLActionDelegateManager *)actionDelegateManager
{
    return actionDelegateManager;
}

+(FLEventDispatcherManager *)eventDispatcherManager
{
    return eventDispatcherManager;
}

+(NSMutableArray *)variableList
{
    return variableList;
}

+(FLSimpleAudioPlayer *)audioPlayer
{
    return audioPlayer;
}

@end
