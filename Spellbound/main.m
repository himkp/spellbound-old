//
//  main.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/4/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestFlight.h"

int main(int argc, char *argv[]) {
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal;
    @try
    {
        retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
    }
    @catch (NSException *exception)
    {
        TFLog(@"Gosh!!! %@", [exception callStackSymbols]);
        @throw;
    }
    [pool release];
    return retVal;
}

