//
//  SPLCheckPointManager.m
//  Spellbound
//
//  Created by Fleon Games on 1/22/14.
//
//

#import "SPLCheckPointManager.h"
#import "Spellbound.h"

@implementation SPLCheckPointManager

- (id)init
{
    self = [super init];
    if (self) {
        checkPoints = [[NSMutableDictionary alloc] init];
        checkPointStatuses = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(FLAction *)checkPointWithTitle:(NSString *)title
{
    return checkPoints[title];
}

-(NSArray *)activeCheckPoints
{
    __block NSMutableArray *activeCheckPoints = [NSMutableArray array];
    [checkPoints enumerateKeysAndObjectsUsingBlock:^(NSString *checkPointName, FLAction *checkPointAction, BOOL *stop) {
        // if checkpoint isn't active, return
        if (![checkPointStatuses[checkPointName] boolValue]) return;
        [activeCheckPoints addObject:checkPointName];
    }];
    NSLog(@"active checkPoints: %@", activeCheckPoints);
    return activeCheckPoints;
}

-(void)registerCheckPoint:(FLAction *)checkPointAction
{
    checkPoints[checkPointAction.data] = checkPointAction;
    checkPointStatuses[checkPointAction.data] = [NSNumber numberWithBool:NO];
}

-(void)activateCheckPoint:(FLAction *)checkPointAction
{
    // disable all checkpoints prior to this checkpoint
    for (FLAction *action = checkPointAction.priorAction; action; action = action.priorAction) {
        if (![action.name isEqualToString:@"CheckPoint"]) continue;
        // if checkpoint is activated, deactivate it and break out
        // since all previous checkpoints are expected to be deactivated already
        if ([checkPointStatuses[action.data] boolValue]) {
            checkPointStatuses[action.data] = [NSNumber numberWithBool:NO];
            break;
        }
    }
    // enable this checkpoint
    checkPointStatuses[checkPointAction.data] = [NSNumber numberWithBool:YES];
}

-(void)resumeFromCheckPointsTitled:(NSArray *)titles
{
    for (NSString *actionSequenceName in [Spellbound actionSequenceManager]->actionSequences.keyEnumerator)
    {
        // duplicate the action sequence to avoid unintended side effects
        FLActionSequence *actionSequence = [[Spellbound actionSequenceManager] actionSequenceWithName:actionSequenceName];
        for (FLAction *action = actionSequence.firstAction; action; action = action.subsequentAction)
            for (NSString *checkPointTitle in titles)
                if ([action.name isEqualToString:@"CheckPoint"] && [action.data isEqualToString:checkPointTitle] && action.subsequentAction)
                    // run the subsequent action since game is saved at a checkpoint action
                    // and we don't want to save all over again when loading a new game
                    // also, run with a delay of 1s so that all the layers in GameScene have been properly initialized
                    // (to prevent actions being lost in delegation to nil objects)
                    [action.subsequentAction performSelector:@selector(run) withObject:nil afterDelay:1];
    }
}

- (void)dealloc
{
    [checkPoints release];
    [super dealloc];
}

@end
