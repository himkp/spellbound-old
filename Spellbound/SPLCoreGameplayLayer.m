
//
//  SPLCoreGameplayLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLCoreGameplayLayer.h"
#import "SPLTiledMap.h"
#import "SPLGameCharacter.h"
#import "SPLGameScene.h"
#import "SPLInteractiveItem.h"

@interface SPLSpriteOceanWaves : CCSprite
{
    CGRect oceanWavesRect;
}

-(id)initWithRect:(CGRect)rect;
+(id)spriteWithRect:(CGRect)rect;
@end

@implementation SPLSpriteOceanWaves

+(id)spriteWithRect:(CGRect)rect
{
    return [[[self alloc] initWithRect:rect] autorelease];
}

-(id)initWithRect:(CGRect)rect
{
    self = [super init];
    if (self) {
        rect.origin = ccpSub(rect.origin, ccp(64, 64));
        rect.size.width += 64 * 2;
        rect.size.height += 64 * 2;
        
        oceanWavesRect = rect;
        
        self.position = rect.origin;
        
        CCLayerColor *background = [CCLayerColor layerWithColor:ccc4(177, 214, 255, 255)];
        CCSprite *waves = [CCSprite spriteWithFile:@"iPad/OceanWaves/Waves.png" rect:oceanWavesRect];
        CCSprite *lineSingleThin = [CCSprite spriteWithFile:@"iPad/OceanWaves/LineSingleThin.png" rect:oceanWavesRect];
        CCSprite *linesThin = [CCSprite spriteWithFile:@"iPad/OceanWaves/LinesThin.png" rect:oceanWavesRect];
        
        background.contentSize = rect.size;
        background.anchorPoint = ccp(0, 0);
        background.ignoreAnchorPointForPosition = NO;
        
        [self addChild:background z:1];
        [self addChild:waves z:2];
        [self addChild:lineSingleThin z:3];
        [self addChild:linesThin z:4];
        
        [waves runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actions:
           [CCMoveBy actionWithDuration:0.5 position:ccp(-8, 8)],
           [CCFadeOut actionWithDuration:1.0],
           [CCDelayTime actionWithDuration:1.5],
           [CCFadeIn actionWithDuration:1.0],
           [CCMoveBy actionWithDuration:1.25 position:ccp(6, -6)],
           [CCMoveBy actionWithDuration:1.5 position:ccp(-6, 6)],
           [CCMoveBy actionWithDuration:1.5 position:ccp(8, -8)],
           nil]]];
        
        lineSingleThin.opacity = 0;
        [lineSingleThin runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actions:
           [CCFadeIn actionWithDuration:0.25],
           [CCMoveBy actionWithDuration:0.5 position:ccp(10, -10)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(0, 2)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(-4, -2)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(0, 6)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(-3, -6)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(0, 4)],
           [CCMoveBy actionWithDuration:0.5 position:ccp(-3, 6)],
           [CCFadeOut actionWithDuration:1.5],
           nil]]];
        
        linesThin.opacity = 0;
        [linesThin runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actions:
           [CCMoveBy actionWithDuration:0.5 position:ccp(-8, 8)],
           [CCFadeIn actionWithDuration:1.0],
           [CCDelayTime actionWithDuration:1.5],
           [CCFadeOut actionWithDuration:1.0],
           [CCMoveBy actionWithDuration:2.25 position:ccp(6, -6)],
           [CCMoveBy actionWithDuration:1.5 position:ccp(-6, 6)],
           [CCMoveBy actionWithDuration:1.5 position:ccp(8, -8)],
           nil]]];
                
        [self runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actionOne:[CCMoveBy actionWithDuration:8 position:ccp(-64, -64)]
                            two:[CCMoveBy actionWithDuration:0 position:ccp(64, 64)]]]];
        
        for (CCNode *sprite in @[waves, lineSingleThin, linesThin])
        {
            sprite.anchorPoint = ccp(0, 0);
            ccTexParams repeat = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
            ((CCSprite *)sprite).texture.texParameters = &repeat;
        }
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

@end

@implementation SPLCoreGameplayLayer

@synthesize Event_Touched, Event_TouchedWhenCloseBy, Event_LandsBridged,
            Event_Froze, Event_MapWasPanned, Event_MapWasLoaded,
            elric;

-(void)createTileHighlighters
{
    spriteTileHighlighters = [[NSMutableArray alloc] init];
    tileHighlighterAssociations = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < 10; i++)
    {
        CCSprite *spriteTileHighlighter = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteTileHighlighter.visible = NO;
        spriteTileHighlighter.opacity = 0.5 * 255;
        spriteTileHighlighter.anchorPoint = ccp(0, 0);
        
        [spriteTileHighlighter runAction:[CCRepeatForever actionWithAction:
                                          [CCSequence actionOne:[CCFadeTo actionWithDuration:0.5 opacity:0.35 * 255]
                                                            two:[CCFadeTo actionWithDuration:0.5 opacity:0.11 * 255]]]];
        [spriteTileHighlighters addObject:spriteTileHighlighter];
    }
}

-(CCSprite *)tileHighlighterAssociatedWithTouch:(UITouch *)touch
{
    for (id tileHighlighterAssociation in tileHighlighterAssociations)
    {
        if (tileHighlighterAssociation[@"touch"] == touch)
        {
            return tileHighlighterAssociation[@"sprite"];
        }
    }
    return nil;
}

-(void)unassociateTileHighlighterWithTouch:(UITouch *)touch
{
    for (id tileHighlighterAssociation in tileHighlighterAssociations)
    {
        if (tileHighlighterAssociation[@"touch"] == touch)
        {
            [tileHighlighterAssociations removeObject:tileHighlighterAssociation];
            return;
        }
    }
}

-(CCSprite *)unassociatedTileHighlighter
{
    NSMutableArray *associatedTileHighlighters = [NSMutableArray array];
    for (id tileHighlighterAssociation in tileHighlighterAssociations)
    {
        [associatedTileHighlighters addObject:tileHighlighterAssociation[@"sprite"]];
    }
    for (CCSprite *tileHighlighter in spriteTileHighlighters)
    {
        if (![associatedTileHighlighters containsObject:tileHighlighter])
            return tileHighlighter;
    }
    return nil;
}

- (id)init {
    self = [super init];
    if (self) {
        // Init Events
        FLEventMake(Event_Touched);
        FLEventMake(Event_TouchedWhenCloseBy);
        FLEventMake(Event_LandsBridged);
        FLEventMake(Event_MapWasLoaded);
        FLEventMake(Event_MapWasPanned);
        FLEventMake(Event_Froze);
        Event_Froze.eventDataComparator = ^BOOL (id dispatchedEventData, id conditionalActionData) {
            if ([dispatchedEventData isEqual:conditionalActionData]) return YES;
            if ([dispatchedEventData isKindOfClass:[NSArray class]])
                return [dispatchedEventData containsObject:conditionalActionData];
            return NO;
        };
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        // init background
        spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteBackground.scaleX = winSize.width;
        spriteBackground.scaleY = winSize.height;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
            spriteBackground.scaleX *= 2,
            spriteBackground.scaleY *= 2;
        spriteBackground.anchorPoint = ccp(0, 1);
        spriteBackground.position = ccp(0, 0);
        
        [self addChild:spriteBackground z:-100];
        spriteBlackOverlay = [[CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"] retain];
        spriteBlackOverlay.color = ccc3(0, 0, 0);
        spriteBlackOverlay.scaleX = winSize.width;
        spriteBlackOverlay.scaleY = winSize.height;
        spriteBlackOverlay.opacity = 0;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
            spriteBlackOverlay.scaleX *= 2,
            spriteBlackOverlay.scaleY *= 2;
        spriteBlackOverlay.position = fl_ccp(0, 0, 2048, 1536);

        [self.parent addChild:spriteBlackOverlay z:999];
         
        self.isTouchEnabled = YES;
        
        // 25 * 30 / 1 - 320 = 750 - 480 = 270
                
        NSString *playerName = [[Spellbound localStorage] dataForKey:@"Player"]; // either Elric or Amy
        // this will not be needed if not directly jumping to the gameplay screen
        if (!playerName)
        {
            playerName = @"Elric";
            [[Spellbound localStorage] setData:playerName forKey:@"Player"];
        }
        // TODO: rename to playerCharacter
        elric = [[SPLGameCharacter characterWithName:playerName] retain];
        
        [self createTileHighlighters];
        
        spriteTiledMapContainer = [CCSprite node];
        [self addChild:spriteTiledMapContainer];
        
        mapsLoaded = [[NSMutableDictionary alloc] init];
        disabledItems = [[NSMutableDictionary alloc] init];

        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"Gameplay"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"Gameplay"];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [self init])
    {
        mapsLoaded.dictionary = [decoder decodeObjectForKey:@"mapsLoaded"];
        disabledItems.dictionary = [decoder decodeObjectForKey:@"disabledItems"];
        for (NSString *item in disabledItems.keyEnumerator) {
            [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:1]
                                              two:[CCCallBlockO actionWithBlock:^(NSString *item) {
                FLAction *action = [disabledItems[item] boolValue] ? Action_Disable : Action_Enable;
                [action runWithData:item];
            } object:item]]];
        }
        
        [elric release];
        elric = [[decoder decodeObjectForKey:@"elric"] retain];
        
        __block NSString *currentMapName = [[decoder decodeObjectForKey:@"currentMapName"] retain];
        
        [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.25] two:[CCCallBlock actionWithBlock:^{
            [Action_LoadMap runWithData:currentMapName];
            [currentMapName release];
        }]]];

        // run all active check points
        [[Spellbound checkPointManager] resumeFromCheckPointsTitled:[decoder decodeObjectForKey:@"activeCheckPoints"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    mapsLoaded[_tiledMap.mapName] = [NSKeyedArchiver archivedDataWithRootObject:_tiledMap];
    
    [encoder encodeObject:mapsLoaded forKey:@"mapsLoaded"];
    [encoder encodeObject:elric forKey:@"elric"];
    [encoder encodeObject:_tiledMap.mapName forKey:@"currentMapName"];
    [encoder encodeObject:disabledItems forKey:@"disabledItems"];
    [encoder encodeObject:[[Spellbound checkPointManager] activeCheckPoints] forKey:@"activeCheckPoints"];
}

-(void)loadMapWithName:(NSString *)mapName
{
    [self unloadCurrentMap];
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGSize tiledMapSize;
    
    if (mapsLoaded[mapName]) {
        NSData *tiledMapData = mapsLoaded[mapName];
        _tiledMap = (SPLTiledMap *) [NSKeyedUnarchiver unarchiveObjectWithData:tiledMapData];
        elric.position = [_tiledMap mainCharacterPosition];
        // todo: when going back to an old map, use positions of other characters...
        NSLog(@"position of elric: %@", NSStringFromCGPoint(elric.position));
    } else {
        _tiledMap = [SPLTiledMap tiledMapWithMapName:mapName];
        
        elric.position = [_tiledMap boundingBoxOfCharacterNamed:@"Player"].origin;

        // write logic to place map in such a way that elric is in the center of the screen
        // in such a way that there are no black borders
        _tiledMap.position = ccp((0.00f) * (winSize.width - tiledMapSize.width),
                                 (-(0.00f) + 1) * (winSize.height - tiledMapSize.height));
        NSLog(@"position of elric: %@", NSStringFromCGPoint(elric.position));
    }
    tiledMapSize = CGSizeMake(_tiledMap.mapSize.width * _tiledMap.tileSize.width / CC_CONTENT_SCALE_FACTOR(),
                              _tiledMap.mapSize.height * _tiledMap.tileSize.height / CC_CONTENT_SCALE_FACTOR());

    _tiledMap.position = ccpClamp(ccpSub(ccp(winSize.width / 2, winSize.height / 2), elric.position),
                                  ccp(winSize.width - tiledMapSize.width, winSize.height - tiledMapSize.height), ccp(0, 0));
    _tiledMap.anchorPoint = ccp(0, 0);
    
    for (CCSprite *spriteTileHighlighter in spriteTileHighlighters)
    {
        [_tiledMap addChild:spriteTileHighlighter z:99];
        
        spriteTileHighlighter.scaleX = _tiledMap.tileSize.width;
        spriteTileHighlighter.scaleY = _tiledMap.tileSize.height;
    }
    
    // bind each interactive item's touched when close by event to action pick up.
    for (SPLInteractiveItem *interactiveItem in _tiledMap.interactiveObjects)
    {
        FLAction *action =  Action_InteractWith;
        action.actionDelegate = interactiveItem;
        [Event_TouchedWhenCloseBy bindToAction:action andDispatchOnlyIfDataEquals:interactiveItem.name];
    }
    
    // unbind from all stepped on actions before binding to any new ones
    // todo: ensure if this is the right way
    [elric.Event_SteppedOn unbindFromAllActions];
    // when stepped on an Exit tile, execute an action as defined in the layer's properties in TMX
    for (NSUInteger i = 1; i <= 10; i++)
    {
        NSString *layerName = [NSString stringWithFormat:@"Exit%d", i];
        CCTMXLayer *layer = [_tiledMap layerNamed:layerName];
        if (!layer) continue;
        for (NSString *prop in layer.properties) { // loop is executed only once
            FLAction *action = [FLAction actionWithActionDelegate:self andName:prop];
            action.data = [layer propertyNamed:prop];
            [elric.Event_SteppedOn bindToAction:action
                    andDispatchOnlyIfDataEquals:layerName];
        }
    }
    
    [_tiledMap.charactersLayer addChild:elric];
    NSInteger i = 0;
    for (SPLGameCharacter *follower in elric.charactersFollowing) {
        // todo: make sure the position of the following character isn't on an obstruction
        follower.position = ccpAdd(elric.position, ccp(0, ++i * -_tiledMap.tileSize.width / CC_CONTENT_SCALE_FACTOR()));
        [_tiledMap.charactersLayer addChild:follower];
    }
    [_tiledMap addChild:[SPLSpriteOceanWaves spriteWithRect:CGRectMake(0, 0, tiledMapSize.width, tiledMapSize.height)] z:-1];
    [spriteTiledMapContainer addChild:_tiledMap z:1];
}

-(void)unloadCurrentMap
{
    if (_tiledMap)
    {
        [_tiledMap removeChild:elric cleanup:NO];
        for (SPLGameCharacter *follower in elric.charactersFollowing)
            [_tiledMap removeChild:follower cleanup:NO];
        for (CCSprite *spriteTileHighlighter in spriteTileHighlighters)
        {
            [_tiledMap removeChild:spriteTileHighlighter cleanup:NO];
        }
        
        NSMutableArray *floatingLetterBlocks = [NSMutableArray array];
        if (_tiledMap.letterBlocks) for (SPLLetterBlock *letterBlock in _tiledMap.letterBlocks.objectEnumerator)
            if (!letterBlock.isFrozen) [floatingLetterBlocks addObject:letterBlock];
        
        for (SPLLetterBlock *letterBlock in floatingLetterBlocks)
        {
            // same logic as in ccTouchStart and doesn't really belong here, apologies for duplicating code, gotta get shit done fast
            // step 1: remove the letter from the tiledmap, retain destroyed sprite first
            SPLLetterBlockSprite *letterBlockSprite = letterBlock.letterSprite;
            [_tiledMap removeLetterAtPoint:letterBlock.location];
            letterBlockSprite.position = ccpAdd(letterBlockSprite.position, _tiledMap.position);
            letterBlockSprite.position = ccp(letterBlockSprite.position.x,
                                             letterBlockSprite.position.y + _tiledMap.tileSize.height / CC_CONTENT_SCALE_FACTOR());
            
            // step 2: add it back to the pile
            [((SPLGameScene *) self.parent).letterPileLayer.spriteThePanel addChild:letterBlockSprite z:100];
            [((SPLGameScene *) self.parent).letterPileLayer.arrayOfLetterBlockSpritesInHand addObject:letterBlockSprite];
            
            // step 3: run return back to pile action
            [FL with:letterBlockSprite runAnimationAction:kFLAnimationActionRestoreSize];
            FLAction *action = Action_ReturnLetterBackToPile;
            [action runWithData:letterBlockSprite];
            
            // todo: optional: animate them one by one, not all at once
        }

        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_tiledMap];

        mapsLoaded[_tiledMap.mapName] = data;
        [spriteTiledMapContainer removeChild:_tiledMap cleanup:YES];
        _tiledMap = nil;

        [[CCDirector sharedDirector] purgeCachedData];
    }
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_LoadMap])
    {
        if (![self.parent.children containsObject:spriteBlackOverlay]) {
            [self.parent addChild:spriteBlackOverlay z:999];
        }

        [spriteBlackOverlay runAction:
         [CCSequence actions:
          [CCFadeIn actionWithDuration:0.5],
          [CCCallBlock actionWithBlock:^ {
             [self unloadCurrentMap];
             [self loadMapWithName:action.data];
             spriteTiledMapContainer.visible = YES;
         }],
          [CCFadeOut actionWithDuration:0.5],
          [CCCallBlock actionWithBlock:^ {
             [action finish];
             [Event_MapWasLoaded dispatchWithData:action.data];
         }],
          nil]];
    }
    else if ([action is:Action_UnloadMap])
    {
        [spriteTiledMapContainer runAction:[CCSequence actions:[CCFadeOut actionWithDuration:0.35], [CCHide action], [CCCallBlock actionWithBlock:^{
            [action finish];
        }], nil]];
        [self unloadCurrentMap];
    }
    else if ([action is:Action_HighlightTileUnderPoint])
    {
        // do as the action says
        // highlight the tile under the point (action.data)
        // passed by the ccTouchMoved method on letterPileLayer
        // if the tile type is water, highlight in green
        // else highlight in red
        
        CGPoint point = ((NSValue*)action.data[@"point"]).CGPointValue;
        
        // convert this point to a point you can pass to [_tiledMap tileTypeAt:]
        NSUInteger xval = floorf((-_tiledMap.position.x + point.x) / _tiledMap.tileSize.width * CC_CONTENT_SCALE_FACTOR());
        NSUInteger yval = floorf(_tiledMap.mapSize.height - (-_tiledMap.position.y + point.y) / _tiledMap.tileSize.height * CC_CONTENT_SCALE_FACTOR()) - 1;
        
        tilePositionOfTileHighlighter = ccp(xval, yval);
        
        CCSprite *tileAtPoint = [[_tiledMap layerNamed:@"ocean waves"] tileAt:tilePositionOfTileHighlighter];
        SPLTileType tileTypeAtPoint = [_tiledMap tileTypeAt:tilePositionOfTileHighlighter];
        
        CCSprite *spriteTileHighlighter = [self tileHighlighterAssociatedWithTouch:action.data[@"touch"]];
        
        if (!spriteTileHighlighter)
        {
            spriteTileHighlighter = [self unassociatedTileHighlighter];
            [tileHighlighterAssociations addObject:@{@"touch": action.data[@"touch"], @"sprite": spriteTileHighlighter}];
        }
        
        spriteTileHighlighter.position = tileAtPoint.position;
        spriteTileHighlighter.visible = YES;
        
        if (tileTypeAtPoint == SPLTileTypeWater)
            spriteTileHighlighter.color = ccc3(0, 255, 0);
        else
            spriteTileHighlighter.color = ccc3(255, 0, 0);
        
        [action finish];
    }
    else if ([action is:Action_UnhighlightHighlightedTile])
    {
        CCSprite *spriteTileHighlighter = [self tileHighlighterAssociatedWithTouch:action.data[@"touch"]];
        spriteTileHighlighter.visible = NO;
        [self unassociateTileHighlighterWithTouch:action.data[@"touch"]];
        
        [action finish];
    }
    else if ([action is:Action_StopDraggingSelectedLetter])
    {
        FLAction *unhighlightAction = Action_UnhighlightHighlightedTile;
        if (action.data)
            unhighlightAction.data = @{@"point": action.data[@"point"], @"touch": action.data[@"touch"]};
        [unhighlightAction run];
        
        if (!action.data)
            return [Action_ReturnLetterBackToPile run];
        
        CGPoint point = ((NSValue*)action.data[@"point"]).CGPointValue;
        CGPoint touchDistance = [(NSValue *) action.data[@"touchDistance"] CGPointValue];
        CGPoint tileHeightDistance = ccp(0, _tiledMap.tileSize.height / CC_CONTENT_SCALE_FACTOR());
        
        point = ccpAdd(ccpSub(point, touchDistance), tileHeightDistance);
        
        // convert this point to a point you can pass to [_tiledMap tileTypeAt:]
        NSUInteger xval = floorf((-_tiledMap.position.x + point.x) / _tiledMap.tileSize.width * CC_CONTENT_SCALE_FACTOR());
        NSUInteger yval = floorf(_tiledMap.mapSize.height - (-_tiledMap.position.y + point.y) / _tiledMap.tileSize.height * CC_CONTENT_SCALE_FACTOR()) - 1;
        
        tilePositionOfTileHighlighter = ccp(xval, yval);
        
        SPLTileType tileTypeAtPoint = [_tiledMap tileTypeAt:tilePositionOfTileHighlighter];
        
        if (tileTypeAtPoint == SPLTileTypeWater)
        {
            action.subsequentAction = Action_RemoveLetterFromPile;
            action.subsequentAction.data = action.data[@"sprite"];
            action.subsequentAction.subsequentAction = Action_AddSelectedLetterToTiledMap;
            action.subsequentAction.subsequentAction.data = action.data[@"sprite"];
            [action finish];
        }
        else
        {
            action.subsequentAction = Action_ReturnLetterBackToPile;
            action.subsequentAction.data = action.data[@"sprite"];
            [action finish];
        }
    }
    else if ([action is:Action_AddSelectedLetterToTiledMap])
    {
        [_tiledMap addLetterSprite:(SPLLetterBlockSprite *) action.data atPoint:tilePositionOfTileHighlighter];
        [action finish];
    }
    else if ([action is:Action_FreezeFloatingLetters])
    {
        NSString *errorMessage = nil;
        NSArray *wordsFrozen = [_tiledMap freezeLetters:&errorMessage];
        if (wordsFrozen && wordsFrozen.count) // hack :(
        {
            [Event_Froze dispatchWithData:wordsFrozen];
            action.subsequentAction = Action_RefillLettersInHand;
        }
        else if (errorMessage)
        {
            NSLog(@"Freeze failed: %@", errorMessage);
            FLAction *alertAction = Action_Alert;
            alertAction.data = errorMessage;
            [alertAction run];
            
            // get freeze failure message from _tiledMap
            // something like _tiledMap.freezeFailureMessage
        }
        
        [action finish];
    }
    else if ([action is:Action_Disable] || [action is:Action_Enable])
    {
        BOOL disabled = [action is:Action_Disable];
        if ([action.data isEqualToString:@"MapDrag"])
            isMapDragDisabled = disabled;
        else if ([action.data isEqualToString:@"PlayerDrag"])
            isPlayerDragDisabled = disabled;
        else if ([action.data isEqualToString:@"LetterPile"])
            [(SPLGameScene *) self.parent letterPileLayer].isDisabled = disabled;
        else if ([action.data isEqualToString:@"LetterPileAutoRefill"])
            [(SPLGameScene *) self.parent letterPileLayer].isAutoRefillDisabled = disabled;
        else if ([action.data isEqualToString:@"Menu"])
            [(SPLGameScene *) self.parent inGameMenuLayer].isDisabled = disabled;
        else if ([action.data isEqualToString:@"Coins"])
            [(SPLGameScene *) self.parent coinsPanelLayer].isDisabled = disabled;
        else if ([action.data isEqualToString:@"Items"])
            [(SPLGameScene *) self.parent itemsPanelLayer].isDisabled = disabled;
        
        disabledItems[action.data] = [NSNumber numberWithBool:disabled];
        
        [action finish];
    }
    else
    {
        SPLGameScene *parent = (SPLGameScene *)self.parent;
        
        [action forwardToActionDelegates:parent.letterPileLayer, parent.alertPanelLayer, nil];
    }
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:10 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (![self visible]) return NO;
    
    if (_tiledMap.letterBlocks)
    {
        for (SPLLetterBlock *letterBlock in _tiledMap.letterBlocks.objectEnumerator)
        {
            // not so fast, theory only applies to non frozen blocks
            if (!letterBlock.isFrozen)
            if (CGRectContainsPoint(letterBlock.letterSprite.boundingBox, ccpSub(point, _tiledMap.position)))
            {
                //NSLog(@"you touched the letter '%@'! cool! lets start dragging this", letterBlock.letter);
                
                // step 1: remove the letter from the tiledmap, retain destroyed sprite first
                SPLLetterBlockSprite *letterBlockSprite = letterBlock.letterSprite.retain;
                [_tiledMap removeLetterAtPoint:letterBlock.location];
                letterBlockSprite.position = ccpAdd(letterBlockSprite.position, _tiledMap.position);
                letterBlockSprite.position = ccp(letterBlockSprite.position.x, letterBlockSprite.position.y + _tiledMap.tileSize.height / CC_CONTENT_SCALE_FACTOR());
                
                // step 2: add it back to the pile
                [((SPLGameScene *) self.parent).letterPileLayer.spriteThePanel addChild:letterBlockSprite z:100];
                [((SPLGameScene *) self.parent).letterPileLayer.arrayOfLetterBlockSpritesInHand addObject:letterBlockSprite];
                
                // step 3: start dragging and release the destroyed sprite
                [FL with:letterBlockSprite runAnimationAction:kFLAnimationActionRestoreSize];
                FLAction *action = Action_StartDraggingSelectedLetter;
                CGPoint touchDistance = [letterBlockSprite convertToNodeSpace:ccpSub(point, letterBlockSprite.anchorPointInPoints)];
                action.data = @{@"sprite": letterBlockSprite, @"touch": touch, @"touchDistance": [NSValue valueWithCGPoint:touchDistance]};
                [action run];
                
                [letterBlockSprite release];
                
                return YES;
            }
        }
    }
    if ([FL doesSprite:elric containPoint:point] && !isPlayerDragDisabled)
    {
        NSLog(@"you touched elric");
        _elricTouchObject = touch;
        return YES;
    }
    
    NSDictionary *interactiveObjectTouched = [_tiledMap interactiveObjectUnderPoint:point];
    if (interactiveObjectTouched)
    {
        [Event_Touched dispatchWithData:interactiveObjectTouched[@"name"]];
        NSLog(@"touched: %@", interactiveObjectTouched[@"name"]);
        
        CGPoint positionOfInteractiveObject = ccp(
            [interactiveObjectTouched[@"x"] floatValue],
            [interactiveObjectTouched[@"y"] floatValue]
        );
        
        CGPoint tileOffset = ccp(floorf([interactiveObjectTouched[@"width"] floatValue] / _tiledMap.tileSize.width) - 1,
                                                    floorf([interactiveObjectTouched[@"height"] floatValue] / _tiledMap.tileSize.height) - 1);
        
        CGPoint distance = ccpSub([_tiledMap convertPositionToTilePosition:elric.position],
                                  [_tiledMap convertPositionToTilePosition:positionOfInteractiveObject]);
        
        NSLog(@"Distance: %@", NSStringFromCGPoint(distance));
        CGPoint minimumDistance = distance;
        if (abs(distance.x) + abs(distance.y) == 1)
            minimumDistance = distance;
        else if (abs(distance.x - tileOffset.x) + abs(distance.y) == 1)
            minimumDistance = ccpSub(distance, ccp(tileOffset.x, 0));
        else if (abs(distance.x) + abs(distance.y - tileOffset.y) == 1)
            minimumDistance = ccpSub(distance, ccp(0, tileOffset.y));
        else if (abs(distance.x - tileOffset.x) + abs(distance.y - tileOffset.y) == 1)
            minimumDistance = ccpSub(distance, tileOffset);
        
        if (abs(minimumDistance.x) + abs(minimumDistance.y) == 1)
        {
            NSLog(@"touched when close by: %@", interactiveObjectTouched[@"name"]);
            [Event_TouchedWhenCloseBy dispatchWithData:interactiveObjectTouched[@"name"]];
            
            FLAction *action;
            if (minimumDistance.x == 0 && minimumDistance.y == 1)
                action = Action_StopFacingUp;
            else if (minimumDistance.x == 0 && minimumDistance.y == -1)
                action = Action_StopFacingDown;
            else if (minimumDistance.x == 1 && minimumDistance.y == 0)
                action = Action_StopFacingLeft;
            else
                action = Action_StopFacingRight;
            action.actionDelegate = elric;
            
            [action run];
        }
        
        return YES;
    }
    
    SPLGameCharacter *characterTouched = [_tiledMap characterUnderPoint:point];
    if (characterTouched && characterTouched != elric)
    {
        [Event_Touched dispatchWithData:characterTouched.characterName];
        
        CGPoint distance = ccpSub([_tiledMap convertPositionToTilePosition:elric.position],
                                  [_tiledMap convertPositionToTilePosition:characterTouched.position]);
        
        NSLog(@"Distance: %@", NSStringFromCGPoint(distance));
        if (abs(distance.x) + abs(distance.y) == 1) {
            NSLog(@"touched when close by: %@", characterTouched.characterName);
            [Event_TouchedWhenCloseBy dispatchWithData:characterTouched.characterName];
            
            FLAction *action, *oppositeAction;
            if (distance.x == 0 && distance.y == 1) {
                action = Action_StopFacingUp;
                oppositeAction = Action_StopFacingDown;
            }
            else if (distance.x == 0 && distance.y == -1) {
                action = Action_StopFacingDown;
                oppositeAction = Action_StopFacingUp;
            }
            else if (distance.x == 1 && distance.y == 0) {
                action = Action_StopFacingLeft;
                oppositeAction = Action_StopFacingRight;
            }
            else {
                action = Action_StopFacingRight;
                oppositeAction = Action_StopFacingLeft;
            }
            action.actionDelegate = elric;
            oppositeAction.actionDelegate = characterTouched;
            
            [action run];
            [oppositeAction run];
        }
        
        return YES;
    }
    
    // bind map touches to a UI touch like the letters
    if (!isMapDragDisabled) {
        NSLog(@"you must have touched the map");
        positionOfMapBeforeTouch = _tiledMap.position;
        positionOfFingerBeforeTouch = point;
        mapDragTouchObject = touch;
        return YES;
    }

    return NO;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    // create a set of functions to convert between
    // screen coordinates and tile coordinates
    
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (_elricTouchObject == touch)
    {
        if (elric.isAnimating)
        {
            FLAction *actionToRun = Action_StopWalking;
            actionToRun.actionDelegate = elric;
            [actionToRun run];
        }
        else
        {
            CGPoint pt;
            
            pt = ccpAdd(elric.position, _tiledMap.position);
            
            // convert this point to a point you can pass to [_tiledMap tileTypeAt:]
            CGPoint src = ccp(floorf((-_tiledMap.position.x + pt.x) / _tiledMap.tileSize.width * CC_CONTENT_SCALE_FACTOR()),
                              floorf(_tiledMap.mapSize.height - (-_tiledMap.position.y + pt.y) / _tiledMap.tileSize.height * CC_CONTENT_SCALE_FACTOR()) - 1);
            
            pt = point;
            CGPoint dst = ccp(floorf((-_tiledMap.position.x + pt.x) / _tiledMap.tileSize.width * CC_CONTENT_SCALE_FACTOR()),
                              floorf(_tiledMap.mapSize.height - (-_tiledMap.position.y + pt.y) / _tiledMap.tileSize.height * CC_CONTENT_SCALE_FACTOR()));
            
            [_tiledMap createConnectorFromSource:src toDestination:dst];
            
            return;
        }
    }
    
    if (mapDragTouchObject == touch)
    {
        CGPoint newPosition = ccpAdd(positionOfMapBeforeTouch, ccpNeg(ccpAdd(positionOfFingerBeforeTouch, ccpNeg(point))));
        
        CGPoint upperLimit = ccp(-_tiledMap.mapSize.width * _tiledMap.tileSize.width / CC_CONTENT_SCALE_FACTOR() + winSize.width,
                                 -_tiledMap.mapSize.height * _tiledMap.tileSize.height / CC_CONTENT_SCALE_FACTOR() + winSize.height);
        
        if (newPosition.x > 0)
            newPosition.x = 0;
        else if (newPosition.x < upperLimit.x)
            newPosition.x = upperLimit.x;
        if (newPosition.y > 0)
            newPosition.y = 0;
        else if (newPosition.y < upperLimit.y)
            newPosition.y = upperLimit.y;

        _tiledMap.position = newPosition;
        [Event_MapWasPanned dispatch];
    }
    // touch move events should be forwarded to the letter pile just in case
    [((SPLGameScene *) self.parent).letterPileLayer ccTouchMoved:touch withEvent:event];
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (_elricTouchObject == touch)
    {
        [_tiledMap.connectorLayer removeAllChildrenWithCleanup:YES];
        _elricTouchObject = nil;
        
        if (!elric.isAnimating) {
            [elric walkAlongPath:_tiledMap.connectorPath];
            [_tiledMap.connectorPath removeAllObjects];
        }
    }
    
    if (mapDragTouchObject == touch)
        mapDragTouchObject = nil;
    
    // touch end events should be forwarded to the letter pile just in case
    [((SPLGameScene *) self.parent).letterPileLayer ccTouchEnded:touch withEvent:event];
}

- (void)dealloc
{
    // release events
    [Event_Touched release];
    [Event_TouchedWhenCloseBy release];
    [Event_LandsBridged release];
    [Event_MapWasPanned release];
    [Event_MapWasLoaded release];
    [Event_Froze release];
    
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    
    [tileHighlighterAssociations release];
    [spriteBlackOverlay release];
    [spriteTileHighlighters release];
    [elric release];
    [mapsLoaded release];
    [disabledItems release];
    [super dealloc];
}

@end
