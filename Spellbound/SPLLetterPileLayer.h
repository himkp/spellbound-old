//
//  SPLLetterPileLayer.h
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLLetterPile.h"

@interface SPLLetterPileLayer : CCLayer <FLActionDelegate, NSCoding>
{
    CCSprite *spriteBackground;
    
    CCSprite *spriteBorderRight;
    CCSprite *spriteBorderTop;
    CCSprite *spriteBorderLeft;
    
    CGPoint positionOfBackground;
    CGPoint positionOfThePanel;
    
    CGPoint positionOfBorderRight;
    CGPoint positionOfBorderLeft;
    CGPoint positionOfBorderTop;
    CGPoint positionOfButtonFreeze;
    
    CGPoint positionOfFirstLetterInHand;
    
    NSMutableArray *currentActions;
}

-(void)initializePositions;
-(void)show;
-(void)hide;
-(void)show:(void(^)())onShow;
-(void)hide:(void(^)())onHide;

@property (nonatomic, readonly) CCSprite* spriteButtonFreeze;
@property (nonatomic, readonly) CCSprite* spriteThePanel;

@property (nonatomic, readonly) SPLLetterPile* letterPile;
@property (nonatomic, readonly) NSMutableArray *arrayOfLetterBlockSpritesInHand;

@property (nonatomic) BOOL isDisabled;
@property (nonatomic) BOOL isAutoRefillDisabled;

// Actions
#define Action_ShowLetterPile                           FLActionMake(Action_ShowLetterPile)
#define Action_HideLetterPile                           FLActionMake(Action_HideLetterPile)
#define Action_RefillLettersInHand                      FLActionMake(Action_RefillLettersInHand)

#define Action_AddLetterToPile                          FLActionMake(Action_AddLetterToPile)
#define Action_AddLetterToPileFrom                      FLActionMake(Action_AddLetterToPileFrom)

#define Action_AddLettersToBag                          FLActionMake(Action_AddLettersToBag)

#define Action_StartDraggingSelectedLetter              FLActionMake(Action_StartDraggingSelectedLetter)
#define Action_ContinueDraggingSelectedLetterAtPoint    FLActionMake(Action_ContinueDraggingSelectedLetterAtPoint)
#define Action_StopDraggingSelectedLetter               FLActionMake(Action_StopDraggingSelectedLetter)
#define Action_RemoveLetterFromPile                     FLActionMake(Action_RemoveLetterFromPile)
#define Action_ReturnLetterBackToPile                   FLActionMake(Action_ReturnLetterBackToPile)

@end