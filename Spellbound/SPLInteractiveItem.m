//
//  SPLInteractiveItem.m
//  Spellbound
//
//  Created by Fleon Games on 9/16/13.
//
//

#import "SPLInteractiveItem.h"
#import "SPLInteractiveItemTreasureChest.h"
#import "SPLInteractiveItemMagicalBag.h"
#import "SPLTiledMap.h"

@implementation SPLInteractiveItem

- (id)init
{
    self = [super init];
    if (self) {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"iPad/InteractiveItems.plist"];
        
        self.anchorPoint = ccp(0, 0);
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [self init])
    {
        self.isPickedUp = [decoder decodeBoolForKey:@"isPickedUp"];
        self.hasInteractedWith = [decoder decodeBoolForKey:@"hasInteractedWith"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.position = [decoder decodeCGPointForKey:@"position"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeBool:self.isPickedUp forKey:@"isPickedUp"];
    [encoder encodeBool:self.hasInteractedWith forKey:@"hasInteractedWith"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeCGPoint:self.position forKey:@"position"];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_PickUp])
    {
        self.isPickedUp = YES;
        [action finish];
    }
    else if ([action is:Action_InteractWith])
    {
        self.hasInteractedWith = YES;
        [action finish];
    }
}

+(SPLInteractiveItem *)createInteractiveItemOfType:(NSString *)itemType;
{
    if ([itemType isEqualToString:@"TreasureChest"])
    {
        return [SPLInteractiveItemTreasureChest node];
    }
    else if ([itemType isEqualToString:@"MagicalBag"])
    {
        return [SPLInteractiveItemMagicalBag node];
    }
    else
    {
        return [SPLInteractiveItem node];
    }
}

-(void)setParent:(CCNode *)parent
{
    if (parent != nil)
        NSAssert([parent isKindOfClass:[SPLTiledMap class]], NSStringFromClass([parent class]), @"Parent must be of type [SPLTiledMap]. Right now it is [%@].");
    _tiledMap = (SPLTiledMap*) parent;
    [super setParent:parent];
}

- (void)dealloc
{
    [_name release];
    [super dealloc];
}

@end
