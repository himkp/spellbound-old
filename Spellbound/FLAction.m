//
//  FLActionSequence.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "FLAction.h"
#import "FLEvent.h"
#import "SPLGlobalActionDelegate.h"

@implementation FLProxyActionDelegate

+(id)actionDelegateWithName:(NSString *)name
{
    return [[[self alloc] initWithName:name] autorelease];
}

-(id)initWithName:(NSString *)name
{
    if (self = [super init])
    {
        actionDelegateName = [name retain];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    id<FLActionDelegate> actionDelegate = [[Spellbound actionDelegateManager] actionDelegateWithName:actionDelegateName];
    
    // do not forward the action to another proxy
    if (![actionDelegate isKindOfClass:[FLProxyActionDelegate class]])
        [actionDelegate handleAction:action];
}

- (void)dealloc
{
    [actionDelegateName release];
    [super dealloc];
}

@end

@implementation FLAction

-(id)init
{
    if (self = [super init])
    {
        _hasStarted = _hasFinished = NO;
        runCountThreshold = UINT_MAX;
        _runCount = 0;
        _onFinish = ^{};
        _onRun = ^{};
        
        Event_ActionDidFinish = [[FLEvent alloc] initWithName:@"ActionDidFinish"];
        Event_ActionDidFinish.shouldUnbindActionsOnDispatch = YES;
        
        delegatesForwardedTo = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate
{
    if (self = [self init])
        self.actionDelegate = actionDelegate;
    return self;
}

-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate andName:(NSString *)name
{
    if (self = [self initWithActionDelegate:actionDelegate])
        self.name = name;
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"(Action_%@:%@)", self.name, self.data];
}

-(FLAction *)lastSubsequentAction
{
    FLAction *nextAction = self;
    FLAction *lastSubsequentAction = nil;
    
    while ((nextAction = nextAction.subsequentAction))
        lastSubsequentAction = nextAction;
    
    return lastSubsequentAction;
}

-(void)setSubsequentAction:(FLAction *)subsequentAction
{
    if (_subsequentAction) {
        [Event_ActionDidFinish unbindFromAction:_subsequentAction];
        [_subsequentAction release];
    }
    if (subsequentAction) {
        _subsequentAction = [subsequentAction retain];
        _subsequentAction.priorAction = self;
        [Event_ActionDidFinish bindToAction:subsequentAction];
    }
    else
    {
        _subsequentAction = nil;
    }
}

+(id)actionWithActionDelegate:(id<FLActionDelegate>)actionDelegate
{
    return [[[self alloc] initWithActionDelegate:actionDelegate] autorelease];
}

+(id)actionWithActionDelegate:(id<FLActionDelegate>)actionDelegate andName:(NSString *)name
{
    return [[[self alloc] initWithActionDelegate:actionDelegate andName:name] autorelease];
}

-(void)run
{
    _hasStarted = YES;
    _runCount ++;
    _onRun();

    [_actionDelegate handleAction:self];
    
    if (_runCount >= runCountThreshold) {
        [self finish];
        runCountThreshold = UINT_MAX;
    }
}

-(void)runWithData:(id)data
{
    self.data = data;
    [self run];
}

-(void)finish
{
    _hasFinished = YES;
    
    [Event_ActionDidFinish dispatch];
    
    _onFinish();
}

-(void)willFinishAfter:(NSTimeInterval)seconds
{
    [self performSelector:@selector(finish) withObject:self afterDelay:seconds];
}

-(void)willFinishAfterRunCountReaches:(NSUInteger)count
{
    runCountThreshold = count;
}

-(void)forwardToActionDelegate:(id<FLActionDelegate>)actionDelegate
{
    if ([delegatesForwardedTo containsObject:actionDelegate]) return;
    
    [delegatesForwardedTo addObject:actionDelegate];
    [actionDelegate handleAction:self];
}

-(void)forwardToActionDelegates: (id<FLActionDelegate>) actionDelegate, ...
{
    id<FLActionDelegate> eachActionDelegate;
    va_list argumentList;
    
    if (actionDelegate)
    {
        [self forwardToActionDelegate:actionDelegate];
        
        va_start(argumentList, actionDelegate); // Start scanning for arguments after actionDelegate.
        while ((eachActionDelegate = va_arg(argumentList, id<FLActionDelegate>))) // As many times as we can get an argument
            [self forwardToActionDelegate:eachActionDelegate];
        va_end(argumentList);
    }
}

-(BOOL)is:(FLAction *)action
{
    return [self.name isEqualToString:action.name];
}

-(void)dealloc
{
    [_name release];
    [_actionDelegate release];
    [_data release];
    [_subsequentAction release];
    [_priorAction release];
    
    [Event_ActionDidFinish release];
    
    [_onRun release];
    [_onFinish release];
    
    [delegatesForwardedTo release];
    [super dealloc];
}

@end

@implementation FLActionSequence

@synthesize firstAction = _firstAction;

-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate
             andFirstAction:(FLAction *)firstAction
                    andName:(NSString *)name
{
    if (self = [super init])
    {
        self.actionDelegate = actionDelegate;
        self.firstAction = firstAction;
        // the action previous to the first action in a sequence is the sequence itself
        self.firstAction.priorAction = self;
        self.name = name;
        
        [self.lastAction->Event_ActionDidFinish bindToAction:self];
    }
    return self;
}

+(id)actionSequenceWithActionDelegate:(id<FLActionDelegate>)actionDelegate
                       andFirstAction:(FLAction *)firstAction
                              andName:(NSString *)name
{
    return [[[self alloc] initWithActionDelegate:actionDelegate
                                  andFirstAction:firstAction
                                         andName:name] autorelease];
}

-(void)run
{
    [super run];
    
    if (self.runCount % 2 == 1)
        [self.firstAction run];
    else
        [self finish];
}

-(FLAction *)lastAction
{
    FLAction *lastAction = self.firstAction.lastSubsequentAction;
    return lastAction ? lastAction : self.firstAction;
}

-(void)dealloc
{
    [_firstAction release];
    [super dealloc];
}

@end

@implementation FLActionSequenceManager

-(id)init
{
    if (self = [super init])
        actionSequences = [[NSMutableDictionary alloc] init];
    return self;
}

-(FLActionSequence *)actionSequenceWithName:(NSString *)name
{
    // A copy should be made to avoid garbage _data from piling up
    FLActionSequence *actionSequence = actionSequences[name];
    
    FLAction* action = actionSequence.firstAction;
    FLAction* newAction = [FLAction actionWithActionDelegate:action.actionDelegate
                                                     andName:action.name];
    newAction.data = action.data;
    
    FLAction* firstAction = newAction;
    
    while ((action = action.subsequentAction))
    {
        newAction.subsequentAction = [FLAction actionWithActionDelegate:action.actionDelegate
                                                                andName:action.name];
        newAction.subsequentAction.data = action.data;
        newAction = newAction.subsequentAction;
    }
    
    FLActionSequence *newActionSequence = [FLActionSequence actionSequenceWithActionDelegate:actionSequence.actionDelegate
                                                                              andFirstAction:firstAction
                                                                                     andName:actionSequence.name];

    return newActionSequence;
}

-(void)addActionSequence:(FLActionSequence *)actionSequence
{
    actionSequences[actionSequence.name] = actionSequence;
}

- (void)dealloc {
    [actionSequences release];
    
    [super dealloc];
}

@end

@implementation FLActionDelegateManager

- (id)init
{
    self = [super init];
    if (self) {
        actionDelegates = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id<FLActionDelegate>)actionDelegateWithName:(NSString *)name
{
    name = name.stringByReplacingVariables;
    NSLog(@"name after replacing variables: %@", name);
    if (!actionDelegates[name])
        actionDelegates[name] = [FLProxyActionDelegate actionDelegateWithName:name];
    return actionDelegates[name];
}

-(void)registerActionDelegate:(id<FLActionDelegate>)actionDelegate withName:(NSString *)name
{
    NSLog(@"Registered: %@", name);
    actionDelegates[name] = actionDelegate;
}

-(void)unregisterActionDelegate:(id<FLActionDelegate>)actionDelegate
{
    NSArray *keys = [actionDelegates allKeysForObject:actionDelegate];
    for (NSString *key in keys)
        [actionDelegates removeObjectForKey:key];
}

-(void)unregisterActionDelegateWithName:(NSString *)name
{
    [actionDelegates removeObjectForKey:name];
}

- (void)dealloc
{
    [actionDelegates release];
    [super dealloc];
}

@end