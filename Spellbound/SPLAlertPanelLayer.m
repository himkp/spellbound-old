//
//  SPLAlertPanelLayer.m
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLAlertPanelLayer.h"

@implementation SPLAlertPanelLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfThePanel          = fl_ccp(   0,    0, 2048, 1536);

        positionOfBackground        = fl_ccp(   0,    0, 2048, 1536);
        positionOfWhiteUnderlay     = fl_ccp( 380,  433, 1280,  380);
        
        positionOfBorderLeft        = fl_ccp( 348,  502,   98,  256);
        positionOfBorderTop         = fl_ccp( 288,  363, 1234,  139);
        positionOfBorderRight       = fl_ccp(1587,  546,  109,  232);
        positionOfBorderBottom1     = fl_ccp( 288,  758,  530,  148);
        positionOfBorderBottom2     = fl_ccp(1237,  778,  502,  122);
        positionOfButtonClose       = fl_ccp(1522,  363,  211,  183);
        positionOfButtonOK          = fl_ccp( 818,  756,  419,  206);
    }
    else if (DEVICE_IS_IPHONE)
    {
        // the game is currently only for iPad
    }
}

-(void)setupBordersAndBackground
{
    spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
    spriteBackground.scaleX = DEVICE_IS_IPAD ? 1024 : 480;
    spriteBackground.scaleY = DEVICE_IS_IPAD ? 768 : 320;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY)
    {
        spriteBackground.scaleX *= 2;
        spriteBackground.scaleY *= 2;
    }
    spriteBackground.position = positionOfBackground;
    
    spriteWhiteUnderlay = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
    spriteWhiteUnderlay.scaleX = DEVICE_IS_IPAD ? 640 : 0; // ipad only
    spriteWhiteUnderlay.scaleY = DEVICE_IS_IPAD ? 190 : 0; // ipad only
    if (DEVICE_SUPPORTS_RETINA_DISPLAY)
    {
        spriteWhiteUnderlay.scaleX *= 2;
        spriteWhiteUnderlay.scaleY *= 2;
    }
    spriteWhiteUnderlay.position = positionOfWhiteUnderlay;
    
    spriteBorderTop = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Border_Top.png"];
    spriteBorderLeft = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Border_Left.png"];
    spriteBorderRight = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Border_Right.png"];
    spriteBorderBottom1 = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Border_Bottom_1.png"];
    spriteBorderBottom2 = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Border_Bottom_2.png"];
    spriteButtonClose = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Button_Close.png"];
    spriteButtonOK = [CCSprite spriteWithSpriteFrameName:@"AlertPanel_Button_OK.png"];
    
    spriteBorderTop.position = positionOfBorderTop;
    spriteBorderLeft.position = positionOfBorderLeft;
    spriteBorderRight.position = positionOfBorderRight;
    spriteBorderBottom1.position = positionOfBorderBottom1;
    spriteBorderBottom2.position = positionOfBorderBottom2;
    spriteButtonClose.position = positionOfButtonClose;
    spriteButtonOK.position = positionOfButtonOK;
    
    spriteThePanel = [CCSprite node];
    spriteThePanel.contentSize = [CCDirector sharedDirector].winSize;
    spriteThePanel.position = positionOfThePanel;
    
    [self addChild:spriteBackground];
    [self addChild:spriteThePanel];
    [spriteThePanel addChild:spriteWhiteUnderlay];
    [spriteThePanel addChild:spriteBorderTop];
    [spriteThePanel addChild:spriteBorderLeft];
    [spriteThePanel addChild:spriteBorderRight];
    [spriteThePanel addChild:spriteBorderBottom1];
    [spriteThePanel addChild:spriteBorderBottom2];
    [spriteThePanel addChild:spriteButtonClose];
    [spriteThePanel addChild:spriteButtonOK];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.visible = NO;
        self.isTouchEnabled = YES;
        
        [self initializePositions];
        [self setupBordersAndBackground];
        
        labelMessage = [CCLabelBMFont labelWithString:@"" fntFile:
                        DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt" :
                        @"iPhone/Fonts/DoctorSoosBold.fnt"];
        
        labelMessage.alignment = kCCTextAlignmentCenter;
        labelMessage.position = positionOfWhiteUnderlay;
        
        [spriteThePanel addChild:labelMessage];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"Alert"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"Alert"];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is: Action_Alert])
    {
        currentAction = [action retain];
        NSString *message = [[Spellbound messageManager] messageWithTitle:action.data];
        if (!message) message = action.data;
        
        self.message = message;
        
        [self show];
    }
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(NSString *)message
{
    return labelMessage.string;
}

-(void)setMessage:(NSString *)message
{
    labelMessage.string = message;
}

-(void)show
{
    [self show:^{}];
}

-(void)show:(void (^)())onShow
{
    self.visible = YES;
    spriteBackground.opacity = 0;
    spriteThePanel.opacity = 0;
    spriteThePanel.scale = 0;
    
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:180]];
    
    [spriteThePanel runAction:
     [CCEaseBackOut actionWithAction:
      [CCScaleTo actionWithDuration:0.5 scale:1]]];
    
    [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.5] two:[CCCallBlock actionWithBlock:onShow]]];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if ([self visible])
    {
        if ([FL doesSprite:spriteButtonClose containPoint:point] ||
            [FL doesSprite:spriteButtonOK containPoint:point])
        {
            [self hide:^{
                if (currentAction)
                {
                    [currentAction finish];
                    [currentAction release];
                    currentAction = nil;
                }
            }];
        }
        return YES;
    }
    return NO;
}

-(void)hide
{
    [self hide:^{}];
}

-(void)hide:(void (^)())onHide
{
    CCActionInterval *scaleToZero = [CCScaleTo actionWithDuration:0.5 scale:0];
    
    [spriteThePanel runAction:[CCEaseBackIn actionWithAction:scaleToZero]];
    [spriteThePanel runAction:[CCFadeOut actionWithDuration:0.5]];
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:0]];
    
    [self runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.5],
      [CCHide action],
      [CCCallBlock actionWithBlock:onHide],
      nil]];
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    [super dealloc];
}

@end
