//
//  SPLWordList.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLWordList.h"

@implementation SPLWordList

-(id)init
{
    if (self = [super init])
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Spellnew" ofType:@"sqlite"];
        fmdb = [[FMDatabase alloc] initWithPath:path];
        
        [fmdb open];

        self.defaultWordList = kEnglishInternationalWordList;
    }
    return self;
}

-(void)dealloc
{
    [fmdb close];
    [fmdb release];
    [super dealloc];
}


-(BOOL)doesWordExist:(NSString *)word
{
    word = [word uppercaseString];
    FMResultSet *result = [fmdb executeQuery:@"SELECT COUNT(*) as 'count' FROM wordlist_en WHERE word = ?", word];
    [result next];
    int i = [result intForColumn:@"count"];
    
    return i ? YES : NO;
}

-(NSString *)randomWordWithNumberOfLetters:(NSUInteger)count
{
    FMResultSet *result;
    NSLog(@"ts begin");
    result = [fmdb executeQuery:@"SELECT COUNT(*) AS 'count' FROM wordlist_en"];
    [result next];
    NSUInteger randomOffset = arc4random() % [result intForColumn:@"count"];
    result = [fmdb executeQuery:@"SELECT word FROM wordlist_en WHERE rowid = ?", @(randomOffset)];
    [result next];
    NSLog(@"ts end: %@", [result stringForColumn:@"word"]);
    return [result stringForColumn:@"word"];
}

@end
