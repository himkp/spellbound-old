//
//  SPLInteractiveItemMagicalBag.m
//  Spellbound
//
//  Created by Fleon Games on 1/19/14.
//
//

#import "SPLInteractiveItemMagicalBag.h"
#import "SPLTiledMap.h"
#import "SPLGameCharacter.h"

@implementation SPLInteractiveItemMagicalBag

- (id)init
{
    self = [super init];
    if (self) {
        spriteBack = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_MagicalBag_Back.png"];
        spriteFront = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_MagicalBag_Front.png"];
        
        spriteBack.anchorPoint = ccp(0, 0);
        spriteFront.anchorPoint = ccp(0, 0);
        
        spriteEmitterGlitter = [[CCParticleFireworks alloc] init];
        spriteEmitterGlitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"Assets/Common/Stars.png"];
        
        [self addChild:spriteBack z:0];
        [self addChild:spriteEmitterGlitter z:0];
        [self addChild:spriteFront z:10];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"MagicalBag"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"MagicalBag"];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_PickUp])
    {
        if (self.isPickedUp) return [action finish]; // can't pick up again if already picked up
        self.isPickedUp = YES;
        [self runAction:[CCScaleBy actionWithDuration:1 scale:0]];
        [self runAction:
         [CCSequence actions:
          [CCMoveTo actionWithDuration:1
                              position:ccpAdd(self.tiledMap.mainCharacter.position,
                                              ccp(self.tiledMap.tileSize.width / 2, self.tiledMap.tileSize.height / 2))],
          [CCHide action],
          [CCCallBlock actionWithBlock:^{
             [action finish];
         }],
          nil]];
    }
    else [super handleAction:action];
}

-(void)setParent:(CCNode *)parent
{
    [super setParent:parent];
    
    if (self.isPickedUp) {
        self.visible = NO;
        return;
    }
    
    // set size when added to the tiled map
    self.contentSize = CGSizeMake(self.tiledMap.tileSize.width, self.tiledMap.tileSize.height);
    
    // set the properties of the glitter, since they depend on the content size
    spriteEmitterGlitter.position = ccp(self.boundingBox.size.width / 2, 0.65 * self.boundingBox.size.height);
    spriteEmitterGlitter.blendAdditive = YES;
    spriteEmitterGlitter.life = 1.00;
    spriteEmitterGlitter.lifeVar = 0.25;
    spriteEmitterGlitter.scale = 0.55;
    spriteEmitterGlitter.speed = 0.86 * self.boundingBox.size.height;
    spriteEmitterGlitter.speedVar = 0.3 * self.boundingBox.size.height;
    spriteEmitterGlitter.posVar = ccp(0.45 * self.boundingBox.size.width, 0);
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    
    [spriteEmitterGlitter release];
    [super dealloc];
}

@end
