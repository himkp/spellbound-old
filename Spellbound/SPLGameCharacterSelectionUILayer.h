//
//  SPLGameCharacterSelectionUILayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "FLAction.h"

@interface SPLGameCharacterSelectionUILayer : CCLayer <FLActionDelegate>
{
    CCSprite *spriteBackground;
    
    CCSprite *spriteDrCyrus;
    CCSprite *spriteAmy;
    CCSprite *spriteElric;
    
    CCSprite *spriteAmyGlow;
    CCSprite *spriteElricGlow;
    
    CGPoint positionOfAmy;
    CGPoint positionOfDrCyrus;
    CGPoint positionOfElric;
    
    CGPoint positionOfBackground;
    CGPoint positionOfAmyGlow;
    CGPoint positionOfElricGlow;
    
    CGPoint positionOfDrCyrusIntially;
    CGPoint positionOfAmyInitially;
    CGPoint positionOfElricInitially;
    
    UITextField *characterNameInputTextField;
}

#define Action_ShowCharacterDrCyrus         FLActionMake(Action_ShowCharacterDrCyrus)
#define Action_IntroduceCharacters          FLActionMake(Action_IntroduceCharacters)
#define Action_HideCharacters               FLActionMake(Action_HideCharacters)

#define Action_ShowCharacterAmyGlow         FLActionMake(Action_ShowCharacterAmyGlow)
#define Action_ShowCharacterElricGlow       FLActionMake(Action_ShowCharacterElricGlow)
#define Action_HideCharacterAmyGlow         FLActionMake(Action_HideCharacterAmyGlow)
#define Action_HideCharacterElricGlow       FLActionMake(Action_HideCharacterElricGlow)

#define Action_ShowTextInputForName         FLActionMake(Action_ShowTextInputForName)

// Internal
#define Action_ShowCharacterAmy             FLActionMake(Action_ShowCharacterAmy)
#define Action_ShowCharacterElric           FLActionMake(Action_ShowCharacterElric)

-(void)initializePositions;

@end
