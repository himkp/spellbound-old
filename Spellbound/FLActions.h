//
//  FLActions.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@interface FLMoveTo : CCMoveTo
@end

@interface FLMoveBy : CCMoveBy
@end

@interface FLJumpBy : CCJumpBy
@end

@interface FLJumpTo : CCJumpTo
@end

@interface FLBezierTo : CCBezierTo 
@end

@interface FLBezierBy : CCBezierBy
@end

@interface FLPlace : CCPlace
@end