//
//  SPLInteractiveItem.h
//  Spellbound
//
//  Created by Fleon Games on 9/16/13.
//
//

#import "Spellbound.h"
@class SPLTiledMap;

@interface SPLInteractiveItem : CCSprite <FLActionDelegate, NSCoding>

@property (nonatomic, retain) NSString *name;
@property (nonatomic) BOOL hasInteractedWith;
@property (nonatomic) BOOL isPickedUp;

@property (nonatomic, readonly) SPLTiledMap *tiledMap;

+(SPLInteractiveItem *)createInteractiveItemOfType:(NSString *)itemType;

#define Action_InteractWith    FLActionMake(Action_InteractWith)
#define Action_PickUp          FLActionMake(Action_PickUp)

@end
