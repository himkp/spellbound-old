//
//  FLYAML.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FLYAML.h"
#import "YAMLSerialization.h"
#import "Spellbound.h"
#import "SPLDialogueManager.h"
#import "FLEventAggregator.h"

@implementation FLYAML

// TODO: change this to
// -(id)initWithYAMLActionDelegate:(id<FLYAMLDelegate, FLActionDelegate>)yamlDelegate
-(id)initWithYAMLDelegate:(id<FLYAMLDelegate>)yamlDelegate
{
    if (self = [super init])
    {
        _yamlDelegate = yamlDelegate;
        
        NSString *className = NSStringFromClass([_yamlDelegate class]);
        NSString *filepath = [[NSBundle mainBundle] pathForResource:className
                                                             ofType:@"yaml"];
        NSInputStream *file = [NSInputStream inputStreamWithFileAtPath:filepath];
        
        _rootYAMLNode = [[YAMLSerialization YAMLWithStream:file options:kYAMLReadOptionStringScalars error:nil] retain];
        
        [self extractDialogueSpeakers];
        [self extractDialogueSequences];
        [self extractMessages];
        [self extractActionSequences];

        [self extractEventActionBindings];
    }
    return self;
}

-(id)yamlNodeWithName:(NSString*)nodeName
{
    return [self yamlNodeWithName:nodeName andChildOf:_rootYAMLNode];
}

-(id)yamlNodeWithName:(NSString *)nodeName andChildOf:(id)yamlNode
{
    if ([yamlNode isKindOfClass:[NSArray class]])
        yamlNode = yamlNode[0];
    
    return [yamlNode valueForKey:nodeName];
}

-(void)extractDialogueSpeakers
{
    // Handle speakers
    NSArray* speakers = [self yamlNodeWithName:@"Speakers"];

    for (id speaker in speakers)
    {
        NSString* speakerName = [[speaker keyEnumerator] nextObject];
        NSDictionary* speakerMoods = [[speaker objectEnumerator] nextObject];
        
        SPLDialogueSpeaker* speaker = [SPLDialogueSpeaker speakerWithName:speakerName andMoods:speakerMoods];
        [[Spellbound dialogueManager] addSpeaker:speaker];
    }
}

// TODO: Variable replacements are not dynamic (should be replaced when the action is run).
-(void)extractDialogueSequences
{
    // Handle dialogues
    NSArray* dialogues = [self yamlNodeWithName:@"Dialogues"];
    
    for (id dialogueSequence in dialogues)
    {
        NSString* dialogueSequenceTitle = [[dialogueSequence keyEnumerator] nextObject];
        NSArray* dialogueSequenceArray = [[dialogueSequence objectEnumerator] nextObject];
        
        NSMutableArray* arrayOfDialogues = [NSMutableArray array];
        
        for (id dialogueDictionary in dialogueSequenceArray)
        {
            NSString* dialogueSpeakerName = [[dialogueDictionary keyEnumerator] nextObject];
            NSString* dialogueText = [[dialogueDictionary objectEnumerator] nextObject];
            dialogueText = [dialogueText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            dialogueText = [dialogueText stringByReplacingVariables];
            
            NSRange range1 = [dialogueSpeakerName rangeOfString:@"("];
            NSRange range2 = [dialogueSpeakerName rangeOfString:@")"];
            NSRange range3, range4;
            NSString* speakerMood = nil;
            
            if (range1.location != NSNotFound && range2.location != NSNotFound)
            {
                range3.location = range1.location + 1;
                range3.length = range2.location - range1.location - 1;
                range4.location = 0;
                range4.length = range1.location;
                
                speakerMood = [dialogueSpeakerName substringWithRange:range3];
                dialogueSpeakerName = [[dialogueSpeakerName substringWithRange:range4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                dialogueSpeakerName = [dialogueSpeakerName stringByReplacingVariables];
            }
            
            SPLDialogue* dialogue = [SPLDialogue dialogueWithSpeakerName:dialogueSpeakerName andText:dialogueText];
            if (speakerMood)
                dialogue.currentMoodOfSpeaker = speakerMood;
            
            [arrayOfDialogues addObject:dialogue];
        }
        
        SPLDialogueSequence* dialogueSequence = [SPLDialogueSequence dialogueSequenceWithTitle:dialogueSequenceTitle andDialogues:arrayOfDialogues];
        
        [[Spellbound dialogueManager] addDialogueSequence:dialogueSequence];
    }
}

-(void)extractActionSequences
{
    // Handle Action Sequences
    NSArray* yamlActionSequences = [self yamlNodeWithName:@"ActionSequences"];
    for (id yamlActionSequence in yamlActionSequences)
    {
        NSString* actionSequenceName = (NSString*) [[yamlActionSequence keyEnumerator] nextObject];
        NSArray* actionList = (NSArray*) [[yamlActionSequence objectEnumerator] nextObject];
        id<FLActionDelegate> actionDelegate = (id<FLActionDelegate>) _yamlDelegate;
        
        FLAction* firstAction = [FLYAML linkActionSequence:actionList withActionDelegate:actionDelegate];
        
        FLActionSequence *actionSequence = [FLActionSequence actionSequenceWithActionDelegate:actionDelegate
                                                                               andFirstAction:firstAction
                                                                                      andName:actionSequenceName];
        
        [[Spellbound actionSequenceManager] addActionSequence:actionSequence];
    }
}

-(void)extractVariables
{
    NSArray *variables = [self yamlNodeWithName:@"Variables"];
    for (id variable in variables)
    {
        // TODO: fill in
    }
}

-(void)extractMessages
{
    NSArray *messages = [self yamlNodeWithName:@"Messages"];
    for (id message in messages)
    {
        NSString *messageTitle = [[message keyEnumerator] nextObject];
        NSString *messageText = [[message objectEnumerator] nextObject];
        
        messageTitle = [messageTitle stringByReplacingVariables];
        messageTitle = [messageTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        messageText = [messageText stringByReplacingVariables];
        messageText = [messageText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [[Spellbound messageManager] addMessage:messageText withTitle:messageTitle];
    }
}

+(FLAction *)linkActionSequence:(id)yamlActionSequence
             withActionDelegate:(id<FLActionDelegate>)actionDelegate
{
    FLAction* firstAction = nil, *newAction;
    for (id yamlAction in [yamlActionSequence reverseObjectEnumerator])
    {
        id<FLActionDelegate> thisActionDelegate = actionDelegate;
        NSString *actionName;
        id actionData;
        if ([FLYAML isNodeNSString:yamlAction])
        {
            actionName = yamlAction;
            actionData = nil;
        }
        else if ([FLYAML isNodeNSDictionary:yamlAction])
        {
            actionName = [[yamlAction keyEnumerator] nextObject];
            actionData = [[yamlAction objectEnumerator] nextObject];
        } else continue;

        if ([actionName rangeOfString:@"."].location != NSNotFound) {
            NSArray *actionComponents = [actionName componentsSeparatedByString:@"."];
            NSLog(@"action components: %@", actionComponents);
            thisActionDelegate = [[Spellbound actionDelegateManager] actionDelegateWithName:actionComponents[0]];
            actionName = actionComponents[1];
        }
        
        if ([actionName rangeOfString:@"("].location != NSNotFound) {
            NSRange range1 = [actionName rangeOfString:@"("];
            NSRange range2 = [actionName rangeOfString:@")"];
            NSRange range3, range4;
            NSMutableArray* actionParams = nil;

            if (range1.location != NSNotFound && range2.location != NSNotFound)
            {
                range3.location = range1.location + 1;
                range3.length = range2.location - range1.location - 1;
                range4.location = 0;
                range4.length = range1.location;

                actionParams = [[[[actionName substringWithRange:range3] componentsSeparatedByString:@","] mutableCopy] autorelease];
                for (NSUInteger i = 0; i < actionParams.count; i++)
                    actionParams[i] = [actionParams[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                actionName = [[actionName substringWithRange:range4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                actionData = @{@"data": actionData, @"params": actionParams};
            }
        }

        newAction = [FLAction actionWithActionDelegate:thisActionDelegate
                                               andName:actionName];
        newAction.data = actionData;
        
        if ([actionName isEqualToString:@"CheckPoint"])
            [[Spellbound checkPointManager] registerCheckPoint:newAction];

        if (firstAction)
            newAction.subsequentAction = firstAction;
        firstAction = newAction;
    }
    
    return firstAction;
}

-(void)extractEventActionBindings
{
    // Handle event-action bindings
    NSArray* eventActionBindings = [self yamlNodeWithName:@"EventActionBindings"];
    
    for (id eventActionBinding in eventActionBindings)
    {
        NSString* eventName = (NSString*) [[eventActionBinding keyEnumerator] nextObject];
        NSString* actionSequenceName = (NSString*) [[eventActionBinding objectEnumerator] nextObject];
        
        id eventData = nil;
        FLEvent* theEvent = [[FLEventAggregator globalEventAggregator] eventWithParameterizedName:eventName eventData:&eventData];
        FLActionSequence *actionSequence = [[Spellbound actionSequenceManager] actionSequenceWithName:actionSequenceName];
        
        if (theEvent) [theEvent bindToAction:actionSequence andDispatchOnlyIfDataEquals:eventData];
    }
}

+(id)yamlWithYAMLDelegate:(id<FLYAMLDelegate>)yamlDelegate
{
    return [[[FLYAML alloc] initWithYAMLDelegate:yamlDelegate] autorelease];
}

+(BOOL)isNodeNSDictionary:(id)yamlNode
{
    return [yamlNode isKindOfClass:[NSDictionary class]];
}

+(BOOL)isNodeNSArray:(id)yamlNode
{
    return [yamlNode isKindOfClass:[NSArray class]];
}

+(BOOL)isNodeNSString:(id)yamlNode
{
    return [yamlNode isKindOfClass:[NSString class]];
}

-(void)dealloc
{
    [_rootYAMLNode release];
    [super dealloc];
}



@end