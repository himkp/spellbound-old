//
//  FLActions.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FLActions.h"
#import "FL.h"

@implementation FLBezierBy

-(id)initWithDuration:(ccTime)t bezier:(ccBezierConfig)c
{
    c.endPosition = [FL translatePoint:c.endPosition];
    c.controlPoint_1 = [FL translatePoint:c.controlPoint_1];
    c.controlPoint_2 = [FL translatePoint:c.controlPoint_2];
    
    return [super initWithDuration:t bezier:c];
}

@end

@implementation FLBezierTo

-(id)initWithDuration:(ccTime)t bezier:(ccBezierConfig)c
{
    c.endPosition = [FL translatePoint:c.endPosition];
    c.controlPoint_1 = [FL translatePoint:c.controlPoint_1];
    c.controlPoint_2 = [FL translatePoint:c.controlPoint_2];
    
    return [super initWithDuration:t bezier:c];
}

@end

@implementation FLJumpBy

-(id)initWithDuration:(ccTime)duration position:(CGPoint)position height:(ccTime)height jumps:(NSUInteger)jumps
{
    position = [FL translatePoint:position];
    return [super initWithDuration:duration position:position height:height jumps:jumps];
}

@end

@implementation FLJumpTo

-(id)initWithDuration:(ccTime)duration position:(CGPoint)position height:(ccTime)height jumps:(NSUInteger)jumps
{
    position = [FL translatePoint:position];
    return [super initWithDuration:duration position:position height:height jumps:jumps];
}

@end

@implementation FLMoveBy

-(id)initWithDuration:(ccTime)duration position:(CGPoint)position
{
    position = [FL translatePoint:position];
    return [super initWithDuration:duration position:position];
}

@end

@implementation FLMoveTo

-(id)initWithDuration:(ccTime)duration position:(CGPoint)position
{
    position = [FL translatePoint:position];
    return [super initWithDuration:duration position:position];
}

@end

@implementation FLPlace

-(id)initWithPosition:(CGPoint)pos
{
    pos = [FL translatePoint:pos];
    return [super initWithPosition:pos];
}

@end