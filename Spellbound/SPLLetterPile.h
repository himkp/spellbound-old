//
//  SPLLetterPile.h
//  Spellbound
//
//  Created by Fleon Games on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLLetterPile : NSObject <NSCoding>

@property (nonatomic, readonly) int numberOfLettersInBag;
@property (nonatomic, readonly) int numberOfLettersInHand;

@property (nonatomic, retain) NSMutableString* lettersInHand;
@property (nonatomic, retain) NSMutableString* lettersInBag;

-(id)initWithNumberOfLettersInHand:(int)numberOfLettersInHand andInBag:(int)numberOfLettersInBag;
+(id)letterPileWithNumberOfLettersInHand:(int)numberOfLettersInHand andInBag:(int)numberOfLettersInBag;

-(void)refillLettersInBag:(int)numberOfLettersToAdd;
-(void)refillLettersInHand;

-(void)insertLetterInHand:(NSString *)letter;

-(void)prependLettersInBag:(NSString *)letterString;

@end
