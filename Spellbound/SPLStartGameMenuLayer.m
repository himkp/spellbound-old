//
//  SPLNewGameMenuLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/31/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SPLStartGameMenuLayer.h"
#import "SPLGameCharacterSelectionScene.h"
#import "SPLTitleScene.h"
#import "SPLGameScene.h"

@implementation SPLStartGameMenuLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfBackground            = fl_ccp(   0,    0, 2048, 1536);
        positionOfWhiteUnderlay         = fl_ccp( 470,  220, 1140, 1240);
        positionOfMainMenu              = fl_ccp(   0,    0, 2048, 1536);
        
        positionOfMenuBorderTop         = fl_ccp( 457,   68, 1079,  262);
        positionOfMenuBorderBottom      = fl_ccp( 428, 1303, 1190,  166);
        positionOfMenuBorderRight       = fl_ccp(1382,  330,  154,  973);
        positionOfMenuBorderLeft        = fl_ccp( 514,  330,  144,  973);
        
        positionOfButtonContinue        = fl_ccp( 684,  330,  675,  190);
        positionOfButtonNewGame         = fl_ccp( 685,  520,  674,  192);
        positionOfButtonSettings        = fl_ccp( 685,  712,  674,  205);
        positionOfButtonCredits         = fl_ccp( 684,  917,  684,  192);
        positionOfButtonMoreGames       = fl_ccp( 685, 1109,  671,  194);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfBackground            = fl_ccp(   0,    0,  960,  640);
        positionOfMainMenu              = fl_ccp(   0,    0,  960,  640);
        
        positionOfMenuBorderTop         = fl_ccp( 236,   11,  489,  110);
        positionOfMenuBorderLeft        = fl_ccp( 263,  121,   57,  439);
        positionOfMenuBorderRight       = fl_ccp( 653,  121,   62,  439);
        positionOfMenuBorderBottom      = fl_ccp( 236,  560,  505,   75);
        
        positionOfButtonContinue        = fl_ccp( 338,  121,  304,   86);
        positionOfButtonNewGame         = fl_ccp( 338,  207,  304,   87);
        positionOfButtonSettings        = fl_ccp( 338,  294,  304,   92);
        positionOfButtonCredits         = fl_ccp( 338,  386,  308,   86);
        positionOfButtonMoreGames       = fl_ccp( 338,  472,  304,   88);
    }
}

-(id)init
{
    if (self=[super init])
    {
        [self initializePositions];
        
        self.visible = NO;
        
        self.isTouchEnabled = YES;
        spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteBackground.scaleX = DEVICE_IS_IPAD ? 1024 : 480;
        spriteBackground.scaleY = DEVICE_IS_IPAD ? 768 : 320;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteBackground.scaleX *= 2;
            spriteBackground.scaleY *= 2;
        }
        spriteBackground.position = positionOfBackground;
        
        spriteWhiteUnderlay = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteWhiteUnderlay.scaleX = 450;
        spriteWhiteUnderlay.scaleY = 570;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteWhiteUnderlay.scaleX *= 2;
            spriteWhiteUnderlay.scaleY *= 2;
        }
        spriteWhiteUnderlay.position = positionOfWhiteUnderlay;
        
        spriteMainMenu = [CCSprite node];
        spriteMainMenu.contentSize = [CCDirector sharedDirector].winSize;
        spriteMainMenu.position = positionOfMainMenu;
        
        spriteMenuBorderTop = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Border_Top.png"];
        spriteMenuBorderLeft = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Border_Left.png"];
        spriteMenuBorderBottom = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Border_Bottom.png"];
        spriteMenuBorderRight = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Border_Right.png"];
        
        spriteMenuBorderTop.position = positionOfMenuBorderTop;
        spriteMenuBorderRight.position = positionOfMenuBorderRight;
        spriteMenuBorderBottom.position = positionOfMenuBorderBottom;
        spriteMenuBorderLeft.position = positionOfMenuBorderLeft;
        
        [self addChild:spriteBackground z:1];
        [spriteMainMenu addChild:spriteWhiteUnderlay];
        [spriteMainMenu addChild:spriteMenuBorderTop];
        [spriteMainMenu addChild:spriteMenuBorderLeft];
        [spriteMainMenu addChild:spriteMenuBorderBottom];
        [spriteMainMenu addChild:spriteMenuBorderRight];
        [self addChild:spriteMainMenu z:2];
        
        _spriteButtonContinue = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Button_Continue.png"];
        _spriteButtonNewGame = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Button_NewGame.png"];
        _spriteButtonSettings = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Button_Settings.png"];
        _spriteButtonCredits = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Button_Credits.png"];
        _spriteButtonMoreGames = [CCSprite spriteWithSpriteFrameName:@"MainScreenMenu_Button_MoreGames.png"];
        
        // if save data does not exist, reduce the continue button's opacity
        if (![[Spellbound localStorage] dataExistsForKey:@"saveData"])
        {
            _spriteButtonContinue.opacity = 140;
        }
        _spriteButtonMoreGames.opacity = 140;
        
        _spriteButtonContinue.position = positionOfButtonContinue;
        _spriteButtonNewGame.position = positionOfButtonNewGame;
        _spriteButtonSettings.position = positionOfButtonSettings;
        _spriteButtonCredits.position = positionOfButtonCredits;
        _spriteButtonMoreGames.position = positionOfButtonMoreGames;
        
        [spriteMainMenu addChild:_spriteButtonContinue z:3];
        [spriteMainMenu addChild:_spriteButtonNewGame z:3];
        [spriteMainMenu addChild:_spriteButtonSettings z:3];
        [spriteMainMenu addChild:_spriteButtonCredits z:3];
        [spriteMainMenu addChild:_spriteButtonMoreGames z:3];
        
    }
    return self;
}

-(void)show
{
    self.visible = YES;
    spriteBackground.opacity= 0;
    spriteMainMenu.scale = 0;
    _spriteButtonContinue.scale = 0;
    _spriteButtonCredits.scale = 0;
    _spriteButtonMoreGames.scale = 0;
    _spriteButtonNewGame.scale = 0;
    _spriteButtonSettings.scale = 0;
    
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:180]];
    
    CCActionInterval *action = [CCEaseBackOut actionWithAction:[CCScaleTo actionWithDuration:0.5 scale:1]];
    
    [spriteMainMenu runAction:
     [CCEaseBackOut actionWithAction:
      [CCScaleTo actionWithDuration:0.5 scale:1]]];
    
    [_spriteButtonContinue runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.35] two:[action.copy autorelease]]];
    [_spriteButtonNewGame runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.50] two:[action.copy autorelease]]];
    [_spriteButtonSettings runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.65] two:[action.copy autorelease]]];
    [_spriteButtonCredits runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.80] two:[action.copy autorelease]]];
    [_spriteButtonMoreGames runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.95] two:[action.copy autorelease]]];
}

-(void)hide
{
    CCActionInterval *scaleToZero = [CCScaleTo actionWithDuration:0.5 scale:0];
    
    [spriteMainMenu runAction:[CCEaseBackIn actionWithAction:scaleToZero]];
    [spriteMainMenu runAction:[CCFadeOut actionWithDuration:0.5]];
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:0]];
    
    [self runAction:
     [CCSequence actionOne:[CCDelayTime actionWithDuration:0.75]
                       two:[CCHide action]]];
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (self.visible)
    {
        if ([FL doesSprite:_spriteButtonContinue containPoint:point])
        {
            if ([[Spellbound localStorage] dataExistsForKey:@"saveData"])
            {
                [FL with:_spriteButtonContinue runAnimationAction:kFLAnimationActionShrinkAndGrow];
                [self hide];
                NSData *saveData = [[Spellbound localStorage] dataForKey:@"saveData"];
                SPLGameScene *gameScene = [NSKeyedUnarchiver unarchiveObjectWithData:saveData];

                [[CCDirector sharedDirector] replaceScene:
                 [CCTransitionFade transitionWithDuration:1 scene:gameScene]];
            }
            return YES;
        } else if ([FL doesSprite:_spriteButtonNewGame containPoint:point]) {
            // todo: confirm overriding save data before continuing
            [FL with:_spriteButtonNewGame runAnimationAction:kFLAnimationActionShrinkAndGrow];
            [self hide];
            
            [[Spellbound audioPlayer] playTrack:@"02_PickYourCharacter.mp3"];
            [[CCDirector sharedDirector] replaceScene:
             [CCTransitionFade transitionWithDuration:1 scene:[SPLGameCharacterSelectionScene node]]];
            
            return YES;
        } else if ([FL doesSprite:_spriteButtonCredits containPoint:point]) {
            [FL with:_spriteButtonCredits runAnimationAction:kFLAnimationActionShrinkAndGrow];
            [((SPLTitleScene*) self.parent).creditsPanelLayer show];
            return YES;
        } else if ([FL doesSprite:_spriteButtonSettings containPoint:point]) {
            [FL with:_spriteButtonSettings runAnimationAction:kFLAnimationActionShrinkAndGrow];
            [((SPLTitleScene*) self.parent).settingsPanelLayer show];
            return YES;
        } else if ([FL doesSprite:_spriteButtonMoreGames containPoint:point]) {
            // [FL with:_spriteButtonMoreGames runAnimationAction:kFLAnimationActionShrinkAndGrow];
            NSLog(@"No more games yet. This is the only one. :)");
            return YES;
        }
        else if ([FL doesSprite:spriteMenuBorderTop containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderLeft containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderRight containPoint:point] ||
                 [FL doesSprite:spriteMenuBorderBottom containPoint:point]) {
            NSLog(@"No point touching the wooden border eh?");
            return YES;
        }
        else {
            [self hide];
            return YES;
        }
    }
    else return NO;
}

@end
