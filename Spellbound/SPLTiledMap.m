//
//  SPLTilemap.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLTiledMap.h"
#import "SPLWordList.h"
#import "FLAStarPathfinder.h"
#import "SPLConnectorBlockSprite.h"

#import "SPLInteractiveItem.h"

// not supposed to import this, just for the hack
#import "SPLCoreGameplayLayer.h"
#import "SPLGameCharacter.h"
#import "SPLGameScene.h"
#import "SPLGameplayButtonsLayer.h"

@implementation SPLTileDescription

+(id)tileDescriptionWithName:(NSString *)name andType:(SPLTileType)type
{
    return [[[self alloc] initWithName:name andType:type] autorelease];
}

-(id)initWithName:(NSString *)name andType:(SPLTileType)type
{
    if (self = [super init])
    {
        self.name = name;
        self.type = type;
    }
    return self;
}

- (void)dealloc
{
    [_name release];
    [super dealloc];
}

@end

@implementation SPLTiledMap

@synthesize connectorPath, charactersLayer;

-(void)hideReferenceLayers
{
    NSArray *layersToHide = @[@"Water%d", @"Island%dLand", @"Island%dCliff", @"Exit%d"];
    for (NSUInteger i = 1; i <= 20; i++)
    {
        for (NSUInteger j = 0; j < layersToHide.count; j++)
        {
            CCTMXLayer *layer = [self layerNamed:[NSString stringWithFormat:layersToHide[j], i]];
            if (layer) layer.visible = NO;
        }
    }
    
    // hide ocean waves as well
    [self layerNamed:@"ocean waves"].visible = NO;

}

-(id)initWithTMXFile:(NSString *)tmxFile
{
    if (self = [super initWithTMXFile:tmxFile])
    {
        [self hideReferenceLayers];
        
        _characters = [[NSMutableArray alloc] init];
        _interactiveObjects = [[NSMutableArray alloc] init];
        
        // initialize all characters
        NSArray *allCharacters = [self objectGroupNamed:@"Characters"].objects;
        for (NSDictionary *characterObject in allCharacters)
        {
            if ([characterObject[@"name"] isEqualToString:@"Player"]) continue;
            SPLGameCharacter *character = [SPLGameCharacter characterWithName:characterObject[@"name"]];
            NSLog(@"creating character: %@", character.characterName);
            [self.charactersLayer addChild:character];
            character.position = [self boundingBoxOfCharacterNamed:characterObject[@"name"]].origin;
            
            [_characters addObject:character];
        }
        
        NSArray *allItems = [self objectGroupNamed:@"InteractiveObjects"].objects;
        for (NSDictionary *itemObject in allItems)
        {
            SPLInteractiveItem *item = [SPLInteractiveItem createInteractiveItemOfType:itemObject[@"type"]];
            item.name = itemObject[@"name"];
            
            [self addChild:item z:[self layerNamed:@"Character"].zOrder];
            item.position = [self boundingBoxOfInteractiveObjectNamed:itemObject[@"name"]].origin;
            
            [_interactiveObjects addObject:item];
        }
    }
    return self;
}

-(id)initWithMapName:(NSString *)name
{
    NSString *mapURL = [NSString stringWithFormat:@"%@/Maps/%@.tmx", DEVICE_IS_IPAD ? @"iPad" : @"iPhone", name];

    if (self = [self initWithTMXFile:mapURL]) {
        _mapName = [name retain];
    }
    return self;
}

+(id)tiledMapWithMapName:(NSString *)name
{
    return [[[self alloc] initWithMapName:name] autorelease];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    NSString *mapName = [decoder decodeObjectForKey:@"mapName"];
    NSString *mapURL = [NSString stringWithFormat:@"%@/Maps/%@.tmx", DEVICE_IS_IPAD ? @"iPad" : @"iPhone", mapName];
    if (self = [super initWithTMXFile:mapURL]) {
        [self hideReferenceLayers];
        
        _mapName = [mapName retain];
        landsBridged = [[decoder decodeObjectForKey:@"landsBridged"] retain];
        self.position = [decoder decodeCGPointForKey:@"position"];
        NSDictionary *letterBlocks = [decoder decodeObjectForKey:@"letterBlocks"];
        
        for (NSValue *location in letterBlocks) {
            SPLLetterBlockSprite *sprite = [(SPLLetterBlock *) letterBlocks[location] letterSprite];
            CGPoint spritePosition = sprite.position;
            [self addLetterSprite:sprite atPoint:location.CGPointValue];
            [sprite stopAllActions];
            sprite.position = spritePosition;
            SPLLetterBlock *block = _letterBlocks[location];
            block.isFrozen = [(SPLLetterBlock *) letterBlocks[location] isFrozen];
            block.isConnectedToACliffBlock = [(SPLLetterBlock *) letterBlocks[location] isConnectedToACliffBlock];
            block.areAdjacentBlocksConnectedToACliffBlock = [(SPLLetterBlock *) letterBlocks[location] areAdjacentBlocksConnectedToACliffBlock];
        }
        
        _mainCharacterPosition = [decoder decodeCGPointForKey:@"mainCharacterPosition"];
        _characters = [[decoder decodeObjectForKey:@"characters"] mutableCopy];
        NSLog(@"decode: characters on map (%@):%@", self.mapName, _characters);
        for (SPLGameCharacter *character in _characters)
        {
            NSLog(@"added character: %@", character.characterName);
            [self.charactersLayer addChild:character];
        }
        
        _interactiveObjects = [[decoder decodeObjectForKey:@"interactiveObjects"] mutableCopy];
        
        for (SPLInteractiveItem *item in _interactiveObjects)
            [self addChild:item z:[self layerNamed:@"Character"].zOrder];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_mapName forKey:@"mapName"];
    [encoder encodeObject:landsBridged forKey:@"landsBridged"];
    [encoder encodeObject:self.letterBlocks forKey:@"letterBlocks"];
    [encoder encodeCGPoint:position_ forKey:@"position"];

    [encoder encodeCGPoint:self.mainCharacter.position forKey:@"mainCharacterPosition"];
    [encoder encodeObject:_interactiveObjects forKey:@"interactiveObjects"];
    [encoder encodeObject:_characters forKey:@"characters"];
    NSLog(@"encode: characters on map (%@):%@", self.mapName, _characters);
    // [encoder encodeObject:self.mainCharacter forKey:@"mainCharacter"];
}

-(SPLGameCharacter *)mainCharacter // currently a hack
// fix later by using proper storage mechanism for character references
{
    return [(SPLCoreGameplayLayer *) parent_.parent elric];
}

-(SPLTileType)tileTypeAt:(CGPoint)point
{
    if (point.x < 0 || point.y < 0 || point.x >= self.mapSize.width || point.y >= self.mapSize.height)
        return SPLTileTypeObstruction;
    
    SPLTileType returnValue = SPLTileTypeObstruction;
    
    for (SPLGameCharacter *character in _characters)
    {
        CGPoint charTilePosition = [self convertPositionToTilePosition:character.position];
        // return only for Characters that aren't following the player.
        if (charTilePosition.x == point.x && charTilePosition.y == point.y && ![self.mainCharacter.charactersFollowing containsObject:character])
            return SPLTileTypeCharacter;
    }
    
    if ([self interactiveObjectUnderPoint:[self convertToWorldSpace:[self convertTilePositionToPosition:point]]])
        return SPLTileTypeInteractiveObject;
    
    if ([self doesLetterExistAtPoint:point])
        return [self getLetterBlockAtPoint:point].isFrozen ? SPLTileTypeLetterFrozen : SPLTileTypeLetterFloating;
    
    if (!tileTypesCache)
        tileTypesCache = [[NSMutableDictionary dictionaryWithCapacity:10] retain];
    
    // TODO: cache is no longer needed
    NSNumber *cachedTileType = tileTypesCache[[NSValue valueWithCGPoint:point]];
    if (cachedTileType) return (SPLTileType) cachedTileType.integerValue;

    for (NSUInteger i = 1; i <= 20; i++)
    {
        CCTMXLayer *waterLayer = [self layerNamed:[NSString stringWithFormat:@"Water%d", i]];
        if (waterLayer && [waterLayer tileGIDAt:point] > 0) {
            returnValue = SPLTileTypeWater;
            break;
        }
        
        CCTMXLayer *landLayer = [self layerNamed:[NSString stringWithFormat:@"Island%dLand", i]];
        if (landLayer && [landLayer tileGIDAt:point] > 0) {
            returnValue = SPLTileTypeLand;
            break;
        }
        
        CCTMXLayer *cliffLayer = [self layerNamed:[NSString stringWithFormat:@"Island%dCliff", i]];
        if (cliffLayer && [cliffLayer tileGIDAt:point] > 0) {
            returnValue = SPLTileTypeCliff;
            break;
        }
    }
    
    tileTypesCache[[NSValue valueWithCGPoint:point]] = @(returnValue);
    return returnValue;
}

-(SPLTileDescription *)tileDescriptionAt:(CGPoint)point
{
    if (point.x < 0 || point.y < 0 || point.x >= self.mapSize.width || point.y >= self.mapSize.height)
        return [SPLTileDescription tileDescriptionWithName:@"Obstruction" andType:SPLTileTypeObstruction];
    // todo: change this
    for (NSUInteger i = 1; i <= 20; i++)
    {
        NSString *layerName = [NSString stringWithFormat:@"Water%d", i];
        CCTMXLayer *waterLayer = [self layerNamed:layerName];
        if (waterLayer && [waterLayer tileGIDAt:point] > 0)
            return [SPLTileDescription tileDescriptionWithName:layerName andType:SPLTileTypeWater];
    }
    for (NSUInteger i = 1; i <= 20; i++)
    {
        NSString *layerName = [NSString stringWithFormat:@"Exit%d", i];
        CCTMXLayer *exitLayer = [self layerNamed:layerName];
        if (exitLayer && [exitLayer tileGIDAt:point] > 0)
            return [SPLTileDescription tileDescriptionWithName:layerName andType:SPLTileTypeLand];
    }
    for (NSUInteger i = 1; i <= 20; i++)
    {
        NSString *layerName = [NSString stringWithFormat:@"Island%dLand", i];
        CCTMXLayer *landLayer = [self layerNamed:layerName];
        if (landLayer && [landLayer tileGIDAt:point] > 0)
            return [SPLTileDescription tileDescriptionWithName:layerName andType:SPLTileTypeLand];
    }
    for (NSUInteger i = 1; i <= 20; i++)
    {
        NSString *layerName = [NSString stringWithFormat:@"Island%dCliff", i];
        CCTMXLayer *cliffLayer = [self layerNamed:layerName];
        if (cliffLayer && [cliffLayer tileGIDAt:point] > 0)
            return [SPLTileDescription tileDescriptionWithName:layerName andType:SPLTileTypeCliff];
    }
    
    return [SPLTileDescription tileDescriptionWithName:@"Obstruction" andType:SPLTileTypeObstruction];
}


-(void)addLetterSprite:(SPLLetterBlockSprite *)letterSprite atPoint:(CGPoint)point
{
    if (!_lettersLayer)
    {
        _lettersLayer = [CCSprite node];
        [self addChild:_lettersLayer z:[self layerNamed:@"letters"].zOrder + 1];
    }
    
    [_lettersLayer addChild:letterSprite z:point.x * self.mapSize.width + point.y];
    
    if (!_letterBlocks)
        _letterBlocks = [[NSMutableDictionary dictionaryWithCapacity:10] retain];
    
    CGPoint oldPosition = ccpAdd(letterSprite.position, ccpNeg(self.position));
    CGPoint newPosition = [[self layerNamed:@"ocean waves"] tileAt:point].position;
    CGPoint positionDifference = ccp(self.tileSize.width / CC_CONTENT_SCALE_FACTOR() / 2,
                                     -self.tileSize.height / CC_CONTENT_SCALE_FACTOR() / 2);
    oldPosition = ccpAdd(oldPosition, ccp(0, positionDifference.y * 2));
    newPosition = ccpAdd(newPosition, ccp(positionDifference.x, positionDifference.y));
    
    letterSprite.position = oldPosition;
    [letterSprite runAction:[CCMoveTo actionWithDuration:0.25 position:newPosition]];
    [FL with:letterSprite runAnimationAction:kFLAnimationActionShrink];
    
    SPLTileType tileType = [self tileTypeAt:point];
    if (tileType != SPLTileTypeWater)
    {
        NSLog(@"Whoa there! Cannot add a letter at (%i, %i) on a %@ tile.", (int) point.x, (int) point.y, SPLTileTypeToNSString(tileType));
        return;
    }
    
    SPLLetterBlock* letterBlock = [SPLLetterBlock letterBlockWithSprite:letterSprite onMap:self atLocation:point];
    _letterBlocks[[NSValue valueWithCGPoint:point]] = letterBlock;
}

-(void)removeLetterAtPoint:(CGPoint)point
{
    if (_letterBlocks == nil) return;
    if ([self doesLetterExistAtPoint:point])
    {
        SPLLetterBlock *letterBlock = _letterBlocks[[NSValue valueWithCGPoint:point]];
        [_lettersLayer removeChild:letterBlock.letterSprite cleanup:YES];
        [_letterBlocks removeObjectForKey:[NSValue valueWithCGPoint:point]];
    }
}

-(BOOL)doesLetterExistAtPoint:(CGPoint)point
{
    if (_letterBlocks == nil) return NO;
    return _letterBlocks[[NSValue valueWithCGPoint:point]] != nil;
}

-(NSString *)getLetterAtPoint:(CGPoint)point
{
    return [[self getLetterBlockAtPoint:point] letter];
}

-(SPLLetterBlock *)getLetterBlockAtPoint:(CGPoint)point
{
    if (_letterBlocks == nil) return nil;
    return (SPLLetterBlock *) _letterBlocks[[NSValue valueWithCGPoint:point]];
}

-(BOOL)canLetterBeAddedAtPoint:(CGPoint)point
{
    return [self tileTypeAt:point] == SPLTileTypeWater;
}

-(NSArray *)freezeLetters:(NSString **)errorMessage
{
    *errorMessage = nil;
    
    NSMutableDictionary *floatingLetterBlocks = [NSMutableDictionary dictionaryWithCapacity:10];
    for (NSValue* key in _letterBlocks)
    {
        SPLLetterBlock *letterBlock = _letterBlocks[key];
        if (!letterBlock.isFrozen) {
            letterBlock.isConnectedToACliffBlock = FLMaybe;
            floatingLetterBlocks[key] = letterBlock;
        }
    }
    
    for (NSValue* key in _letterBlocks)
    {
        SPLLetterBlock *letterBlock = _letterBlocks[key];
        if (letterBlock.isConnectedToACliffBlock == FLYes)
        {
            letterBlock.areAdjacentBlocksConnectedToACliffBlock = FLYes;
        }
    }
    
    NSMutableArray *wordsCreated = [NSMutableArray array];

    // verify the words and throw errors if any
    for (NSValue* key in floatingLetterBlocks)
    {
        SPLLetterBlock *letterBlock = _letterBlocks[key];
        if (letterBlock.isConnectedToACliffBlock == FLNo ||
            letterBlock.isConnectedToACliffBlock == FLMaybe)
        {
            *errorMessage = @"The letter(s) you have added aren't\nconnected to the island player is standing on!";
            return nil;
        }
        NSLog(@"vertical word: %@, horizontal word: %@, at point: %@", letterBlock.verticalWord, letterBlock.horizontalWord, NSStringFromCGPoint(letterBlock.location));
        
        NSLog(@"'%@' exists in the dictionary: %@", letterBlock.verticalWord, [[Spellbound wordList] doesWordExist:letterBlock.verticalWord] ? @"YES": @"NO");
        NSLog(@"'%@' exists in the dictionary: %@", letterBlock.horizontalWord, [[Spellbound wordList] doesWordExist:letterBlock.horizontalWord] ? @"YES": @"NO");
        
        NSString *horizontalWord = letterBlock.horizontalWord,
                 *verticalWord = letterBlock.verticalWord;

        if (verticalWord.length == 1 && horizontalWord.length == 1)
        {
            *errorMessage = [NSString stringWithFormat:@"INVALID WORD: %@", verticalWord];
        }
        if (!([[Spellbound wordList] doesWordExist:horizontalWord] || horizontalWord.length == 1))
        {
            *errorMessage = [NSString stringWithFormat:@"INVALID WORD: %@", horizontalWord];
        }
        if (!([[Spellbound wordList] doesWordExist:verticalWord] || verticalWord.length == 1))
        {
            *errorMessage = [NSString stringWithFormat:@"INVALID WORD: %@", verticalWord];
        }
        
        if (horizontalWord.length > 1 && ![wordsCreated containsObject:horizontalWord])
            [wordsCreated addObject:horizontalWord];
        if (verticalWord.length > 1 && ![wordsCreated containsObject:verticalWord])
            [wordsCreated addObject:verticalWord];
    }

    __block NSUInteger pointsEarned = 0;

    SPLGameplayButtonsLayer *gameplayButtonsLayer = [(SPLGameScene *) self.parent.parent.parent gameplayButtonsLayer];
    
    struct {
        CGPoint topLeftCorner;
        CGPoint bottomRightCorner;
    } floatingLettersBoundingBox;
    
    CGPoint floatingLettersCenter;
    
    floatingLettersBoundingBox.topLeftCorner = ccp(NSIntegerMax, NSIntegerMax);

    // compute points and do an animation - basic
    for (SPLLetterBlock *letterBlock in floatingLetterBlocks.objectEnumerator)
    {
        // calculate the boundingBox to display the advanced bonus score in
        for (NSString *direction in @[@"Top", @"Bottom", @"Left", @"Right"]) {
            direction = [@"letterBlockToThe" stringByAppendingString:direction];
            SEL letterBlockSelector = NSSelectorFromString(direction);
            for (SPLLetterBlock *letterBlockIterator = letterBlock;
                 letterBlockIterator != nil;
                 letterBlockIterator = [letterBlockIterator performSelector:letterBlockSelector]) {
                floatingLettersBoundingBox.topLeftCorner.x = min(floatingLettersBoundingBox.topLeftCorner.x, letterBlockIterator.letterSprite.position.x);
                floatingLettersBoundingBox.topLeftCorner.y = min(floatingLettersBoundingBox.topLeftCorner.y,
                                                                 letterBlockIterator.letterSprite.position.y + letterBlockIterator.letterSprite.tileSize.height);
                floatingLettersBoundingBox.bottomRightCorner.x = max(floatingLettersBoundingBox.bottomRightCorner.x, letterBlockIterator.letterSprite.position.x);
                floatingLettersBoundingBox.bottomRightCorner.y = max(floatingLettersBoundingBox.bottomRightCorner.y,
                                                                     letterBlockIterator.letterSprite.position.y + letterBlockIterator.letterSprite.tileSize.height);
            }
        }
        
        // skip the point computation part if error
        if (*errorMessage) continue;
        
        NSString *pointValueString = [NSString stringWithFormat:@"%d", letterBlock.letter.pointValue];
        __block CCLabelBMFont *labelLetterPoints = [CCLabelBMFont labelWithString:pointValueString fntFile:@"iPad/Fonts/Grobold.fnt"];

        labelLetterPoints.position = [gameplayButtonsLayer.spriteButtonCoins convertToNodeSpace:[self convertToWorldSpace:letterBlock.letterSprite.position]];
        NSLog(@"letter point label position: %@", NSStringFromCGPoint(labelLetterPoints.position));
        labelLetterPoints.position = ccp(labelLetterPoints.position.x, labelLetterPoints.position.y + letterBlock.letterSprite.tileSize.height);
        labelLetterPoints.opacity = 0;
        [gameplayButtonsLayer.spriteButtonCoins addChild:labelLetterPoints z:0];
        [labelLetterPoints runAction:[CCFadeIn actionWithDuration:0.10]];
        [labelLetterPoints runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.55] two:[CCFadeOut actionWithDuration:0.25]]];
        [labelLetterPoints runAction:[CCSequence actions:
                                      [CCEaseExponentialInOut actionWithAction:
                                       [CCMoveTo actionWithDuration:1
                                                           position:gameplayButtonsLayer.spriteCoinsCounter.position]],
                                      [CCCallBlock actionWithBlock:^{
                                          [labelLetterPoints removeFromParentAndCleanup:YES];
                                      }],
                                      nil]];

        pointsEarned += pointValueString.integerValue;
    }
    
    floatingLettersCenter = ccpMidpoint(floatingLettersBoundingBox.topLeftCorner, floatingLettersBoundingBox.bottomRightCorner);
    NSLog(@"floating letters center: %@", NSStringFromCGPoint(floatingLettersCenter));
    floatingLettersCenter = [gameplayButtonsLayer.spriteButtonCoins convertToNodeSpace:[self convertToWorldSpace:floatingLettersCenter]];
    
    if (*errorMessage)
    {
        __block CCLabelBMFont *labelErrorString = [CCLabelBMFont labelWithString:*errorMessage fntFile:@"iPad/Fonts/GroboldRed.fnt" width:450 alignment:kCCTextAlignmentCenter];
        labelErrorString.anchorPoint = ccp(1, 0.5);
        labelErrorString.position = ccpAdd(floatingLettersCenter, ccp(labelErrorString.boundingBox.size.width / 2, -50));
        labelErrorString.opacity = 0;
        [gameplayButtonsLayer.spriteButtonCoins addChild:labelErrorString z:0];
        
        [labelErrorString runAction:
         [CCSequence actions:
          [CCFadeIn actionWithDuration:0.25],
          [CCDelayTime actionWithDuration:1.0],
          [CCFadeOut actionWithDuration:0.25],
          [CCDelayTime actionWithDuration:5],
          [CCCallBlock actionWithBlock:^{
             [labelErrorString removeFromParentAndCleanup:YES];
         }],
          nil]];
        
        [labelErrorString runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
          [CCDelayTime actionWithDuration:0.5],
          [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
          nil]];
        
        *errorMessage = nil;
        return nil;
    }
    
    NSLog(@"points earned: %d", pointsEarned);

    [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.9] two:[CCCallBlock actionWithBlock:^{
        gameplayButtonsLayer.coinCount += pointsEarned;
    }]]];
    
    // compute bonus points and do animations - advanced
    NSUInteger i = 0;
    for (NSString *word in wordsCreated)
    {
        __block NSUInteger points = word.pointValue;
        __block NSUInteger multiplier = 0;
        switch (word.length)
        {
            case 9: multiplier = 10; break;
            case 10: multiplier = 15; break;
            case 11: multiplier = 20; break;
            case 12: multiplier = 25; break;
            case 13: multiplier = 50; break;
            case 14: multiplier = 100; break;
            case 15: multiplier = 200; break;
            default: if (word.length > 3 && word.length < 9)
                multiplier = word.length - 3;
        }
        
        if (multiplier)
        {
            NSTimeInterval waitTime = 1;
            
            NSString *bonusString = [NSString stringWithFormat:@"%d LETTER WORD BONUS:", word.length];
            NSString *bonusScore = [NSString stringWithFormat:@"%d x%d", points, multiplier];
            __block CCLabelBMFont *labelBonusString = [CCLabelBMFont labelWithString:bonusString fntFile:@"iPad/Fonts/Grobold.fnt" width:450 alignment:kCCTextAlignmentRight];
            labelBonusString.anchorPoint = ccp(1, 0.5);
            __block CCLabelBMFont *labelBonusScore = [CCLabelBMFont labelWithString:bonusScore fntFile:@"iPad/Fonts/GroboldGreenNumeric.fnt" width:150 alignment:kCCTextAlignmentLeft];
            labelBonusScore.anchorPoint = ccp(0, 0.5);
            labelBonusString.position = ccpAdd(floatingLettersCenter, ccp(labelBonusString.boundingBox.size.width / 2 - 50, -50));
            labelBonusScore.position = ccpAdd(floatingLettersCenter, ccp(labelBonusString.boundingBox.size.width / 2 - 30, -50));
            labelBonusString.opacity = 0;
            labelBonusScore.opacity = 0;
            [gameplayButtonsLayer.spriteButtonCoins addChild:labelBonusString z:0];
            [gameplayButtonsLayer.spriteButtonCoins addChild:labelBonusScore z:0];
            
            [labelBonusString runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:waitTime * (i + 1)],
              [CCFadeIn actionWithDuration:0.25],
              [CCDelayTime actionWithDuration:1.0],
              [CCFadeOut actionWithDuration:0.25],
              [CCDelayTime actionWithDuration:5],
              [CCCallBlock actionWithBlock:^{
                 [labelBonusString removeFromParentAndCleanup:YES];
             }],
              nil]];
            
            [labelBonusString runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:waitTime * (i + 1)],
              [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
              [CCDelayTime actionWithDuration:0.5],
              [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
              nil]];
            
            [labelBonusScore runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:waitTime * (i + 1)],
              [CCFadeIn actionWithDuration:0.25],
              [CCDelayTime actionWithDuration:1.75],
              [CCFadeOut actionWithDuration:0.25],
              [CCDelayTime actionWithDuration:5],
              [CCCallBlock actionWithBlock:^ {
                 [labelBonusScore removeFromParentAndCleanup:YES];
             }],
              nil]];
            
            [labelBonusScore runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:waitTime * (i + 1)],
              [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
              [CCDelayTime actionWithDuration:0.50],
              [CCCallBlock actionWithBlock:^{
                for (NSUInteger i = 0; i <= 30; i++) {
                    __block NSUInteger score = points + i * points * (multiplier - 1) / 30;
                    [labelBonusScore runAction:
                     [CCSequence actionOne:[CCDelayTime actionWithDuration:i * 0.75 / 30]
                                       two:[CCCallBlock actionWithBlock:^{
                        labelBonusScore.string = [NSString stringWithFormat:@"%d", score];
                    }]]];
                }
             }],
              [CCMoveBy actionWithDuration:0.5 position:ccp(0, 50)],
              [CCEaseExponentialInOut actionWithAction:
               [CCMoveTo actionWithDuration:1 position:gameplayButtonsLayer.spriteCoinsCounter.position]],
              nil]];
            
            [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:waitTime * (++ i + 2)] two:[CCCallBlock actionWithBlock:^{
                gameplayButtonsLayer.coinCount += points * multiplier;
            }]]];
        }
    }
    
    // finally freeze the floating blocks
    for (NSValue* key in floatingLetterBlocks)
    {
        SPLLetterBlock *letterBlock = _letterBlocks[key];
        letterBlock.isFrozen = YES;
        letterBlock.isConnectedToACliffBlock = FLYes;
        
        [letterBlock.letterSprite runAction:[CCEaseBackOut actionWithAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]]];
    }
    
    // check if the new path connects two different cliff nodes
    // if so, dispatch a LandsBridged event
    NSMutableArray *scannedPositions = [NSMutableArray array];
    NSMutableArray *cliffsConnected = [NSMutableArray array];
    
    CGPoint letterBlockPosition = ((NSValue *)floatingLetterBlocks.keyEnumerator.nextObject).CGPointValue;
    [scannedPositions addObject:[NSValue valueWithCGPoint:letterBlockPosition]];
    
    __block void (^scanAdjPositions) (CGPoint) = ^(CGPoint position) {
        CGPoint adj[4] = {ccp(0, -1), ccp(0, 1), ccp(-1, 0), ccp(1, 0)};
        for (NSUInteger i = 0; i < 4; i++) {
            CGPoint adjPosition = ccpAdd(position, adj[i]);
            
            SPLTileType tileType = [self tileTypeAt:adjPosition];
            if (!(tileType == SPLTileTypeLetterFrozen || tileType == SPLTileTypeCliff)) continue;
            
            if ([scannedPositions containsObject:[NSValue valueWithCGPoint:adjPosition]]) continue;
            
            NSString *tileDescription = [self tileDescriptionAt:adjPosition].name;
            NSLog(@"tile type at %@: %@ (%@)", NSStringFromCGPoint(adjPosition), SPLTileTypeToNSString(tileType), tileDescription);
            if ([tileDescription rangeOfString:@"Cliff"].location != NSNotFound) {
                tileDescription = [tileDescription stringByReplacingOccurrencesOfString:@"Cliff" withString:@""];
                if (![cliffsConnected containsObject:tileDescription]) {
                    [cliffsConnected addObject:[tileDescription stringByReplacingOccurrencesOfString:@"Cliff" withString:@""]];
                }
            }
            [scannedPositions addObject:[NSValue valueWithCGPoint:adjPosition]];
            scanAdjPositions(adjPosition);
        }
    };

    scanAdjPositions(letterBlockPosition);
    [scanAdjPositions release];
    
    if (!landsBridged) landsBridged = [[NSMutableArray alloc] init];
    if (cliffsConnected.count >= 2 && ![landsBridged containsObject:cliffsConnected]) {
        [[(SPLCoreGameplayLayer *) self.parent.parent Event_LandsBridged] dispatchWithData:[NSArray arrayWithArray:cliffsConnected]];
        [landsBridged addObject:[NSArray arrayWithArray:cliffsConnected]];
        NSLog(@"Lands Bridged: %@", cliffsConnected);
    }
    
    NSLog(@"Cool! Freeze was successful! Good job!");
    
    return [NSArray arrayWithArray:wordsCreated];
}

-(void)highlightTileUnderPoint:(CGPoint)point
{
    [[self layerNamed:@"ocean waves"] tileAt:point].opacity = 50;
}

-(BOOL)isPointAnObstruction:(CGPoint)point
{
    
    SPLTileType tileTypeAtPoint = [self tileTypeAt:point];
    if (tileTypeAtPoint == SPLTileTypeLand ||
        tileTypeAtPoint == SPLTileTypeLetterFrozen)
        return NO;
    else if (tileTypeAtPoint == SPLTileTypeObstruction ||
             tileTypeAtPoint == SPLTileTypeWater ||
             tileTypeAtPoint == SPLTileTypeLetterFloating ||
             tileTypeAtPoint == SPLTileTypeInteractiveObject)
        return YES;
    
    // if current type is cliff, and either of surrounded blocks is letter
    // its not an obstruction
    
    CGPoint neighbours[4] = {
        ccp(point.x + 1, point.y),
        ccp(point.x, point.y + 1),
        ccp(point.x - 1, point.y),
        ccp(point.x, point.y - 1),
    };
    
    if ([self tileTypeAt:point] == SPLTileTypeCliff)
        for (int i = 0; i < 4; i++)
            if ([self tileTypeAt:neighbours[i]] == SPLTileTypeLetterFrozen)
                return NO;
    
    return YES;
}


-(NSSet *)validNeighbouringNodesForPoint:(CGPoint)point
{
    NSMutableSet *returnSet = [NSMutableSet set];
    
    CGPoint neighbours[4] = {
        CGPointMake(point.x + 1, point.y),
        CGPointMake(point.x - 1, point.y),
        CGPointMake(point.x, point.y + 1),
        CGPointMake(point.x, point.y - 1)
    };
    
    SPLTileType tileTypeAtPoint = [self tileTypeAt:point];
    
    for (int i = 0; i < 4; i++)
        //if (![self isPointAnObstruction:neighbours[i]])
        {
            if (tileTypeAtPoint == SPLTileTypeCliff && 
                [self tileTypeAt:neighbours[i]] == SPLTileTypeCliff)
                continue;
            [returnSet addObject:[NSValue valueWithCGPoint:neighbours[i]]];
        }
    
    return returnSet;
}

-(NSInteger)costOfNodeAtPoint:(CGPoint)point
{
    if ([self tileTypeAt:point] == SPLTileTypeWater ||
        [self tileTypeAt:point] == SPLTileTypeLetterFloating)
        return 10;
    return [self isPointAnObstruction:point] ? 20 : 1;
}

-(void)__buildConnectorPath:(NSArray*)path
{
    [self removeConnectors];
    
    BOOL isPreviousDirectionEndValid = YES;
    
    for (int i = 0, l = path.count; i < l; i++)
    {
        CGPoint currentPoint = [path[i] CGPointValue],
        previousPoint, nextPoint;
        SPLConnectorBlockDirection directionStart, directionEnd;
        
        BOOL isDirectionStartValid = NO, isDirectionEndValid = NO;
        
        if (i)
        {
            previousPoint = [path[i - 1] CGPointValue];
            if (currentPoint.x > previousPoint.x)
                directionStart = SPLConnectorBlockDirectionRight; // came from left
            else if (currentPoint.x < previousPoint.x)
                directionStart = SPLConnectorBlockDirectionLeft; // came from right
            else if (currentPoint.y > previousPoint.y)
                directionStart = SPLConnectorBlockDirectionTop;
            else
                directionStart = SPLConnectorBlockDirectionBottom;
        }
        else
        {
            directionStart = SPLConnectorBlockDirectionNone;
        }
        
        isDirectionStartValid = isPreviousDirectionEndValid && ![self isPointAnObstruction:currentPoint];
        
        if (isDirectionStartValid || (![self isPointAnObstruction:previousPoint] && [self isPointAnObstruction:currentPoint])) {
            [connectorPath addObject:[NSValue valueWithCGPoint:currentPoint]];
        }
        
        if (isDirectionStartValid && i == l - 1) {
            [connectorPath addObject:[NSValue valueWithCGPoint:ccpAdd(currentPoint, ccpSub(currentPoint, previousPoint))]];
        }

        if (l - i - 1)
        {
            nextPoint = [path[i + 1] CGPointValue];
            if (currentPoint.x > nextPoint.x)
                directionEnd = SPLConnectorBlockDirectionRight; // going right
            else if (currentPoint.x < nextPoint.x)
                directionEnd = SPLConnectorBlockDirectionLeft; // going left
            else if (currentPoint.y > nextPoint.y)
                directionEnd = SPLConnectorBlockDirectionTop;
            else
                directionEnd = SPLConnectorBlockDirectionBottom;
            
            isDirectionEndValid = isPreviousDirectionEndValid && ![self isPointAnObstruction:nextPoint];
            isPreviousDirectionEndValid = isDirectionEndValid;
        }
        else directionEnd = SPLConnectorBlockDirectionNone;
        
        
        SPLConnectorBlockInfo info = SPLConnectorBlockSpriteInfoMake(directionStart, directionEnd, isDirectionStartValid, isDirectionEndValid);
        SPLConnectorBlockSprite *connectorSprite = [SPLConnectorBlockSprite connectorBlockSpriteWithInfo:info];
        
        connectorSprite.position = [[self layerNamed:@"ocean waves"] tileAt:currentPoint].position;
        
        [_connectorLayer addChild:connectorSprite];
    }
    
    // value may change while the selector is in process
    if (!((SPLCoreGameplayLayer*) self.parent.parent).elricTouchObject)
        [self removeConnectors];
}

-(void)__findConnectorPath
{
    @autoreleasepool
    {
        NSArray *path;

        if (((SPLCoreGameplayLayer*) self.parent.parent).elricTouchObject)
        {
            CGPoint sourcePoint = currentlyConnectedPoints[0];
            CGPoint destinationPoint = currentlyConnectedPoints[1];
            
            if (!connectorPathCache)
            {
                connectorPathCache = [[NSMutableDictionary alloc] init];
            }
            
            FLAStarPathfinder *pathfinder = [FLAStarPathfinder pathfinderWithPathfinderDelegate:self];
            NSArray *cachedPath = connectorPathCache[[NSStringFromCGPoint(sourcePoint) stringByAppendingString:NSStringFromCGPoint(destinationPoint)]];
            if (cachedPath)
            {
                path = cachedPath;
            }
            else
            {
                path = [pathfinder findPathFromSource:sourcePoint toDestination:destinationPoint];
                connectorPathCache[[NSStringFromCGPoint(sourcePoint) stringByAppendingString:NSStringFromCGPoint(destinationPoint)]] = path;
            }
        }

        // value may change while the selector is in process
        if (((SPLCoreGameplayLayer*) self.parent.parent).elricTouchObject)
            [self performSelectorOnMainThread:@selector(__buildConnectorPath:) withObject:path waitUntilDone:NO];

        __selectorInProgress = NO;
    }
}

-(void)createConnectorFromSource:(CGPoint)sourcePoint toDestination:(CGPoint)destinationPoint
{
    if (__selectorInProgress)
        return;
    
    if (currentlyConnectedPoints[0].x == sourcePoint.x &&
        currentlyConnectedPoints[0].y == sourcePoint.y &&
        currentlyConnectedPoints[1].x == destinationPoint.x &&
        currentlyConnectedPoints[1].y == destinationPoint.y)
    {
        // connector already created
        return;
    }
    
    if (!_connectorLayer)
    {
        _connectorLayer = [CCSprite node];
        [self addChild:_connectorLayer z:999];
    }
    
    if (!connectorPathCache)
    {
        connectorPathCache = [[NSMutableDictionary alloc] init];
    }
    
    currentlyConnectedPoints[0] = sourcePoint;
    currentlyConnectedPoints[1] = destinationPoint;
    
    
    if (sourcePoint.x == destinationPoint.x &&
        sourcePoint.y == destinationPoint.y) // whoa there, you're already at your destination!
    {
        [self removeConnectors];
        
        SPLConnectorBlockInfo info = SPLConnectorBlockSpriteInfoMake(SPLConnectorBlockDirectionNone, SPLConnectorBlockDirectionNone, YES, NO);
        SPLConnectorBlockSprite *connectorSprite = [SPLConnectorBlockSprite connectorBlockSpriteWithInfo:info];
        connectorSprite.position = [[self layerNamed:@"ocean waves"] tileAt:sourcePoint].position;
        
        [_connectorLayer addChild:connectorSprite];
        
        return;
    }
    
    __selectorInProgress = YES;
    [self performSelectorInBackground:@selector(__findConnectorPath) withObject:nil];
}

-(void)removeConnectors
{
    if (!connectorPath)
        connectorPath = [[NSMutableArray array] retain];
    else
        [connectorPath removeAllObjects];
    
    [connectorPathCache release];
    connectorPathCache = nil;
    
    [_connectorLayer removeAllChildrenWithCleanup:YES];
    
    currentlyConnectedPoints[0] = ccp(-1, -1);
    currentlyConnectedPoints[1] = ccp(-1, -1);
}

-(CGRect)boundingBoxOfCharacterNamed:(NSString *)characterName
{
    NSDictionary *object = [[self objectGroupNamed:@"Characters"] objectNamed:characterName];
    float x = ((NSString *) object[@"x"]).floatValue;
    float y = ((NSString *) object[@"y"]).floatValue;
    float w = ((NSString *) object[@"width"]).floatValue;
    float h = ((NSString *) object[@"height"]).floatValue;
    NSLog(@"rect: %@", NSStringFromCGRect(CGRectMake(x, y, w, h)));
    return CGRectMake(x, y, w, h);
}

-(CGRect)boundingBoxOfInteractiveObjectNamed:(NSString *)interactiveObjectName
{
    NSDictionary *object = [[self objectGroupNamed:@"InteractiveObjects"] objectNamed:interactiveObjectName];
    float x = ((NSString *) object[@"x"]).floatValue;
    float y = ((NSString *) object[@"y"]).floatValue;
    float w = ((NSString *) object[@"width"]).floatValue;
    float h = ((NSString *) object[@"height"]).floatValue;
    NSLog(@"rect: %@", NSStringFromCGRect(CGRectMake(x, y, w, h)));
    return CGRectMake(x, y, w, h);
}

-(NSDictionary *)interactiveObjectUnderPoint:(CGPoint)point
{
    for (id object in [self objectGroupNamed:@"InteractiveObjects"].objects)
    {
        float x = ((NSString *) object[@"x"]).floatValue;
        float y = ((NSString *) object[@"y"]).floatValue;
        float w = ((NSString *) object[@"width"]).floatValue;
        float h = ((NSString *) object[@"height"]).floatValue;
        
        SPLInteractiveItem *interactiveItem;
        
        // todo: optimize this - change O(n) lookup to O(1) lookup by changing _interactiveObjects to a dictionary.
        for (interactiveItem in _interactiveObjects)
            if ([interactiveItem.name isEqualToString:object[@"name"]]) break;
        
        if (CGRectContainsPoint(CGRectMake(x, y, w, h), [self convertToNodeSpace:point]) && interactiveItem.visible)
            return object;
    }
    
    return nil;
}

-(SPLGameCharacter *)characterUnderPoint:(CGPoint)point
{
    for (SPLGameCharacter *character in _characters)
    {
        if ([FL doesSprite:character containPoint:point])
            return character;
    }
    return nil;
}

-(CGPoint)convertPositionToTilePosition:(CGPoint)position
{
    return ccp(floorf(position.x / self.tileSize.width), floorf(self.mapSize.height - 1 - position.y / self.tileSize.height));
}

-(CGPoint)convertTilePositionToPosition:(CGPoint)tilePosition
{
    return ccp(tilePosition.x * self.tileSize.width, (self.mapSize.height - 1 - tilePosition.y) * self.tileSize.height);
}

-(CCSprite *)charactersLayer
{
    if (!charactersLayer) {
        charactersLayer = [CCSprite node];
        [self addChild:charactersLayer z:[self layerNamed:@"Character"].zOrder];
    }
    return charactersLayer;
}

- (void)dealloc
{
    [_mapName release];
    [landsBridged release];
    [_letterBlocks release];
    [tileTypesCache release];
    [connectorPath release];
    
    [_characters release];
    [_interactiveObjects release];
    if (connectorPathCache)
    {
        [connectorPathCache release];
    }
    
    [super dealloc];
}

@end
