//
//  FLActionSequence.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FLActionMake(action)                    \
    [FLAction actionWithActionDelegate:self     \
    andName:[@#action stringByReplacingOccurrencesOfString:@"Action_" withString:@""]]

@class FLEvent;
@class FLAction;

@protocol FLActionDelegate <NSObject>

-(void)handleAction:(FLAction *)action;

@end

@interface FLProxyActionDelegate : NSObject <FLActionDelegate>
{
    NSString *actionDelegateName;
}

+(id)actionDelegateWithName:(NSString *)name;
-(id)initWithName:(NSString *)name;

@end

@interface FLAction : NSObject {
    FLEvent* Event_ActionDidFinish;
    NSUInteger runCountThreshold;
    NSMutableArray *delegatesForwardedTo;
}

-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate;
-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate
                    andName:(NSString*)name;

+(id)actionWithActionDelegate:(id<FLActionDelegate>)actionDelegate;
+(id)actionWithActionDelegate:(id<FLActionDelegate>)actionDelegate
                      andName:(NSString*)name;

-(void)run;
-(void)runWithData:(id)data;
-(void)forwardToActionDelegate:(id<FLActionDelegate>)actionDelegate;
-(void)forwardToActionDelegates: (id<FLActionDelegate>) actionDelegate, ...;
-(void)finish;
-(void)willFinishAfter:(NSTimeInterval)seconds;
-(void)willFinishAfterRunCountReaches:(NSUInteger)count;
-(BOOL)is:(FLAction*)action;

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) id<FLActionDelegate> actionDelegate;
@property (nonatomic, retain) FLAction* subsequentAction;
@property (nonatomic, retain) FLAction* priorAction;
@property (nonatomic, retain) id data;

@property (nonatomic, copy) void (^onRun) (void);
@property (nonatomic, copy) void (^onFinish) (void);

@property (nonatomic, readonly) FLAction* lastSubsequentAction;
@property (nonatomic) NSUInteger runCount; // number of times this action has been run
@property (nonatomic, readonly) BOOL hasStarted;
@property (nonatomic, readonly) BOOL hasFinished;

@end

@interface FLActionSequence : FLAction

-(id)initWithActionDelegate:(id<FLActionDelegate>)actionDelegate
             andFirstAction:(FLAction *)firstAction
                    andName:(NSString *)name;

+(id)actionSequenceWithActionDelegate:(id<FLActionDelegate>)actionDelegate
                       andFirstAction:(FLAction *)firstAction
                              andName:(NSString *)name;

@property (nonatomic, retain) FLAction *firstAction;
@property (nonatomic, readonly) FLAction *lastAction;

@end

@interface FLActionSequenceManager : NSObject
{@public
    NSMutableDictionary* actionSequences;
}

-(FLActionSequence *)actionSequenceWithName:(NSString *)name;
-(void)addActionSequence:(FLActionSequence *)actionSequence;

@end

@interface FLActionDelegateManager : NSObject
{
    NSMutableDictionary *actionDelegates;
}

-(id<FLActionDelegate>)actionDelegateWithName:(NSString *)name;
-(void)registerActionDelegate:(id<FLActionDelegate>)actionDelegate withName:(NSString *)name;
-(void)unregisterActionDelegate:(id<FLActionDelegate>)actionDelegate;
-(void)unregisterActionDelegateWithName:(NSString *)name;

@end