//
//  AppDelegate.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/18/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "SPLLogoScene.h"

@implementation AppDelegate

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if your Application only supports landscape mode
	//

//	CC_ENABLE_DEFAULT_GL_STATES();
//	CCDirector *director = [CCDirector sharedDirector];
//	CGSize size = [director winSize];
//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
//	sprite.position = ccp(size.width/2, size.height/2);
//	sprite.rotation = -90;
//	[sprite visit];
//	[[director openGLView] swapBuffers];
//	CC_ENABLE_DEFAULT_GL_STATES();
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Main Window
	_window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Director
	_director = (CCDirectorIOS*)[CCDirector sharedDirector];
    [_director setDisplayStats:YES];
	[_director setAnimationInterval:1.0/60];
    
    // GL View
	CCGLView *__glView = [CCGLView viewWithFrame:[_window bounds]
									 pixelFormat:kEAGLColorFormatRGB565
									 depthFormat:0 /* GL_DEPTH_COMPONENT24_OES */
							  preserveBackbuffer:NO
									  sharegroup:nil
								   multiSampling:NO
								 numberOfSamples:0
						  ];
	
	[_director setView:__glView];
	[_director setDelegate:self];
    [_director setProjection:kCCDirectorProjection2D];
	_director.wantsFullScreenLayout = YES;
    
	// Retina Display ?
	[_director enableRetinaDisplay:useRetinaDisplay_];
    
    _director.view.multipleTouchEnabled = YES;
	
	// Navigation Controller
	_navController = [[UINavigationController alloc] initWithRootViewController:_director];
	_navController.navigationBarHidden = YES;
    
	// AddSubView doesn't work on iOS6
	// [_window addSubview:_navController.view];
    [_window setRootViewController:_navController];
    
	[_window makeKeyAndVisible];
    
    // Default texture format for PNG/BMP/TIFF/JPEG/GIF images
    // It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
    // You can change anytime.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

    // PVR Textures have alpha premultiplied
    [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];

    // Spellbound - begin
    [Spellbound begin];
    
	// Run the intro Scene
	[[CCDirector sharedDirector] runWithScene: [SPLLogoScene scene]];
    
    return YES;
    
}


// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application
{
	if( [_navController visibleViewController] == _director )
		[_director pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	if( [_navController visibleViewController] == _director )
		[_director resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	if( [_navController visibleViewController] == _director )
		[_director stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	if( [_navController visibleViewController] == _director )
		[_director startAnimation];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application
{
	CC_DIRECTOR_END();
}

// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[_director purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[_director setNextDeltaTimeZero:YES];
}

- (void) dealloc
{
	[_window release];
	[_navController release];
    
	[super dealloc];
}

@end
