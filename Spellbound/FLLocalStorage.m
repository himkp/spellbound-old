//
//  FLLocalStorage.m
//  Spellbound
//
//  Created by Fleon Games on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FLLocalStorage.h"

@implementation FLLocalStorage

- (id)init
{
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
        NSString *documentsDirectoryPath = paths[0];
        
        fmdb = [[FMDatabase alloc] initWithPath:[documentsDirectoryPath stringByAppendingPathComponent:@"LocalStorage.sqlite"]];
        
        [fmdb open];
        [fmdb executeUpdate:@"CREATE TABLE IF NOT EXISTS kvstore (_key VARCHAR(32) PRIMARY KEY, _data BLOB);"];
    }
    return self;
}

-(void)dealloc
{
    [fmdb close];
    [fmdb release];
    
    [super dealloc];
}

-(id)dataForKey:(NSString *)key
{
    FMResultSet *result = [fmdb executeQuery:@"SELECT _data FROM kvstore WHERE _key = ?", key];
    if ([result next]) {
        if ([result stringForColumnIndex:0]) {
            return [result stringForColumnIndex:0];
        } else if ([result dataForColumnIndex:0]) {
            return [result dataForColumnIndex:0];
        }
        return [result stringForColumnIndex:0];
    }
    return nil;
}

-(BOOL)dataExistsForKey:(NSString *)key
{
    FMResultSet *result = [fmdb executeQuery:@"SELECT COUNT(_data) FROM kvstore WHERE _key = ?", key];
    if ([result next])
        return (BOOL) [result intForColumnIndex:0];
    return NO;
}

-(void)setData:(id)data forKey:(NSString *)key
{
    if (![fmdb executeUpdate:@"INSERT INTO kvstore (_key, _data) VALUES (?, ?)", key, data])
        [fmdb executeUpdate:@"UPDATE kvstore SET _data = ? WHERE _key = ?", data, key];
}

-(void)unsetDataForKey:(NSString *)key
{
    [fmdb executeUpdate:@"DELETE FROM kvstore WHERE _key = ?", key];
}

@end
