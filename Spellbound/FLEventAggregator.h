//
//  FLEventAggregator.h
//  Spellbound
//
//  Created by Fleon Games on 5/6/13.
//
//

#import "Spellbound.h"
#import "FLEvent.h"

@interface FLProxyEvent : FLEvent

-(id)initWithName:(NSString *)name andDispatcherName:(NSString *)eventDispatcherName;
+(id)eventWithName:(NSString *)name andDispatcherName:(NSString *)eventDispatcherName;

@property (nonatomic, retain) NSString *eventDispatcherName;

@end

@interface FLEventDispatcherManager : NSObject
{
    NSMutableDictionary *eventDispatchers;
}

-(id)eventDispatcherWithName:(NSString *)name;
-(void)registerEventDispatcher:(id)eventDispatcher withName:(NSString *)name;
-(void)unregisterEventDispatcher:(id)eventDispatcher;
-(void)unregisterEventDispatcherWithName:(NSString *)name;

@end

@interface FLEventAggregator : NSObject
{
    NSMutableDictionary *events;
}

+(FLEventAggregator *)globalEventAggregator;

-(FLEvent *)eventWithName:(NSString *)eventName;
-(FLEvent *)eventWithParameterizedName:(NSString *)eventName eventData:(id *)eventData;

-(void)addEvent:(FLEvent *)event;

@end