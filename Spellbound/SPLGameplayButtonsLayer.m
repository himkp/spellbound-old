//
//  SPLGameplayButtonsLayer.m
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGameplayButtonsLayer.h"
#import "SPLGameScene.h"
#import "FLEvent.h"

@implementation SPLGameplayButtonsLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfButtonMenu        = fl_ccp(  38,   30,  397,  148);
        positionOfButtonCoins       = fl_ccp( 756,   30,  538,  148);
        positionOfButtonItems       = fl_ccp(1602,   30,  398,  148);
        
        positionOfTheButtons        = fl_ccp(   0,    0,    0,    0);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfButtonMenu        = fl_ccp(  16,   18,  189,   71);
        positionOfButtonCoins       = fl_ccp( 354,   19,  253,   70);
        positionOfButtonItems       = fl_ccp( 750,   18,  187,   71);
        
        positionOfTheButtons        = fl_ccp(   0,    0,    0,    0);
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initializePositions];
        
        self.isTouchEnabled = YES;
        self.visible = NO;
        
        _coinCount = 0;
        
        spriteTheButtons = [CCSprite node];
        spriteTheButtons.position = positionOfTheButtons;
        spriteTheButtons.contentSize = [CCDirector sharedDirector].winSize;
        spriteTheButtons.anchorPoint = ccp(0, 1);
        
        _spriteButtonMenu = [CCSprite spriteWithSpriteFrameName:@"GameplayScreen_Button_Menu.png"];
        _spriteButtonMenu.position = positionOfButtonMenu;
        _spriteButtonCoins = [CCSprite spriteWithSpriteFrameName:@"GameplayScreen_Button_Coins.png"];
        _spriteButtonCoins.position = positionOfButtonCoins;
        _spriteButtonItems = [CCSprite spriteWithSpriteFrameName:@"GameplayScreen_Button_Items.png"];
        _spriteButtonItems.position = positionOfButtonItems;
        
        _spriteCoinsCounter = [CCLabelBMFont labelWithString:@"0"
                                                    fntFile:@"iPad/Fonts/Grobold.fnt"
                                                      width:300
                                                  alignment:kCCTextAlignmentCenter];

        [_spriteButtonCoins addChild:_spriteCoinsCounter z:999];
        _spriteCoinsCounter.position = ccp(170, 30);
        
        [spriteTheButtons addChild:_spriteButtonMenu];
        [spriteTheButtons addChild:_spriteButtonCoins];
        [spriteTheButtons addChild:_spriteButtonItems];
        
        [self addChild:spriteTheButtons];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"GameplayButtons"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"GameplayButtons"];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [self init])
    {
        if ([decoder decodeBoolForKey:@"visible"]) [self show];
        self.coinCount = [decoder decodeIntegerForKey:@"coinCount"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeBool:self.visible forKey:@"visible"];
    [encoder encodeInteger:self.coinCount forKey:@"coinCount"];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_HideGameplayButtons])
    {
        [self hide:^{
            [action finish];
        }];
    }
    else if ([action is:Action_ShowGameplayButtons])
    {
        [self show:^{
            [action finish];
        }];
    }
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(void)setCoinCount:(NSUInteger)coinCount
{
    NSTimeInterval transitionTime = 1.0;
    NSUInteger fps = 60;
    for (int i = 0; i < transitionTime * fps; i++) {
        NSTimeInterval time = (float) i / fps;
        NSUInteger coinCountDelta = _coinCount + ((float) i / transitionTime / fps) * (coinCount - _coinCount);
        NSString *coinCountString = [NSString stringWithFormat:@"%d", coinCountDelta];
        [_spriteCoinsCounter performSelector:@selector(setString:) withObject:coinCountString afterDelay:time];
    }
    [_spriteCoinsCounter performSelector:@selector(setString:)
                             withObject:[NSString stringWithFormat:@"%d", coinCount]
                             afterDelay:transitionTime + 0.05];
    
    FLAction *setAction = Action_Set;
    setAction.actionDelegate = [Spellbound globalActionDelegate];
    setAction.data = [NSString stringWithFormat:@"CoinCount = %d", coinCount];
    [setAction run];
    
    _coinCount = coinCount;
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if (!self.visible) return NO;
    
    if ([FL doesSprite:_spriteButtonMenu containPoint:point])
    {
        // also pause the game
        
        [FL with:_spriteButtonMenu runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [[(SPLGameScene*) self.parent inGameMenuLayer] show];
        return YES;
    }
    else if ([FL doesSprite:_spriteButtonItems containPoint:point])
    {
        [FL with:_spriteButtonItems runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [((SPLGameScene*) self.parent).itemsPanelLayer show];
        return YES;
    }
    else if ([FL doesSprite:_spriteButtonCoins containPoint:point])
    {
        [FL with:_spriteButtonCoins runAnimationAction:kFLAnimationActionShrinkAndGrow];
        [((SPLGameScene*) self.parent).coinsPanelLayer show];
        return YES;
    }
    return NO;
}

-(void)show
{
    [self show: ^{}];
}

-(void)show:(void (^)())onShow
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    self.visible = YES;
    spriteTheButtons.position = ccp(0, _spriteButtonCoins.boundingBox.size.height + winSize.height * 2 - _spriteButtonCoins.position.y);
    
    [spriteTheButtons runAction:
     [CCSequence actionOne:
      [CCEaseExponentialOut actionWithAction:
       [CCMoveTo actionWithDuration:0.5 position:ccp(0, winSize.height)]]
                       two:[CCCallBlock actionWithBlock:onShow]]];
}

-(void)hide
{
    [self hide: ^{}];
}

-(void)hide:(void (^)())onHide
{
    [spriteTheButtons runAction:
     [CCEaseExponentialIn actionWithAction:
      [CCMoveTo actionWithDuration:0.5
                          position:ccp(0, _spriteButtonCoins.boundingBox.size.height + [CCDirector sharedDirector].winSize.height * 2 - _spriteButtonCoins.position.y)]]];
    
    [self runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.52],
      [CCHide action],
      [CCCallBlock actionWithBlock:onHide],
      nil]];
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    [super dealloc];
}

@end
