//
//  Spellbound.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// THE GAMEPLAY IS FAR MORE IMPORTANT
// YOU'RE DEVELOPING A GAME, NOT A LIBRARY/MIDDLEWARE

#ifndef Spellbound_Spellbound_h
#define Spellbound_Spellbound_h

// general formula to convert position from Photoshop coordinates to cocos2d coordinates:
// x = x / 2 - width / 4
// y = screenHeight - y / 2 - height / 4

CGPoint fl_ccp(float x, float y, float width, float height);

// Common enumerators

typedef enum {
    SPLTileTypeLand = 1,
    SPLTileTypeWater,
    SPLTileTypeObstruction,
    SPLTileTypeLetterFloating,
    SPLTileTypeLetterFrozen,
    SPLTileTypeCliff,
    SPLTileTypeCharacter,
    SPLTileTypeInteractiveObject
} SPLTileType;

typedef enum {
    kEnglishInternationalWordList = 0,
    kSpanishWordList,
    kFrenchWordList,
    kItalianWordList
} SPLLocalizedWordList;

// Foundation
#import <Foundation/Foundation.h>

// Common functions
NSString* SPLTileTypeToNSString (SPLTileType tileType);

// FMDB
#import "SPLWordList.h"
#import "FLLocalStorage.h"
#import "SPLDialogueManager.h"
#import "SPLCheckPointManager.h"
#import "SPLGlobalActionDelegate.h"
#import "FLSimpleAudioPlayer.h"
#import "FLEventAggregator.h"

// Utils
#import "NSString+Spellbound.h"

// Cocos2D
#import "cocos2d.h"

// Fleon
#import "FL.h"

@class SPLWordList;
@class FLEventDispatcherManager;

// Static Functions
NSString* SPLTileTypeToNSString (SPLTileType tileType);

@interface Spellbound : NSObject

+(void)begin;
+(void)end;

+(id<FLActionDelegate>)globalActionDelegate;
+(SPLWordList*) wordList;
+(SPLDialogueManager*) dialogueManager;
+(SPLMessageManager *)messageManager;
+(SPLCheckPointManager *)checkPointManager;
+(FLLocalStorage*) localStorage;
+(FLActionSequenceManager *)actionSequenceManager;
+(FLActionDelegateManager *)actionDelegateManager;
+(FLEventDispatcherManager *)eventDispatcherManager;
+(NSMutableArray *)variableList;
+(FLSimpleAudioPlayer *)audioPlayer;

@end

#endif
