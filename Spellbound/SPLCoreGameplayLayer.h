//
//  SPLCoreGameplayLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLTiledMap.h"

@class SPLGameCharacter;

@interface SPLCoreGameplayLayer : CCLayer <FLActionDelegate, NSCoding>
{
    CCSprite *spriteBackground;

    NSMutableArray *spriteTileHighlighters;
    NSMutableArray *tileHighlighterAssociations;
    
    CCSprite *spriteTiledMapContainer;
    CCSprite *spriteBlackOverlay;
    
    CGPoint tilePositionOfTileHighlighter;
    
    SPLGameCharacter *elric; // hack/test
    
    UITouch *mapDragTouchObject;
    CGPoint positionOfMapBeforeTouch;
    CGPoint positionOfFingerBeforeTouch;
    
    NSMutableDictionary *mapsLoaded;
    NSMutableDictionary *disabledItems;
    
    BOOL isMapDragDisabled;
    BOOL isPlayerDragDisabled;
    BOOL areInteractionsDisabled; // interactions like touching a character, touching items on the map
}

@property (nonatomic, readonly) UITouch *elricTouchObject;
@property (nonatomic, readonly) SPLGameCharacter *elric;

// TODO: rename to currentMap
@property (nonatomic, readonly) SPLTiledMap *tiledMap;

// Events
@property (nonatomic, retain) FLEvent *Event_Froze;
@property (nonatomic, retain) FLEvent *Event_Touched;
@property (nonatomic, retain) FLEvent *Event_TouchedWhenCloseBy;
@property (nonatomic, retain) FLEvent *Event_LandsBridged;
@property (nonatomic, retain) FLEvent *Event_MapWasPanned;
@property (nonatomic, retain) FLEvent *Event_MapWasLoaded;

-(void)loadMapWithName:(NSString *)mapName;
-(void)unloadCurrentMap;

#define Action_HighlightTileUnderPoint          FLActionMake(Action_HighlightTileUnderPoint)
#define Action_UnhighlightHighlightedTile       FLActionMake(Action_UnhighlightHighlightedTile)
#define Action_StopDraggingSelectedLetter       FLActionMake(Action_StopDraggingSelectedLetter)
#define Action_AddSelectedLetterToTiledMap      FLActionMake(Action_AddSelectedLetterToTiledMap)
#define Action_FreezeFloatingLetters            FLActionMake(Action_FreezeFloatingLetters)
#define Action_LoadMap                          FLActionMake(Action_LoadMap)
#define Action_UnloadMap                        FLActionMake(Action_UnloadMap)
#define Action_Disable                          FLActionMake(Action_Disable)
#define Action_Enable                           FLActionMake(Action_Enable)

@end
