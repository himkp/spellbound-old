//
//  SPLGameScene.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLCoreGameplayLayer.h"
#import "SPLGameplayButtonsLayer.h"
#import "SPLItemsPanelLayer.h"
#import "SPLCoinsPanelLayer.h"
#import "SPLInGameMenuLayer.h"
#import "SPLLetterPileLayer.h"
#import "SPLDialoguePanelLayer.h"
#import "SPLEnvironmentLayer.h"
#import "SPLSettingsPanelLayer.h"
#import "SPLAlertPanelLayer.h"
#import "SPLConfirmPanelLayer.h"
#import "FLYAML.h"

@interface SPLGameScene : CCScene <FLActionDelegate, FLYAMLDelegate, NSCoding>

@property (nonatomic, readonly) SPLCoreGameplayLayer *coreGameplayLayer;
@property (nonatomic, readonly) SPLGameplayButtonsLayer *gameplayButtonsLayer;
@property (nonatomic, readonly) SPLLetterPileLayer *letterPileLayer;
@property (nonatomic, readonly) SPLItemsPanelLayer *itemsPanelLayer;
@property (nonatomic, readonly) SPLCoinsPanelLayer *coinsPanelLayer;
@property (nonatomic, readonly) SPLSettingsPanelLayer *settingsPanelLayer;
@property (nonatomic, readonly) SPLInGameMenuLayer *inGameMenuLayer;
@property (nonatomic, readonly) SPLDialoguePanelLayer *dialoguePanelLayer;
@property (nonatomic, readonly) SPLEnvironmentLayer *environmentLayer;
@property (nonatomic, readonly) SPLAlertPanelLayer *alertPanelLayer;
@property (nonatomic, readonly) SPLConfirmPanelLayer *confirmPanelLayer;

// Events
@property (nonatomic, retain) FLEvent *Event_SceneWasLoaded;

// Actions
#define Action_HideGameplayButtonsAndLetterPile    FLActionMake(Action_HideGameplayButtonsAndLetterPile)
#define Action_ShowGameplayButtonsAndLetterPile    FLActionMake(Action_ShowGameplayButtonsAndLetterPile)

#define Action_CheckPoint                          FLActionMake(Action_CheckPoint)

@end
