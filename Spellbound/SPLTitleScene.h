//
//  SPLGameLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLTitleUILayer.h"
#import "SPLStartGameMenuLayer.h"
#import "SPLCreditsPanelLayer.h"
#import "SPLSettingsPanelLayer.h"
#import "SPLDialoguePanelLayer.h"

@interface SPLTitleScene : CCScene

@property (nonatomic, readonly) SPLTitleUILayer *primaryUILayer;
@property (nonatomic, readonly) SPLStartGameMenuLayer *startGameMenuLayer;
@property (nonatomic, readonly) SPLCreditsPanelLayer *creditsPanelLayer;
@property (nonatomic, readonly) SPLSettingsPanelLayer *settingsPanelLayer;

@end
