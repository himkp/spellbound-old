//
//  SPLGameCharacter.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"
#import "SPLTiledMap.h"
#import "FLAction.h"
#import "FLEvent.h"

typedef enum
{
    SPLGameCharacterAnimationIdleDown = 1,
    SPLGameCharacterAnimationIdleUp,
    SPLGameCharacterAnimationIdleLeft,
    SPLGameCharacterAnimationIdleRight,
    
    SPLGameCharacterAnimationWalkingDown,
    SPLGameCharacterAnimationWalkingUp,
    SPLGameCharacterAnimationWalkingLeft,
    SPLGameCharacterAnimationWalkingRight,
    
    SPLGameCharacterAnimationJumpingDown,
    SPLGameCharacterAnimationJumpingUp,
    SPLGameCharacterAnimationJumpingLeft,
    SPLGameCharacterAnimationJumpingRight,
    
    SPLGameCharacterAnimationExcitedDown
} SPLGameCharacterAnimation;

@interface SPLGameCharacter : CCSprite <FLActionDelegate, NSCoding>
{
    NSMutableString *lastSteppedOn;
    NSMutableArray *cliffsSteppedOn;
    
    // set to a value by action StopWalking
    FLAction *stopWalkingAction;
    
    CCSprite *spriteExclamationCallout;
    
    CCSprite *currentAnimationSprite;
    NSMutableDictionary *animationSpriteList;
    NSDictionary *jsonData;
    NSDictionary *animationData;
}

@property (nonatomic, readonly) NSString* characterName;
@property (nonatomic, readonly) SPLTiledMap* tiledMap;
@property (nonatomic, readonly) NSArray *cliffsSteppedOn;
@property (nonatomic, readonly) BOOL isAnimating;

@property (readonly) SPLGameCharacterAnimation currentAnimation;

@property (nonatomic, readonly) NSMutableArray *charactersFollowing;

// Events
@property (nonatomic, retain) FLEvent *Event_SteppedOn;
@property (nonatomic, retain) FLEvent *Event_IsWithinRadius;
@property (nonatomic, retain) FLEvent *Event_IsWithinDiagonalRadius;

#define Action_WalkOneTileUp        FLActionMake(Action_WalkOneTileUp)
#define Action_WalkOneTileDown      FLActionMake(Action_WalkOneTileDown)
#define Action_WalkOneTileLeft      FLActionMake(Action_WalkOneTileLeft)
#define Action_WalkOneTileRight     FLActionMake(Action_WalkOneTileRight)

#define Action_StopFacingUp         FLActionMake(Action_StopFacingUp)
#define Action_StopFacingDown       FLActionMake(Action_StopFacingDown)
#define Action_StopFacingLeft       FLActionMake(Action_StopFacingLeft)
#define Action_StopFacingRight      FLActionMake(Action_StopFacingRight)

#define Action_BeExcitedFacingDown  FLActionMake(Action_BeExcitedFacingDown) // flippers only action

#define Action_StopWalking          FLActionMake(Action_StopWalking)

#define Action_CircleAround         FLActionMake(Action_CircleAround)
#define Action_WalkNear             FLActionMake(Action_WalkNear)

#define Action_StartFollowing       FLActionMake(Action_StartFollowing)
#define Action_StopFollowing        FLActionMake(Action_StopFollowing)

#define Action_Exclaim              FLActionMake(Action_Exclaim)

#define Action_EmptyAction          FLActionMake(Action_EmptyAction)

-(id)initWithCharacterName:(NSString *)name;
-(id)initWithCharacterName:(NSString *)name andInitialAnimation:(SPLGameCharacterAnimation)animation;

-(CGPoint)currentTilePosition;
-(CGPoint)currentTilePositionInPoints;

-(void)teleportToTile:(CGPoint)tile;
-(void)walkToTile:(CGPoint)tile;
-(void)walkToTile:(CGPoint)tile andThen:(void(^)(void))block;

-(BOOL)canMoveToTile:(CGPoint)tile;

-(void)walkAlongPath:(NSArray*)path;
-(void)walkAlongPath:(NSArray *)path andThen:(void(^)(void))block;

-(void)runAnimation:(SPLGameCharacterAnimation)animation;

+(id)characterWithName:(NSString*)name;
+(id)characterWithName:(NSString *)name andInitialAnimation:(SPLGameCharacterAnimation)animation;

@end
