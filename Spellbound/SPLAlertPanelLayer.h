//
//  SPLAlertPanelLayer.h
//  Spellbound
//
//  Created by Fleon Games on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLAlertPanelLayer : CCLayer <FLActionDelegate>
{
    CCSprite *spriteBackground;
    CCSprite *spriteThePanel;
    CCSprite *spriteWhiteUnderlay;
    CCSprite *spriteBorderTop;
    CCSprite *spriteBorderLeft;
    CCSprite *spriteBorderRight;
    CCSprite *spriteBorderBottom1;
    CCSprite *spriteBorderBottom2;
    CCSprite *spriteButtonClose;
    CCSprite *spriteButtonOK;
    
    CGPoint positionOfBackground;
    CGPoint positionOfThePanel;
    CGPoint positionOfWhiteUnderlay;
    CGPoint positionOfBorderTop;
    CGPoint positionOfBorderLeft;
    CGPoint positionOfBorderRight;
    CGPoint positionOfBorderBottom1;
    CGPoint positionOfBorderBottom2;
    CGPoint positionOfButtonClose;
    CGPoint positionOfButtonOK;
    
    FLAction *currentAction;
    
    CCLabelBMFont *labelMessage;
}

-(void)initializePositions;
-(void)setupBordersAndBackground;

@property (nonatomic, assign) NSString *message;

-(void)show;
-(void)hide;
-(void) show: (void(^)()) onShow;
-(void) hide: (void(^)()) onHide;

#define Action_Alert FLActionMake(Action_Alert)

@end
