//
//  SPLInteractiveItemMagicalBag.h
//  Spellbound
//
//  Created by Fleon Games on 1/19/14.
//
//

#import "SPLInteractiveItem.h"

@interface SPLInteractiveItemMagicalBag : SPLInteractiveItem
{
    CCSprite *spriteBack;
    CCSprite *spriteFront;
    
    CCParticleSystem *spriteEmitterGlitter;
}

@end
