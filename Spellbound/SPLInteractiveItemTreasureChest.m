//
//  SPLInteractiveItemTreasureChest.m
//  Spellbound
//
//  Created by Fleon Games on 9/16/13.
//
//

#import "SPLInteractiveItemTreasureChest.h"
#import "SPLTiledMap.h"

@implementation SPLInteractiveItemTreasureChest

- (id)init
{
    self = [super init];
    if (self) {
        spriteBottomBack = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_TreasureChest_BottomBack.png"];
        spriteBottomFront = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_TreasureChest_BottomFront.png"];
        spriteLidClosed = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_TreasureChest_LidClosed.png"];
        spriteLidOpenBack = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_TreasureChest_LidOpenBack.png"];
        spriteLidOpenFront = [CCSprite spriteWithSpriteFrameName:@"InteractiveItem_TreasureChest_LidOpenFront.png"];
        
        spriteBottomBack.anchorPoint = ccp(0, 0);
        spriteBottomFront.anchorPoint = ccp(0, 0);
        spriteLidClosed.anchorPoint = ccp(0, 0);
        spriteLidOpenBack.anchorPoint = ccp(0, 0);
        spriteLidOpenFront.anchorPoint = ccp(0, 0);
        
        spriteEmitterGlitter = [[CCParticleFireworks alloc] init];
        spriteEmitterGlitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"Assets/Common/Stars.png"];
        
        spriteLidOpenFront.visible = NO;
        spriteLidOpenBack.visible = NO;
                
        [self addChild:spriteBottomBack z:0];
        [self addChild:spriteBottomFront z:2];
        [self addChild:spriteLidClosed z:3];
        [self addChild:spriteLidOpenBack z:4];
        [self addChild:spriteLidOpenFront z:5];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
        if (self.hasInteractedWith)
        {
            spriteLidOpenFront.visible = YES;
            spriteLidOpenBack.visible = YES;
            spriteLidClosed.visible = NO;
        }
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_InteractWith])
    {
        if (self.hasInteractedWith) return [action finish]; // can't interact with if already interacted with before
        self.hasInteractedWith = YES;
        
        spriteLidOpenFront.opacity = 0;
        spriteLidOpenFront.visible = YES;
        [spriteLidOpenFront runAction:[CCFadeIn actionWithDuration:0.2]];
        
        spriteLidOpenBack.opacity = 0;
        spriteLidOpenBack.visible = YES;
        [spriteLidOpenBack runAction:[CCFadeIn actionWithDuration:0.2]];
        
        [spriteLidClosed runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.2] two:[CCHide action]]];
        
        [self runAction:[CCSequence actions:[CCCallBlock actionWithBlock:^{
            [self addChild:spriteEmitterGlitter z:1];
        }], [CCDelayTime actionWithDuration:4], [CCCallBlock actionWithBlock:^{
            [self removeChild:spriteEmitterGlitter cleanup:YES];
        }], nil]];
    }
    else [super handleAction:action];
}

-(void)setIsPickedUp:(BOOL)isPickedUp // can only be set to true
{
    if (super.isPickedUp) return;
}

-(void)setParent:(CCNode *)parent
{
    [super setParent:parent];
    
    // set size when added to the tiled map
    self.contentSize = CGSizeMake(self.tiledMap.tileSize.width * 2, self.tiledMap.tileSize.height);
    
    // set the properties of the glitter, since they depend on the content size
    spriteEmitterGlitter.position = ccp(self.boundingBox.size.width / 2, 0.15 * self.boundingBox.size.height);
    spriteEmitterGlitter.blendAdditive = YES;
    spriteEmitterGlitter.life = 1;
    spriteEmitterGlitter.lifeVar = 0.25;
    spriteEmitterGlitter.scale = 0.7;
    spriteEmitterGlitter.duration = 3.0;
    spriteEmitterGlitter.speed = 1.5 * self.boundingBox.size.height;
    spriteEmitterGlitter.speedVar = 0.5 * self.boundingBox.size.height;
    spriteEmitterGlitter.posVar = ccp(0.45 * self.boundingBox.size.width, 0);
}

- (void)dealloc
{
    [spriteEmitterGlitter release];
    [super dealloc];
}

@end
