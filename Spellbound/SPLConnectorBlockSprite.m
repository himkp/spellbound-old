//
//  SPLConnectorBlockSprite.m
//  Spellbound
//
//  Created by Fleon Games on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLConnectorBlockSprite.h"
#import "NSData+Base64.h"

SPLConnectorBlockInfo SPLConnectorBlockSpriteInfoMake(SPLConnectorBlockDirection directionStart, SPLConnectorBlockDirection directionEnd, BOOL isDirectionStartValid, BOOL isDirectionEndValid)
{
    return (SPLConnectorBlockInfo) {directionStart, directionEnd, isDirectionStartValid, isDirectionEndValid};
}

@implementation SPLConnectorBlockSprite

+(id)connectorBlockSpriteWithInfo:(SPLConnectorBlockInfo)info
{
    return [[[self alloc] initWithInfo:info] autorelease];
}

-(id)initWithInfo:(SPLConnectorBlockInfo)info
{
    NSUInteger tileDimensions = DEVICE_IS_IPAD ? 64 : 40;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY) 
        tileDimensions *= 2;
    
    CGSize screenSize = DEVICE_IS_IPAD ? CGSizeMake(1024, 768) : CGSizeMake(640, 480);
    if (DEVICE_SUPPORTS_RETINA_DISPLAY) 
        screenSize.width *= 2, 
        screenSize.height *= 2;
    
    NSMutableString *tilesetPath = [NSMutableString stringWithString:DEVICE_IS_IPAD ? @"iPad" : @"iPhone"];
    [tilesetPath appendString:@"/Maps/SpellboundTiles"];
    [tilesetPath appendString:DEVICE_SUPPORTS_RETINA_DISPLAY ? @"-hd.png" : @".png"];
    
    UInt32 circleGID = 151 + info.isDirectionStartValid;
    UInt32 directionStartGID, directionEndGID;
    
    if (info.directionStart == SPLConnectorBlockDirectionTop)
        directionStartGID = 169 + info.isDirectionStartValid;
    else if (info.directionStart == SPLConnectorBlockDirectionBottom)
        directionStartGID = 153 + info.isDirectionStartValid;
    else if (info.directionStart == SPLConnectorBlockDirectionLeft)
        directionStartGID = 155 + info.isDirectionStartValid * 16;
    else if (info.directionStart == SPLConnectorBlockDirectionRight)
        directionStartGID = 156 + info.isDirectionStartValid * 16;
    else if (info.directionStart == SPLConnectorBlockDirectionNone)
        directionStartGID = 0;
    
    if (info.directionEnd == SPLConnectorBlockDirectionTop)
        directionEndGID = 169 + info.isDirectionEndValid;
    else if (info.directionEnd == SPLConnectorBlockDirectionBottom)
        directionEndGID = 153 + info.isDirectionEndValid;
    else if (info.directionEnd == SPLConnectorBlockDirectionLeft)
        directionEndGID = 155 + info.isDirectionEndValid * 16;
    else if (info.directionEnd == SPLConnectorBlockDirectionRight)
        directionEndGID = 156 + info.isDirectionEndValid * 16;
    else if (info.directionEnd == SPLConnectorBlockDirectionNone)
        directionEndGID = 0;
    
    NSData *layerDirectionStart = [NSData dataWithBytes:&directionStartGID length:4];
    NSData *layerDirectionEnd = [NSData dataWithBytes:&directionEndGID length:4];
    NSData *layerCircle = [NSData dataWithBytes:&circleGID length:4];
    
    NSString *xmlString = [NSString stringWithFormat:
                           @"<map version=\"1.0\" orientation=\"orthogonal\" width=\"1\" height=\"1\" tilewidth=\"%d\" tileheight=\"%d\">\
                           <tileset firstgid=\"1\" name=\"SpellboundTiles\" tilewidth=\"%d\" tileheight=\"%d\">\
                           <image source=\"SpellboundTiles.png\" width=\"%d\" height=\"%d\"/>\
                           </tileset>\
                           <layer name=\"direction start\" width=\"1\" height=\"1\">\
                           <data encoding=\"base64\">\
                           %@ \
                           </data>\
                           </layer>\
                           <layer name=\"direction end\" width=\"1\" height=\"1\">\
                           <data encoding=\"base64\">\
                           %@ \
                           </data>\
                           </layer>\
                           <layer name=\"circle\" width=\"1\" height=\"1\">\
                           <data encoding=\"base64\">\
                           %@ \
                           </data>\
                           </layer>\
                           </map>",
                           tileDimensions,
                           tileDimensions,
                           tileDimensions,
                           tileDimensions,
                           (int) screenSize.width, 
                           (int) screenSize.height,
                           [layerDirectionStart base64EncodedString],
                           [layerDirectionEnd base64EncodedString],
                           [layerCircle base64EncodedString]];

    if (self = [super initWithXML:xmlString resourcePath:[tilesetPath stringByDeletingLastPathComponent]])
    {
        blockInfo = info;
    }
    return self;
}

@end
