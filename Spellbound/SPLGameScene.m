//
//  SPLGameScene.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGameScene.h"
#import "FLEvent.h"


@implementation SPLGameScene

@synthesize Event_SceneWasLoaded;

-(void)prefetchTextures
{
    // let us load the textures
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     DEVICE_IS_IPAD ? @"iPad/GameplayScreen_Textures.plist" : @"iPhone/GameplayScreen_Textures.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     DEVICE_IS_IPAD ? @"iPad/AlertPanel+ConfirmPanel_Textures.plist" : @"iPhone/AlertPanel+ConfirmPanel_Textures.plist"];
}

-(void)addAllLayersAsChild
{
    [self addChild:_coreGameplayLayer z:1];
    [self addChild:_environmentLayer z:2];
    [self addChild:_gameplayButtonsLayer z:3];
    [self addChild:_letterPileLayer z:4];
    [self addChild:_inGameMenuLayer z:5];
    [self addChild:_dialoguePanelLayer z:9];
    [self addChild:_itemsPanelLayer z:10];
    [self addChild:_coinsPanelLayer z:11];
    [self addChild:_settingsPanelLayer z:12];
    [self addChild:_alertPanelLayer z:13];
    [self addChild:_confirmPanelLayer z:14];
}

-(id)init
{
    if (self = [super init])
    {
        // Init Events
        FLEventMake(Event_SceneWasLoaded);
        
        [self prefetchTextures];
        [FLYAML yamlWithYAMLDelegate:self];
        
        _coreGameplayLayer = [SPLCoreGameplayLayer node];
        _gameplayButtonsLayer = [SPLGameplayButtonsLayer node];
        _letterPileLayer = [SPLLetterPileLayer node];
        _inGameMenuLayer = [SPLInGameMenuLayer node];
        _itemsPanelLayer = [SPLItemsPanelLayer node];
        _coinsPanelLayer = [SPLCoinsPanelLayer node];
        _dialoguePanelLayer = [SPLDialoguePanelLayer node];
        _settingsPanelLayer = [SPLSettingsPanelLayer node];
        _environmentLayer = [SPLEnvironmentLayer node];
        _alertPanelLayer = [SPLAlertPanelLayer node];
        _confirmPanelLayer = [SPLConfirmPanelLayer node];
        
        [self addAllLayersAsChild];
        
        [Event_SceneWasLoaded dispatchAfterTime:1];
    }
    return self;
}

-(void)saveGame
{
    NSData *saveData = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[Spellbound localStorage] setData:saveData forKey:@"saveData"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        // Init Events
        FLEventMake(Event_SceneWasLoaded);
        
        [self prefetchTextures];
        [FLYAML yamlWithYAMLDelegate:self];
        
        _coreGameplayLayer = [decoder decodeObjectForKey:@"coreGameplayLayer"];
        _gameplayButtonsLayer = [decoder decodeObjectForKey:@"gameplayButtonsLayer"];
        _letterPileLayer = [decoder decodeObjectForKey:@"letterPileLayer"];;
        _inGameMenuLayer = [SPLInGameMenuLayer node];
        _coinsPanelLayer = [decoder decodeObjectForKey:@"coinsPanelLayer"];
        _itemsPanelLayer = [decoder decodeObjectForKey:@"itemsPanelLayer"];
        _settingsPanelLayer = [decoder decodeObjectForKey:@"settingsPanelLayer"];
        _dialoguePanelLayer = [SPLDialoguePanelLayer node];
        _environmentLayer = [SPLEnvironmentLayer node];
        _alertPanelLayer = [SPLAlertPanelLayer node];
        _confirmPanelLayer = [SPLConfirmPanelLayer node];
        
        [self addAllLayersAsChild];
        
        NSString *audioTrackName = [decoder decodeObjectForKey:@"currentAudioTrack"];
        if (audioTrackName) [[Spellbound audioPlayer] playTrack:audioTrackName];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    // todo: serialize all the other layers as well along with their states (hidden / visible)
    [encoder encodeObject:_coreGameplayLayer forKey:@"coreGameplayLayer"];
    [encoder encodeObject:_gameplayButtonsLayer forKey:@"gameplayButtonsLayer"];
    [encoder encodeObject:_itemsPanelLayer forKey:@"itemsPanelLayer"];
    [encoder encodeObject:_settingsPanelLayer forKey:@"settingsPanelLayer"];
    [encoder encodeObject:_coinsPanelLayer forKey:@"coinsPanelLayer"];
    [encoder encodeObject:_letterPileLayer forKey:@"letterPileLayer"];
    [encoder encodeObject:[Spellbound audioPlayer].nowPlaying forKey:@"currentAudioTrack"];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_HideGameplayButtonsAndLetterPile])
    {
        __block NSUInteger actionsFinished = 0;
        void(^onFinish)() = ^{
            actionsFinished++;
            if (actionsFinished == 2) {
                [action finish];
            }
        };

        FLAction *subAction;
        subAction = Action_HideGameplayButtons;
        subAction.onFinish = onFinish;
        [subAction run];
        subAction = Action_HideLetterPile;
        [subAction run];
        subAction.onFinish = onFinish;
    }
    else if ([action is:Action_ShowGameplayButtonsAndLetterPile])
    {
        __block NSUInteger actionsFinished = 0;
        void(^onFinish)() = ^{
            actionsFinished++;
            if (actionsFinished == 2) {
                [action finish];
            }
        };
        
        FLAction *subAction;
        subAction = Action_ShowGameplayButtons;
        subAction.onFinish = onFinish;
        [subAction run];
        subAction = Action_ShowLetterPile;
        [subAction run];
        subAction.onFinish = onFinish;

    }
    else if ([action is:Action_CheckPoint])
    {
        [[Spellbound checkPointManager] activateCheckPoint:action];
        [self saveGame];
        [action finish];
    }
    else
    {
        [action forwardToActionDelegates:_dialoguePanelLayer,
                                         _gameplayButtonsLayer,
                                         _letterPileLayer,
                                         _coreGameplayLayer,
                                         _alertPanelLayer,
                                         _confirmPanelLayer,
                                        [Spellbound globalActionDelegate], nil];
    }
}

- (void)dealloc
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/GameplayScreen_Textures.plist" : @"iPhone/GameplayScreen_Textures.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/AlertPanel+ConfirmPanel_Textures.plist" : @"iPhone/AlertPanel+ConfirmPanel_Textures.plist"];
    
    // release events
    [Event_SceneWasLoaded release];
    
    [super dealloc];
}

@end
