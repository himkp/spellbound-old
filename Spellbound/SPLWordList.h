//
//  SPLWordList.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FMDatabase.h"
#import "Spellbound.h"

@interface SPLWordList : NSObject {
@private
    FMDatabase* fmdb;
}

@property (nonatomic) SPLLocalizedWordList defaultWordList;

-(BOOL)doesWordExist:(NSString*)word;

-(NSString *)randomWordWithNumberOfLetters:(NSUInteger)count;

@end
