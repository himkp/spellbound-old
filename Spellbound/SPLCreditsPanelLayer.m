//
//  SPLCreditsLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLCreditsPanelLayer.h"

@implementation SPLCreditsPanelLayer

-(id)init
{
    if (self = [super initWithPanelName:@"Credits"])
    {
        CGSize s = [[CCDirector sharedDirector] winSize];
		
        NSString* credits1 = @"GAME DESIGN &\nPROGRAMMING:\n\nART & UI DESIGN:\n\n\nANIMATION:\n\nMUSIC &\nSOUND DESIGN:";
		CCLabelBMFont *label1 = [CCLabelBMFont labelWithString:credits1 fntFile:
                                 DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt" : 
                                 @"iPhone/Fonts/DoctorSoosBold.fnt"];
        
        NSString* credits2 = @"\nHimanshu Kapoor\n\nWandi Julham\nEdo Septiyan\n\nK. Baukowviewy Phasom \n\n\nMegan Randa";
        CCLabelBMFont *label2 = [CCLabelBMFont labelWithString:credits2 fntFile:
                                 DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt" : 
                                 @"iPhone/Fonts/DoctorSoosBold.fnt"];
        
        NSString* credits3 = @"(c) 2012 Fleon Games LLC. All Rights Reserved.\nVisit us at www.fleon.org";
        CCLabelBMFont *label3 = [CCLabelBMFont labelWithString:credits3 fntFile:
                                 DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt" : 
                                 @"iPhone/Fonts/DoctorSoosBold.fnt"];
        
        [label1 setAlignment:UITextAlignmentRight];
        [label2 setAlignment:UITextAlignmentLeft];
        [label3 setAlignment:UITextAlignmentCenter];
        
		[spriteThePanel addChild:label1];
        [spriteThePanel addChild:label2];
        [spriteThePanel addChild:label3];
		[label1 setPosition: ccp(s.width/2 - (DEVICE_IS_IPAD ? 150 : 70), s.height/2)];
        [label2 setPosition: ccp(s.width/2 + (DEVICE_IS_IPAD ? 150 : 70), s.height/2)];
		[label3 setPosition: ccp(s.width/2, s.height/2 - (DEVICE_IS_IPAD ? 350 : 164))];
    }
    return self;
}

@end
