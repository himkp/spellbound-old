//
//  SPLLetterPile.m
//  Spellbound
//
//  Created by Fleon Games on 5/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLLetterPile.h"

@implementation SPLLetterPile

+(id)letterPileWithNumberOfLettersInHand:(int)numberOfLettersInHand andInBag:(int)numberOfLettersInBag
{
    return [[[self alloc] initWithNumberOfLettersInHand:numberOfLettersInHand andInBag:numberOfLettersInBag] autorelease];
}

-(id)initWithNumberOfLettersInHand:(int)numberOfLettersInHand andInBag:(int)numberOfLettersInBag
{
    if (self = [super init])
    {
        _lettersInBag = [[NSMutableString alloc] initWithString:@""];
        _lettersInHand = [[NSMutableString alloc] initWithString:@""];
        _numberOfLettersInHand = numberOfLettersInHand;
        
        [self refillLettersInBag:numberOfLettersInBag];
        [self refillLettersInHand];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        _lettersInHand = [[decoder decodeObjectForKey:@"lettersInHand"] mutableCopy];
        _lettersInBag = [[decoder decodeObjectForKey:@"lettersInBag"] mutableCopy];
        _numberOfLettersInHand = [decoder decodeIntegerForKey:@"numberOfLettersInHand"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_lettersInBag forKey:@"lettersInBag"];
    [encoder encodeObject:_lettersInHand forKey:@"lettersInHand"];
    [encoder encodeInteger:_numberOfLettersInHand forKey:@"numberOfLettersInHand"];
}

-(void)refillLettersInBag:(int)numberOfLettersToAdd
{
    // todo: fix this algorithm
    NSMutableString *letters = [NSMutableString stringWithString:@""];
    while (letters.length < numberOfLettersToAdd) {
        int charCount = arc4random() % 10 + 2;
        NSString *word = [[Spellbound wordList] randomWordWithNumberOfLetters:charCount];
        NSString *scrambledWord = [word scramble];
        [letters appendString:scrambledWord];
    }
    [letters setString:[letters substringToIndex:numberOfLettersToAdd - 1]];
    [_lettersInBag appendString:letters];
}

-(void)refillLettersInHand
{
    for (int i = 0, l = _numberOfLettersInHand - _lettersInHand.length; i < l; i++)
    {
        [_lettersInHand appendString:[_lettersInBag substringToIndex:1]];
        [_lettersInBag setString:[_lettersInBag substringFromIndex:1]];
    }
}

-(void)prependLettersInBag:(NSString *)letterString
{
    [_lettersInBag setString:[letterString stringByAppendingString:_lettersInBag]];
}

-(void)insertLetterInHand:(NSString *)letter
{
    [_lettersInHand appendString:letter];
    [_lettersInBag setString:[_lettersInBag substringFromIndex:1]];
}

-(int)numberOfLettersInBag
{
    return [_lettersInBag length];
}

-(void)dealloc
{
    [_lettersInBag release];
    [_lettersInHand release];
    
    [super dealloc];
}

@end
