//
//  FLGlobal.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FL.h"

@implementation FL

+(CGPoint)translatePoint:(CGPoint)point relativeTo:(FLScreenCorner)corner
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGPoint originPt, originSignPt;
    
    switch (corner) {
        case kFLScreenCornerBottomLeft: 
            originPt = ccp(0, 0); 
            originSignPt = ccp(1, 1);
            break;
        case kFLScreenCornerBottomRight:
            originPt = ccp(winSize.width, 0);
            originSignPt = ccp(-1, 1);
            break;
        case kFLScreenCornerTopLeft:
            originPt = ccp(0, winSize.height);
            originSignPt = ccp(1, -1);
            break;
        case kFLScreenCornerTopRight:
            originPt = ccp(winSize.width, winSize.height);
            originSignPt = ccp(-1, -1);
            break;
    }
    return ccpAdd(originPt, ccpCompMult(originSignPt, point));
}

+(CGPoint)translatePoint:(CGPoint)point
{
    return [FL translatePoint:point relativeTo:kFLScreenCornerTopLeft];
}

+(BOOL)doesSprite:(CCNode*)sp containPoint:(CGPoint)pt // pt is global
{
    pt = [sp convertToNodeSpaceAR:pt];
    
    float width = sp.boundingBox.size.width / sp.scaleX;
    float height = sp.boundingBox.size.height / sp.scaleY;
    CGRect boundingBox = CGRectMake(-width * sp.anchorPoint.x, -height * sp.anchorPoint.y, width, height);
    
    if ([sp respondsToSelector:@selector(opacity)])
        return sp.visible && ((CCSprite*) sp).opacity > 0 && CGRectContainsPoint(boundingBox, pt);
    else 
        return sp.visible && CGRectContainsPoint(boundingBox, pt);
}

+(void)with:(CCNode *)sprite runAnimationAction:(FLAnimationAction)action
{
    if (action == kFLAnimationActionGrowAndShrink)
    {
        [sprite runAction:
         [CCSequence actionOne:[CCScaleTo actionWithDuration:0.15 scale:1.2]
                           two:[CCScaleTo actionWithDuration:0.15 scale:1.0]]];
    }
    else if (action == kFLAnimationActionShrinkAndGrow)
    {
        [sprite runAction:
         [CCSequence actionOne:[CCScaleTo actionWithDuration:0.15 scale:0.8]
                           two:[CCScaleTo actionWithDuration:0.15 scale:1.0]]];
    }
    else if (action == kFLAnimationActionShrink)
    {
        [sprite runAction:
         //[CCEaseBackOut actionWithAction:
          [CCScaleTo actionWithDuration:0.25 scale:0.8]];
    }
    else if (action == kFLAnimationActionGrow)
    {
        [sprite runAction:
         [CCEaseBackOut actionWithAction:
          [CCScaleTo actionWithDuration:0.25 scale:1.2]]];
    }
    else if (action == kFLAnimationActionRestoreSize)
    {
        [sprite runAction:
         [CCEaseBackOut actionWithAction:
          [CCScaleTo actionWithDuration:0.25 scale:1.0]]];
    }
}

+(void)with:(CCNode *)sprite
setPosition:(CGPoint)position
andAnchorPoint:(FLAnchorPoint)anchorPoint
 fromCorner:(FLScreenCorner)corner
{
    CGPoint anchorPt, sizePt;
    CGSize winSize;
    
    switch (anchorPoint) {
        case kFLAnchorPointBottomCenter: anchorPt = ccp(0.5, 0); break;
        case kFLAnchorPointBottomRight: anchorPt = ccp(1, 0); break;
        case kFLAnchorPointBottomLeft: anchorPt = ccp(0, 0); break;
        case kFLAnchorPointCenter: anchorPt = ccp(0.5, 0.5); break;
        case kFLAnchorPointCenterLeft: anchorPt = ccp(0, 0.5); break;
        case kFLAnchorPointCenterRight: anchorPt = ccp(1, 0.5); break;
        case kFLAnchorPointTopCenter: anchorPt = ccp(0.5, 1); break;
        case kFLAnchorPointTopLeft: anchorPt = ccp(0, 1); break;
        case kFLAnchorPointTopRight: anchorPt = ccp(1, 1); break;
    }
    
    winSize = [CCDirector sharedDirector].winSize;
    
    sizePt = CGPointMake(sprite.boundingBox.size.width, sprite.boundingBox.size.height);
    
    sprite.anchorPoint = anchorPt;
    sprite.position = [FL translatePoint:ccpAdd(position, ccpCompMult(sizePt, ccp(anchorPt.x, 1 - anchorPt.y))) relativeTo:corner];
}

+(void)with:(CCNode *)sprite setPosition:(CGPoint)position andAnchorPoint:(FLAnchorPoint)anchorPoint
{
    [FL with:sprite setPosition:position andAnchorPoint:anchorPoint fromCorner:kFLScreenCornerTopLeft];
}

+(void)with:(CCNode *)sprite setPosition:(CGPoint)position
{
    [FL with:sprite setPosition:position andAnchorPoint:kFLAnchorPointTopLeft fromCorner:kFLScreenCornerTopLeft];
}

+(void)with:(CCNode *)sprite adjustAnchorPoint:(CGPoint)point
{
    CGSize size = sprite.boundingBox.size;
    CGPoint oldAnchorPoint = sprite.anchorPoint;
    CGPoint diffPt = ccpSub(point, oldAnchorPoint);
    sprite.anchorPoint = point;
    sprite.position = ccp(sprite.position.x + diffPt.x * size.width, sprite.position.y + diffPt.y * size.height);
}

+(void)with:(CCNode *)sprite adjustAnchorPointInPoints:(CGPoint)point
{
    CGSize size = sprite.boundingBox.size;
    [FL with:sprite adjustAnchorPoint:ccp(point.x / size.width, point.y / size.height)];
}

@end

@implementation NSString (fl_substring_search)
- (unsigned) countOccurencesOf: (NSString *)subString {
    unsigned count = 0;
    unsigned myLength = [self length];
    NSRange uncheckedRange = NSMakeRange(0, myLength);
    for(;;) {
        NSRange foundAtRange = [self rangeOfString:subString
                                           options:0
                                             range:uncheckedRange];
        if (foundAtRange.location == NSNotFound) return count;
        unsigned newLocation = NSMaxRange(foundAtRange); 
        uncheckedRange = NSMakeRange(newLocation, myLength-newLocation);
        count++;
    }
}

-(unsigned int)indexOf:(NSString *)needle
{
    return [self rangeOfString:needle].location;
}

@end

