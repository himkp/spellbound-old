//
//  SPLInteractiveItemTreasureChest.h
//  Spellbound
//
//  Created by Fleon Games on 9/16/13.
//
//

#import "SPLInteractiveItem.h"

@interface SPLInteractiveItemTreasureChest : SPLInteractiveItem
{
    CCSprite *spriteBottomBack;
    CCSprite *spriteBottomFront;
    CCSprite *spriteLidOpenBack;
    CCSprite *spriteLidOpenFront;
    CCSprite *spriteLidClosed;
    
    CCParticleSystem *spriteEmitterGlitter;
}

@end
