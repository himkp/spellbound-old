//
//  SPLCreditsLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLOverlayPanelLayer.h"

@implementation SPLOverlayPanelLayer

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfThePanel          = fl_ccp(   0,    0, 2048, 1536);
        positionOfBackground        = fl_ccp(   0,    0, 2048, 1536);
        positionOfWhiteUnderlay     = fl_ccp( 200,  200, 1640, 1400);
        
        positionOfBorderLeft        = fl_ccp(  70,  502,  148, 1034);
        positionOfBorderTop         = fl_ccp( 620,   34, 1090,  178);
        positionOfBorderRight       = fl_ccp(1807,  292,  141, 1244);
        positionOfButtonClose       = fl_ccp(1710,   34,  308,  258);
        positionOfButtonPanelTitle  = fl_ccp(  16,   34,  604,  468);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfThePanel          = fl_ccp(   0,    0,  960,  640);
        positionOfBackground        = fl_ccp(   0,    0,  960,  640);
        positionOfWhiteUnderlay     = fl_ccp(  90,   90,  776,  583);
        
        positionOfBorderLeft        = fl_ccp(  40,  245,   66,  395);
        positionOfBorderTop         = fl_ccp( 295,   21,  507,   89);
        positionOfBorderRight       = fl_ccp( 847,  143,   67,  497);
        positionOfButtonClose       = fl_ccp( 802,   21,  145,  122);
        positionOfButtonPanelTitle  = fl_ccp(   0,    0,  295,  245);
    }
}

-(void)setupBordersAndBackground
{
    spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
    spriteBackground.scaleX = DEVICE_IS_IPAD ? 1024 : 480;
    spriteBackground.scaleY = DEVICE_IS_IPAD ? 768 : 320;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY)
    {
        spriteBackground.scaleX *= 2;
        spriteBackground.scaleY *= 2;
    }
    spriteBackground.position = positionOfBackground;
    
    spriteWhiteUnderlay = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
    spriteWhiteUnderlay.scaleX = DEVICE_IS_IPAD ? 820 : 388;
    spriteWhiteUnderlay.scaleY = DEVICE_IS_IPAD ? 700 : 292;
    if (DEVICE_SUPPORTS_RETINA_DISPLAY)
    {
        spriteWhiteUnderlay.scaleX *= 2;
        spriteWhiteUnderlay.scaleY *= 2;
    }
    spriteWhiteUnderlay.position = positionOfWhiteUnderlay;
    
    spriteBorderTop = [CCSprite spriteWithSpriteFrameName:@"OverlayPanel_Border_Top.png"];
    spriteBorderLeft = [CCSprite spriteWithSpriteFrameName:@"OverlayPanel_Border_Left.png"];
    spriteBorderRight = [CCSprite spriteWithSpriteFrameName:@"OverlayPanel_Border_Right.png"];
    spriteButtonClose = [CCSprite spriteWithSpriteFrameName:@"OverlayPanel_Button_Close.png"];
    
    spriteBorderTop.position = positionOfBorderTop;
    spriteBorderLeft.position = positionOfBorderLeft;
    spriteBorderRight.position = positionOfBorderRight;
    spriteButtonClose.position = positionOfButtonClose;
    
    spriteThePanel = [CCSprite node];
    spriteThePanel.contentSize = [CCDirector sharedDirector].winSize;
    
    [self addChild:spriteBackground];
    [self addChild:spriteThePanel];
    [spriteThePanel addChild:spriteWhiteUnderlay];
    [spriteThePanel addChild:spriteBorderTop];
    [spriteThePanel addChild:spriteBorderLeft];
    [spriteThePanel addChild:spriteBorderRight];
    [spriteThePanel addChild:spriteButtonClose];
}

-(id)initWithPanelName:(NSString*)name
{
    if (self = [super init])
    {
        self.visible = NO;
        self.isTouchEnabled = YES;
        
        [self initializePositions];
        [self setupBordersAndBackground];
        
        panelName = [name retain];
        
        NSString *panelPNGFrameName = [NSString stringWithFormat:@"OverlayPanel_Label_%@.png", panelName];
        spriteButtonPanelTitle = [CCSprite spriteWithSpriteFrameName:panelPNGFrameName];
        spriteButtonPanelTitle.position = positionOfButtonPanelTitle;
        
        [spriteThePanel addChild:spriteButtonPanelTitle];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [self initWithPanelName:[decoder decodeObjectForKey:@"panelName"]])
    {
        // do something here
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:panelName forKey:@"panelName"];
}

+(id)layerWithPanelName:(NSString *)name
{
    return [[[SPLOverlayPanelLayer alloc] initWithPanelName:name] autorelease];
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if ([self visible])
    {
        if ([FL doesSprite:spriteButtonClose containPoint:point])
            [self hide];
        return YES;
    }
    return NO;
}

-(void)show
{
    [self show: ^ {}];
}

-(void)show:(void (^)())onShow
{
    if (self.isDisabled) return onShow();
    
    self.visible = YES;
    spriteBackground.opacity = 0;
    
    [FL with:spriteThePanel setPosition:ccp(0, [CCDirector sharedDirector].winSize.height)];
    
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:180]];
    [spriteThePanel runAction:
     [CCEaseExponentialOut actionWithAction:
      [CCSequence actionOne:[FLMoveTo actionWithDuration:0.5 position:ccp(0, 0)]
                        two:[CCCallBlock actionWithBlock:onShow]]]];
}

-(void)hide
{
    [self hide:^ {}];
}

-(void)hide:(void (^)())onHide
{
    if (self.isDisabled) return onHide();
    
    [spriteThePanel runAction:[CCEaseExponentialIn actionWithAction:[CCMoveTo actionWithDuration:0.5 position:ccp(0, 0)]]];
    [spriteBackground runAction:[CCFadeTo actionWithDuration:0.5 opacity:0]];
    [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:0.5], [CCHide action], [CCCallBlock actionWithBlock:onHide], nil]];
}

-(void)dealloc
{
    [panelName release];
    [super dealloc];
}

@end
