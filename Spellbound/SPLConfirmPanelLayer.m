//
//  SPLConfirmPanelLayer.m
//  Spellbound
//
//  Created by Fleon Games on 5/5/13.
//
//

#import "SPLConfirmPanelLayer.h"
#import "FLEvent.h"

@implementation SPLConfirmPanelLayer

@synthesize Event_DidConfirm, Event_DidNotConfirm;

-(void)initializePositions
{
    [super initializePositions];
    
    if (DEVICE_IS_IPAD)
    {
        positionOfBorderBottom1     = fl_ccp( 288,  758,  175,  148);
        positionOfBorderBottom2     = fl_ccp( 882,  758,  296,  106);
        positionOfBorderBottom3     = fl_ccp(1587,  778,  152,  122);
        positionOfButtonOK          = fl_ccp( 463,  758,  419,  206);
        positionOfButtonCancel      = fl_ccp(1178,  758,  409,  206);
    }
    else if (DEVICE_IS_IPHONE)
    {
        
        // the game is currently only for iPad
    }
}

-(void)setupBordersAndBackground
{
    [super setupBordersAndBackground];
    
    [spriteThePanel removeChild:spriteBorderBottom1 cleanup:YES];
    [spriteThePanel removeChild:spriteBorderBottom2 cleanup:YES];
    [spriteThePanel removeChild:spriteButtonOK cleanup:YES];
    
    spriteBorderBottom1 = [CCSprite spriteWithSpriteFrameName:@"ConfirmPanel_Border_Bottom_1.png"];
    spriteBorderBottom2 = [CCSprite spriteWithSpriteFrameName:@"ConfirmPanel_Border_Bottom_2.png"];
    spriteBorderBottom3 = [CCSprite spriteWithSpriteFrameName:@"ConfirmPanel_Border_Bottom_3.png"];
    
    spriteButtonOK = [CCSprite spriteWithSpriteFrameName:@"ConfirmPanel_Button_OK.png"];
    spriteButtonCancel = [CCSprite spriteWithSpriteFrameName:@"ConfirmPanel_Button_Cancel.png"];
    
    spriteBorderBottom1.position = positionOfBorderBottom1;
    spriteBorderBottom2.position = positionOfBorderBottom2;
    spriteBorderBottom3.position = positionOfBorderBottom3;
    spriteButtonOK.position = positionOfButtonOK;
    spriteButtonCancel.position = positionOfButtonCancel;
    
    [spriteThePanel addChild:spriteBorderBottom1];
    [spriteThePanel addChild:spriteBorderBottom2];
    [spriteThePanel addChild:spriteBorderBottom3];
    [spriteThePanel addChild:spriteButtonOK];
    [spriteThePanel addChild:spriteButtonCancel];
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_Confirm])
    {
        [Event_DidConfirm unbindFromAllActions];
        [Event_DidNotConfirm unbindFromAllActions];
        
        NSString *message = [[Spellbound messageManager] messageWithTitle:action.data];
        if (!message) message = action.data;
        
        self.message = message;
        
        [self show];
        [action finish];
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        FLEventMake(Event_DidConfirm);
        FLEventMake(Event_DidNotConfirm);
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"Confirm"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"Confirm"];
    }
    return self;
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if ([self visible])
    {
        if ([FL doesSprite:spriteButtonOK containPoint:point])
        {
            [self hide:^{
                [Event_DidConfirm dispatch];
            }];
        }
        else if ([FL doesSprite:spriteButtonCancel containPoint:point] ||
                 [FL doesSprite:spriteButtonClose containPoint:point])
        {
            [self hide: ^{
                [Event_DidNotConfirm dispatch];
            }];
        }
        return YES;
    }
    return NO;
}

- (void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    [super dealloc];
}

@end
