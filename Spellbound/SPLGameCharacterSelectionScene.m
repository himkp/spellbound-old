//
//  SPLGameCharacterSelectionScreen.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLGameCharacterSelectionScene.h"
#import "YAMLSerialization.h"
#import "FLEvent.h"
#import "SPLGameScene.h"

@implementation SPLGameCharacterSelectionScene

@synthesize Event_SceneWasLoaded, Event_MaleCharacterWasTouched, 
            Event_FemaleCharacterWasTouched, Event_PlayerNameWasEntered,
            Event_FemaleCharacterWasFinalized, Event_MaleCharacterWasFinalized;

- (id)init {
    self = [super init];
    if (self) {
        
        // Create Actions and Events
        
        FLEventMake(Event_SceneWasLoaded);
        FLEventMake(Event_PlayerNameWasEntered);
        FLEventMake(Event_MaleCharacterWasTouched);
        FLEventMake(Event_FemaleCharacterWasTouched);
        FLEventMake(Event_MaleCharacterWasFinalized);
        FLEventMake(Event_FemaleCharacterWasFinalized);
        
        [FLYAML yamlWithYAMLDelegate:self];
        
        // let us load the textures
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/CharacterSelection_Background.plist" : @"iPhone/CharacterSelection_Background.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/CharacterSelection_Textures.plist" : @"iPhone/CharacterSelection_Textures.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
        
        
        // add layers
        _primaryUILayer = [SPLGameCharacterSelectionUILayer node];
        _dialoguePanelLayer = [SPLDialoguePanelLayer node];
        
        [self addChild:_primaryUILayer];
        [self addChild:_dialoguePanelLayer];
        
        [Event_SceneWasLoaded dispatchAfterTime:1];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_BeginGame])
    {
        NSLog(@"Let the game begin!");
        [[CCDirector sharedDirector] replaceScene:
         [CCTransitionFade transitionWithDuration:1 
                                            scene:[SPLGameScene node]]];
    }
    else
    {
        [action forwardToActionDelegates:_primaryUILayer,
                                         _dialoguePanelLayer,
                                        [Spellbound globalActionDelegate], nil];
    }
}

- (void)dealloc {
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/CharacterSelection_Background.plist" : @"iPhone/CharacterSelection_Background.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/CharacterSelection_Textures.plist" : @"iPhone/CharacterSelection_Textures.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:
     DEVICE_IS_IPAD ? @"iPad/OverlayPanel+DialogueWindow_Textures.plist" : @"iPhone/OverlayPanel+DialogueWindow_Textures.plist"];
    
    [[CCDirector sharedDirector] purgeCachedData];
    
    [Event_SceneWasLoaded release];
    [Event_PlayerNameWasEntered release];
    [Event_MaleCharacterWasTouched release];
    [Event_FemaleCharacterWasTouched release];
    [Event_MaleCharacterWasFinalized release];
    [Event_FemaleCharacterWasFinalized release];
    
    [super dealloc];
}

@end
