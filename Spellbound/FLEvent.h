//
//  FLEvent.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLAction.h"

#define FLEventMake(event)      \
    self.event = [FLEvent eventWithName:    \
    [@#event stringByReplacingOccurrencesOfString:@"Event_" withString:@""] andDispatcher:self] \

@interface FLEvent : NSObject
{
    @protected
    NSMutableArray *actionsToUnbindFromOnDispatch;
    
    // when the event is dispatched with dispatchWithData:,
    // and an action was bound using bindToAction:andDispatchOnlyIfDataEquals:,
    // that action will run only if the conditional data at the corresponding
    // index in the array matches with the data given in dispatchWithData:.
    NSMutableArray *conditionalActionData;
}

-(id)initWithName:(NSString*)name;
-(id)initWithName:(NSString *)name andDispatcher:(id)dispatcher;

+(id)eventWithName:(NSString*)name;
+(id)eventWithName:(NSString *)name andDispatcher:(id)dispatcher;

-(void)bindToAction:(FLAction*)action;
// todo: rename to bindToAction:andRunOnlyIfDataEquals
// (typo) actions are run, not dispatched
-(void)bindToAction:(FLAction *)action andDispatchOnlyIfDataEquals:(id)data;
-(void)bindToActionAndUnbindOnDispatch:(FLAction*) action;
-(void)bindToActionAndUnbindOnDispatch:(FLAction *)action
           andDispatchOnlyIfDataEquals:(id)data;
-(void)unbindFromAction:(FLAction*)action;
-(void)unbindFromAllActions;
-(void)dispatch;
-(void)dispatchWithData:(id)data;
-(void)dispatchAfterTime:(NSTimeInterval)seconds;

@property (nonatomic, readonly) id dispatcher;
@property (nonatomic, retain) NSString* name;
@property (nonatomic) BOOL shouldUnbindActionsOnDispatch;

@property (nonatomic, copy) BOOL (^eventDataComparator)(id dispatchedEventData, id conditionalActionData);

@property (nonatomic, copy) void (^onDispatch)(id data);

@property (nonatomic, retain) NSMutableArray* actions;

@end
