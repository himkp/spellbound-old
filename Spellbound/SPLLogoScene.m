//
//  HelloWorldLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/18/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "SPLLogoScene.h"
#import "SPLTitleScene.h"
#import "SPLGameScene.h"

#import "SimpleAudioEngine.h"
#import "CDAudioManager.h"

@implementation SPLLogoScene

+(SPLLogoScene *) scene
{
	return [SPLLogoScene node];
}

-(id) init
{
	if( (self=[super init])) 
    {
        shouldJumpDirectlyToGameScene = NO;
        shouldSkipTitleScreenLogos = YES;
        
        NSLog(@"value stored locally: %@", [[Spellbound localStorage] dataForKey:@"test"]);
        
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        
        CCLayer *layer = [CCLayer node];
        [self addChild:layer];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPHONE ? @"iPhone/LogoScreen.plist" : @"iPad/LogoScreen.plist"];
        
		CCSprite *bg = [CCSprite spriteWithSpriteFrameName:@"FleonLogo.png"];
		bg.anchorPoint = ccp(0, 0);
		bg.position = ccp(0, 0);
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        CCSprite *black = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255) width:winSize.width height:winSize.height];
        black.ignoreAnchorPointForPosition = NO;
        black.position = ccp(0, 0);
        black.anchorPoint = ccp(0, 0);
        
        CCSprite *bg2 = [CCSprite spriteWithSpriteFrameName:@"TWIPLogo.png"];
        bg2.anchorPoint = ccp(0, 0);
        bg2.position = ccp(0, 0);
        
        CCSprite *black2 = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255) width:winSize.width height:winSize.height];
        black2.ignoreAnchorPointForPosition = NO;
        black2.position = ccp(0, 0);
        black2.anchorPoint = ccp(0, 0);
        
        CCSprite *bg3 = [CCSprite spriteWithSpriteFrameName:@"Cocos2DLogo.jpg"]; // eww, get rid of the jpg
        bg3.anchorPoint = ccp(0, 0);
        bg3.position = ccp(0, 0);
		
        [layer addChild:bg3 z:0];
        [layer addChild:black2 z:1];
        [layer addChild:bg2 z:2];
        [layer addChild:black z:3];
		[layer addChild:bg z:4];
        
        if (shouldJumpDirectlyToGameScene || shouldSkipTitleScreenLogos)
        {
            [bg runAction:
             [CCSequence actions:
              [CCCallBlock actionWithBlock:^{
                 [black runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCCallBlock actionWithBlock:^{
                 [bg2 runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCCallBlock actionWithBlock:^{
                 [black2 runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCCallBlock actionWithBlock:^{
                 [self scheduleOnce:@selector(play:) delay:3 * 0];
             }],
              nil]];
        }
        else
        {
            [bg runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:3.0],
              [CCFadeOut actionWithDuration:0.5],
              [CCCallBlock actionWithBlock:^{
                 [black runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCDelayTime actionWithDuration:3.5],
              [CCCallBlock actionWithBlock:^{
                 [bg2 runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCDelayTime actionWithDuration:0.5],
              [CCCallBlock actionWithBlock:^{
                 [black2 runAction:[CCFadeOut actionWithDuration:0.5]];
             }],
              [CCDelayTime actionWithDuration:0.5],
              [CCCallBlock actionWithBlock:^{
                 [self scheduleOnce:@selector(play:) delay:3 * 0];
             }],
              nil]];
        }
        
        
		
	}
	return self;
}

- (void) play:(id) sender
{
    
    if (shouldJumpDirectlyToGameScene)
        [[CCDirector sharedDirector] replaceScene:
         [CCTransitionFade transitionWithDuration:1
                                            scene:[SPLGameScene node]]];

    else
        [[CCDirector sharedDirector] replaceScene:
         [CCTransitionFade transitionWithDuration:1
                                            scene:[SPLTitleScene node]]];
    
}

- (void) dealloc
{
	[[CCDirector sharedDirector] purgeCachedData];
    
	[super dealloc];
}
@end
