//
//  SPLCreditsLayer.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Spellbound.h"

@interface SPLOverlayPanelLayer : CCLayer <NSCoding>
{
    CCSprite *spriteBackground;
    
    CCSprite *spriteThePanel;
    
    CCSprite *spriteWhiteUnderlay;
    
    CCSprite *spriteBorderTop;
    CCSprite *spriteBorderLeft;
    CCSprite *spriteBorderRight;
    
    CCSprite *spriteButtonClose;
    CCSprite *spriteButtonPanelTitle;
    
    NSString *panelName;
    
    CGPoint positionOfBackground;
    CGPoint positionOfThePanel;
    CGPoint positionOfWhiteUnderlay;
    CGPoint positionOfBorderTop;
    CGPoint positionOfBorderLeft;
    CGPoint positionOfBorderRight;
    CGPoint positionOfButtonClose;
    CGPoint positionOfButtonPanelTitle;
}

-(void)initializePositions;

-(id)initWithPanelName:(NSString*)name;
-(void)show;
-(void)hide;
-(void) show: (void(^)()) onShow;
-(void) hide: (void(^)()) onHide;
-(void)setupBordersAndBackground;

+(id)layerWithPanelName:(NSString*)name;

@property (nonatomic) BOOL isDisabled;

@end
