//
//  FLAStarPathfinder.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FLAStarPathfinderDelegate <NSObject>

-(BOOL) isPointAnObstruction:(CGPoint)point;
-(NSSet*)validNeighbouringNodesForPoint:(CGPoint)point;

-(NSInteger) costOfNodeAtPoint:(CGPoint)point;

@end

@interface FLAStarPathfinder : NSObject

-(id)initWithPathfinderDelegate:(id<FLAStarPathfinderDelegate>)delegate;

+(id)pathfinderWithPathfinderDelegate:(id<FLAStarPathfinderDelegate>)delegate;

-(NSArray*)findPathFromSource:(CGPoint)source toDestination:(CGPoint)destination;
-(NSInteger)heuristicCostEstimateFromSource:(CGPoint)source toDestination:(CGPoint)destination;
-(NSSet*)neighbouringNodesForPoint:(CGPoint)point;
-(NSInteger)distanceBetweenPointA:(CGPoint)a andPointB:(CGPoint)b;

@property (nonatomic, retain) id <FLAStarPathfinderDelegate> delegate;

@end
