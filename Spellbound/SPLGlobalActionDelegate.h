//
//  SPLGlobalActionDelegate.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLAction.h"

@interface SPLGlobalActionDelegate : NSObject <FLActionDelegate>
+(id)globalActionDelegate;

#define Action_ConditionCheck   FLActionMake(Action_ConditionCheck)
#define Action_WaitFor          FLActionMake(Action_WaitFor)
#define Action_WaitUntil        FLActionMake(Action_WaitUntil)
#define Action_WaitUntilEither  FLActionMake(Action_WaitUntilEither)

#define Action_Goto             FLActionMake(Action_Goto)
#define Action_Play             FLActionMake(Action_Play)
#define Action_Set              FLActionMake(Action_Set)
#define Action_With             FLActionMake(Action_With)

#define Action_PlayMusic        FLActionMake(Action_PlayMusic)

#define Action_DummyAction      FLActionMake(Action_DummyAction)

@end