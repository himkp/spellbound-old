//
//  SPLDialoguePanelLayer.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 1/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPLDialoguePanelLayer.h"
#import "SPLDialogueManager.h"
#import "FLEvent.h"

@implementation SPLDialoguePanelLayer

@synthesize Event_TouchDidHappen;

-(void)initializePositions
{
    if (DEVICE_IS_IPAD)
    {
        positionOfBorderLeft    = fl_ccp(   0, 1032,  657, 504);
        positionOfBorderTop     = fl_ccp( 657, 1197, 1013, 166);
        positionOfBorderRight   = fl_ccp(1670, 1153,  376, 383);
        positionOfContinueArrow = fl_ccp(1816, 1454,   64,  66);
        
        positionOfBackground1   = fl_ccp( 120, 1280, 1820, 320);
        positionOfBackground2   = fl_ccp( 232, 1120,  324, 324);
        
        positionOfDialogueText  = fl_ccp( 660, 1348,    0,   0);
        
        positionOfAvatar        = positionOfBackground2;
        
        positionOfThePanel      = fl_ccp(   0,    0,    0,   0);
    }
    else if (DEVICE_IS_IPHONE)
    {
        positionOfBorderLeft    = fl_ccp(   0,  387,  297,  253);
        positionOfBorderTop     = fl_ccp( 297,  487,  491,   71);
        positionOfBorderRight   = fl_ccp( 788,  460,  172,  180);
        positionOfContinueArrow = fl_ccp( 849,  600,   30,   32);
        
        positionOfBackground1   = fl_ccp(  56,  520,  853,  150);
        positionOfBackground2   = fl_ccp( 108,  445,  152,  152);
        
        positionOfDialogueText  = fl_ccp( 308,  560,    0,    0);
        
        positionOfAvatar        = positionOfBackground2;
        
        positionOfThePanel      = fl_ccp(   0,    0,    0,    0);
    }
}

-(id)init
{
    if (self=[super init])
    {
        [self initializePositions];

        // init events
        FLEventMake(Event_TouchDidHappen);
        Event_TouchDidHappen.shouldUnbindActionsOnDispatch = YES;
        
        spriteAvatar = nil;
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         DEVICE_IS_IPAD ? @"iPad/CharacterAvatar_Textures.plist" : @"iPhone/CharacterAvatar_Textures.plist"];
        
        spriteBackground = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteBackground.scaleX = DEVICE_IS_IPAD ? 910 : 426;
        spriteBackground.scaleY = DEVICE_IS_IPAD ? 160 : 75;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteBackground.scaleX *= 2;
            spriteBackground.scaleY *= 2;
        }
        spriteBackground.position = positionOfBackground1;
        
        spriteBackground2 = [CCSprite spriteWithFile:@"Assets/Common/WhitePixel.png"];
        spriteBackground2.scaleX = DEVICE_IS_IPAD ? 162 : 76;
        spriteBackground2.scaleY = DEVICE_IS_IPAD ? 162 : 76;
        if (DEVICE_SUPPORTS_RETINA_DISPLAY)
        {
            spriteBackground2.scaleX *= 2;
            spriteBackground2.scaleY *= 2;
        }
        spriteBackground2.position = positionOfBackground2;
        
        self.visible = NO;
        self.isTouchEnabled = YES;
        
        spriteBorderTop = [CCSprite spriteWithSpriteFrameName:@"DialogueWindow_Border_Top.png"];
        spriteBorderRight = [CCSprite spriteWithSpriteFrameName:@"DialogueWindow_Border_Right.png"];
        spriteBorderLeft = [CCSprite spriteWithSpriteFrameName:@"DialogueWindow_Thumbnail.png"];
        spriteContinueArrow = [CCSprite spriteWithSpriteFrameName:@"DialogueWindow_Button_NextLine.png"];
        
        spriteBorderTop.position = positionOfBorderTop;
        spriteBorderRight.position = positionOfBorderRight;
        spriteBorderLeft.position = positionOfBorderLeft;
        spriteContinueArrow.position = positionOfContinueArrow;
        
        spriteContinueArrow.visible = NO;
        spriteContinueArrow.opacity = 0;
        
        [spriteContinueArrow runAction:
         [CCRepeatForever actionWithAction:
          [CCSequence actionOne:[CCMoveBy actionWithDuration:0.5 position:ccp(0, 5)]
                            two:[CCMoveBy actionWithDuration:0.5 position:ccp(0, -5)]]]];
        
        spriteThePanel = [CCSprite node];
        spriteThePanel.position = positionOfThePanel;
        
        spriteDialogueText = [CCLabelBMFont labelWithString:@"" 
                                                    fntFile:DEVICE_IS_IPAD ? @"iPad/Fonts/DoctorSoosBold.fnt"
                                                           :@"iPhone/Fonts/DoctorSoosBold.fnt"
                                                      width:DEVICE_IS_IPAD ? 550 : 257
                                                  alignment:UITextAlignmentLeft];
        spriteDialogueText.position = positionOfDialogueText;
        spriteDialogueText.anchorPoint = ccp(0, 1);
        
        [spriteThePanel addChild:spriteBackground z:-100];
        [spriteThePanel addChild:spriteBackground2 z:-101];
        [spriteThePanel addChild:spriteDialogueText];
        [spriteThePanel addChild:spriteContinueArrow];
        [spriteThePanel addChild:spriteBorderTop];
        [spriteThePanel addChild:spriteBorderRight];
        [spriteThePanel addChild:spriteBorderLeft];
        [self addChild:spriteThePanel];
        
        currentDialogueString = [[NSMutableString stringWithString:@""] retain];
        currentAvatarFilename = [[NSMutableString stringWithString:@""] retain];
        
        [[Spellbound actionDelegateManager] registerActionDelegate:self withName:@"DialogueWindow"];
        [[Spellbound eventDispatcherManager] registerEventDispatcher:self withName:@"DialogueWindow"];
    }
    return self;
}

-(void)handleAction:(FLAction *)action
{
    if ([action is:Action_HideDialogueWindow])
    {
        [self hide: ^ {
            [action finish];
        }];
    }
    else if ([action is:Action_ShowDialogueWindow])
    {
        [self show: ^ {
            [action finish];
        }];
    }
    else if ([action is:Action_BeginDialogue])
    {
        // Break down our action into a lot of actions, and
        // link the last action to the subsequent action
        
        FLAction* subsequentAction = [action.subsequentAction retain];
        
        SPLDialogueSequence* dialogueSequence = [[Spellbound dialogueManager] dialogueSequenceWithTitle:action.data];
        
        FLAction* previousAction = action, *thisAction = nil;
        
        for (SPLDialogue* dialogue in dialogueSequence.dialogues)
        {
            NSString* avatarFile = [dialogue.speaker avatarFilenameForMood:dialogue.currentMoodOfSpeaker];
            thisAction = Action_SetCharacterSprite;
            thisAction.data = avatarFile;
            
            previousAction.subsequentAction = thisAction;
            previousAction = thisAction;
            
            NSArray* dialogueLines = [dialogue.text componentsSeparatedByString:@"\n"];
            
            NSUInteger rangeLength = dialogueLines.count < 2 ? 1 : 2;
            NSArray* firstTwoLines = [dialogueLines objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, rangeLength)]];
            
            thisAction = Action_DisplayFirstTwoLines;
            thisAction.data = firstTwoLines;
            
            previousAction.subsequentAction = thisAction;
            previousAction = thisAction;
            
            for (NSUInteger i = 2, l = dialogueLines.count; i < l; i++)
            {
                thisAction = Action_DisplayNextLine;
                thisAction.data = (NSString*) dialogueLines[i];
                
                previousAction.subsequentAction = thisAction;
                previousAction = thisAction;
            }
        }
        
        previousAction.subsequentAction = subsequentAction;
        
        [subsequentAction release];
        
        [action finish];
    }
    else if ([action is:Action_DisplayFirstTwoLines])
    {
        currentAction = action;
        [currentDialogueString setString:@""];
        
        [self schedule:@selector(typewriterEffectForAction_DisplayFirstTwoLines:) interval:0.020];
        
        NSLog(@"%@", action.subsequentAction.name);
        NSLog(@"%@", action.subsequentAction.subsequentAction.name);
    }
    else if ([action is:Action_DisplayNextLine])
    {
        currentAction = action;
        [spriteDialogueText runAction:[CCMoveBy actionWithDuration:0.15 position:ccp(0, DEVICE_IS_IPAD ? 50 : 23)]];
        
        [self scheduleOnce:@selector(scrollEffectForAction_DisplayNextLine:) delay:0.15];
    }
    else if ([action is:Action_SetCharacterSprite])
    {
        if ([currentAvatarFilename isEqualToString:action.data]) {
            [action finish];
            return;
        }
        if (spriteAvatar)
            [spriteThePanel removeChild:spriteAvatar cleanup:YES];
        
        spriteAvatar = [CCSprite spriteWithSpriteFrameName:action.data];
        spriteAvatar.position = positionOfAvatar;
        [currentAvatarFilename setString:action.data];
        
        [spriteAvatar runAction:[CCFadeIn actionWithDuration:0.25]];
        
        [spriteThePanel addChild:spriteAvatar z:-20];
        
        [action finish];
    }
}

-(void)scrollEffectForAction_DisplayNextLine:(ccTime)dt
{
    NSString* newDialogueString = [currentDialogueString substringFromIndex:
                                   [currentDialogueString rangeOfString:@"\n"].location + 1];
    [currentDialogueString setString:newDialogueString];
    [currentDialogueString appendString:@"\n"];
    
    spriteDialogueText.position = ccp(spriteDialogueText.position.x, spriteDialogueText.position.y - (DEVICE_IS_IPAD ? 50 : 23));
    spriteDialogueText.string = currentDialogueString;
    
    [self schedule:@selector(typewriterEffectForAction_DisplayNextLine:) interval:0.020];
}

-(void)typewriterEffectForAction_DisplayNextLine:(ccTime)dt
{
    NSUInteger typingSpeed = 1;
    NSArray* dialogueLines = [currentDialogueString componentsSeparatedByString:@"\n"];
    NSString* currentLine = [currentAction data];
    
    if ([currentLine isEqualToString:dialogueLines[1]])
    {
        [self unschedule:@selector(typewriterEffectForAction_DisplayNextLine:)];
        
        // show the arrow (wait) only if the next action is a dialogue action,
        // else just run the next action directly
        if (currentAction.subsequentAction && [self canHandleAction:currentAction.subsequentAction])
        {
            spriteContinueArrow.visible = YES;
            [spriteContinueArrow runAction:[CCFadeIn actionWithDuration:0.10]];
            [Event_TouchDidHappen bindToAction:currentAction.subsequentAction];
        }
        else [currentAction finish];
    
        return;
    }
    
    NSUInteger remainingCharCount = currentLine.length - ((NSString*)dialogueLines[1]).length;
    NSRange range = NSMakeRange(currentDialogueString.length - ((NSString *) dialogueLines[0]).length - 1, min(typingSpeed, remainingCharCount));
    NSString *stringToAppend = [currentLine substringWithRange:range];
    
    if ([stringToAppend rangeOfString:@"|"].location != NSNotFound)
    {
        // if a pipe is encountered
        // pause the current action and wait for input to resume it
        stringToAppend = [stringToAppend componentsSeparatedByString:@"|"][0];
        
        [self unschedule:@selector(typewriterEffectForAction_DisplayNextLine:)];
        
        spriteContinueArrow.visible = YES;
        [spriteContinueArrow runAction:[CCFadeIn actionWithDuration:0.10]];
        
        isCurrentActionPaused = YES;
        
        NSUInteger indexOfPipe = [currentLine rangeOfString:@"|"].location;
        
        currentLine = [[currentLine substringToIndex:indexOfPipe] stringByAppendingString:[currentLine substringFromIndex:indexOfPipe + 1]];
        currentAction.data = currentLine;
    }
    
    [currentDialogueString appendString:stringToAppend];
    
    spriteDialogueText.string = currentDialogueString;
}

-(void)typewriterEffectForAction_DisplayFirstTwoLines:(ccTime)dt
{
    NSUInteger typingSpeed = 1;
    NSString* currentDialogue = [[currentAction data] componentsJoinedByString:@"\n"];
    
    if ([currentDialogue isEqualToString:currentDialogueString])
    {
        [self unschedule:@selector(typewriterEffectForAction_DisplayFirstTwoLines:)];
        
        // show the arrow (wait) only if the next action is a dialogue action,
        // else just run the next action directly
        if (currentAction.subsequentAction && [self canHandleAction:currentAction.subsequentAction])
        {
            spriteContinueArrow.visible = YES;
            [spriteContinueArrow runAction:[CCFadeIn actionWithDuration:0.10]];
            [Event_TouchDidHappen bindToAction:currentAction.subsequentAction];
        }
        else [currentAction finish];
        
        return;
    }
    
    NSUInteger remainingCharCount = currentDialogue.length - currentDialogueString.length;
    NSRange range = NSMakeRange(currentDialogueString.length, min(typingSpeed, remainingCharCount));
    NSString *stringToAppend = [currentDialogue substringWithRange:range];
    
    if ([stringToAppend rangeOfString:@"|"].location != NSNotFound)
    {
        // if a pipe is encountered
        // pause the current action and wait for input to resume it
        stringToAppend = [stringToAppend componentsSeparatedByString:@"|"][0];
        
        [self unschedule:@selector(typewriterEffectForAction_DisplayFirstTwoLines:)];
        
        spriteContinueArrow.visible = YES;
        [spriteContinueArrow runAction:[CCFadeIn actionWithDuration:0.10]];
        
        isCurrentActionPaused = YES;
        
        NSUInteger indexOfPipe = [currentDialogue rangeOfString:@"|"].location;
        
        currentDialogue = [[currentDialogue substringToIndex:indexOfPipe] stringByAppendingString:[currentDialogue substringFromIndex:indexOfPipe + 1]];
        currentAction.data = [currentDialogue componentsSeparatedByString:@"\n"];
    }
    
    [currentDialogueString appendString:stringToAppend];
    
    spriteDialogueText.string = currentDialogueString;
}

-(BOOL)canHandleAction:(FLAction *)action
{
    return [action is:Action_ShowDialogueWindow] ||
           [action is:Action_HideDialogueWindow] ||
           [action is:Action_BeginDialogue] ||
           [action is:Action_SetCharacterSprite] ||
           [action is:Action_DisplayFirstTwoLines] ||
           [action is:Action_DisplayNextLine];
}

-(void)registerWithTouchDispatcher
{
    CCTouchDispatcher *dispatcher = [CCDirector sharedDirector].touchDispatcher;
    [dispatcher addTargetedDelegate:self priority:10 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:[touch view]];
    point = [[CCDirector sharedDirector] convertToGL:point];
    
    if ([self visible])
    {
        if (Event_TouchDidHappen.actions.count)
        {
            [spriteContinueArrow runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.10] two:[CCHide action]]];
            
            [Event_TouchDidHappen dispatch];
            return YES;
        }
        
        if (isCurrentActionPaused)
        {
            if ([currentAction is:Action_DisplayFirstTwoLines])
                [self schedule:@selector(typewriterEffectForAction_DisplayFirstTwoLines:) interval:0.020];
            else [self schedule:@selector(typewriterEffectForAction_DisplayNextLine:) interval:0.020];
            
            [spriteContinueArrow runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.10] two:[CCHide action]]];
            
            isCurrentActionPaused = NO;
            return YES;
        }
    }
    return NO;
}

-(void)show
{
    [self show: ^ {}];
}

-(void)show:(void(^)())onShow
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    self.visible = YES;
    spriteContinueArrow.visible = NO;
    if (spriteAvatar)
        [spriteThePanel removeChild:spriteAvatar cleanup:YES];
    spriteDialogueText.string = @"";
    
    [FL with:spriteThePanel setPosition:ccp(0, winSize.height + spriteBorderRight.boundingBox.size.height)];
    [spriteThePanel runAction:
     [CCEaseExponentialOut actionWithAction:
      [CCSequence actionOne:[FLMoveTo actionWithDuration:0.5 position:ccp(0, winSize.height)]
                        two:[CCCallBlock actionWithBlock:onShow]]]];
}

-(void)hide
{
    [self hide: ^ {}];
}

-(void)hide:(void(^)())onHide
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    [currentAvatarFilename setString:@""];
    [spriteThePanel runAction:
     [CCEaseExponentialIn actionWithAction:
      [FLMoveTo actionWithDuration:0.5 position:ccp(0, winSize.height + spriteBorderRight.boundingBox.size.height)]]];
    [self runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:0.5],
      [CCHide action],
      [CCCallBlock actionWithBlock:onHide],
      nil]];
}

-(void)dealloc
{
    [[Spellbound actionDelegateManager] unregisterActionDelegate:self];
    [[Spellbound eventDispatcherManager] unregisterEventDispatcher:self];
    
    // release events
    [Event_TouchDidHappen release];
    
    // other objects
    [currentAvatarFilename release];
    [currentDialogueString release];
    
    [super dealloc];
}

@end
