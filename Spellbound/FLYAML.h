//
//  FLYAML.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLEvent.h"

@protocol FLYAMLDelegate <FLActionDelegate>
@end

@interface FLYAML : NSObject

@property (nonatomic, readonly) id<FLYAMLDelegate> yamlDelegate;
@property (nonatomic, readonly) id rootYAMLNode;

-(id)initWithYAMLDelegate:(id<FLYAMLDelegate>)yamlDelegate;
-(id)yamlNodeWithName:(NSString*)nodeName;
-(id)yamlNodeWithName:(NSString*)nodeName andChildOf:(id)yamlNode;

-(void)extractDialogueSpeakers;
-(void)extractDialogueSequences;
-(void)extractActionSequences;
-(void)extractEventActionBindings;
-(void)extractVariables;
-(void)extractMessages;

+(FLAction *)linkActionSequence:(id)yamlActionSequence
             withActionDelegate:(id<FLActionDelegate>)actionDelegate;

+(id)yamlWithYAMLDelegate:(id<FLYAMLDelegate>)yamlDelegate;

+(BOOL)isNodeNSDictionary:(id)yamlNode;
+(BOOL)isNodeNSArray:(id)yamlNode;
+(BOOL)isNodeNSString:(id)yamlNode;

@end