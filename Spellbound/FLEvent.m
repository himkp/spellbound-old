//
//  FLEvent.m
//  Spellbound
//
//  Created by Himanshu Kapoor on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FLEvent.h"
#import "FLEventAggregator.h"

@implementation FLEvent

-(id)init
{
    if (self = [super init])
    {
        _onDispatch = ^(id data){};
        
        _actions = [[NSMutableArray alloc] init];
        actionsToUnbindFromOnDispatch = [[NSMutableArray alloc] init];
        conditionalActionData = [[NSMutableArray alloc] init];
        
        _eventDataComparator = ^BOOL (id dispatchedEventData, id conditionalActionData_) {
            return [dispatchedEventData isEqual:conditionalActionData_];
        };
    }
    return self;
}

-(id)initWithName:(NSString *)name
{
    if (self = [self init])
        self.name = name;
    return self;
}

-(id)initWithName:(NSString *)name andDispatcher:(id)dispatcher
{
    if (self = [self initWithName:name])
    {
        _dispatcher = dispatcher;
        [[FLEventAggregator globalEventAggregator] addEvent:self];
    }
    return self;
}

+(id)eventWithName:(NSString *)name
{
    return [[[self alloc] initWithName:name] autorelease];
}

+(id)eventWithName:(NSString *)name andDispatcher:(id)dispatcher
{
    return [[[self alloc] initWithName:name andDispatcher:dispatcher] autorelease];
}

-(void)bindToAction:(FLAction *)action
{
    if (action)
        [_actions addObject:action];
}

-(void)bindToAction:(FLAction *)action andDispatchOnlyIfDataEquals:(id)data
{
    if (action)
    {
        [self bindToAction:action];
        if (data) [conditionalActionData addObject:data];
    }
}

-(void)bindToActionAndUnbindOnDispatch:(FLAction *)action
{
    if (action)
    {
        [self bindToAction:action];
        [actionsToUnbindFromOnDispatch addObject:action];
    }
}

-(void)bindToActionAndUnbindOnDispatch:(FLAction *)action andDispatchOnlyIfDataEquals:(id)data
{
    if (action)
    {
        [self bindToActionAndUnbindOnDispatch:action];
        if (data) [conditionalActionData addObject:data];
    }
}

-(void)unbindFromAction:(FLAction *)action
{
    if (action && [_actions containsObject:action])
    {
        NSUInteger i = [_actions indexOfObject:action];
        [_actions removeObject:action];
        if (i < conditionalActionData.count)
            [conditionalActionData removeObjectAtIndex:i];
        [actionsToUnbindFromOnDispatch removeObject:action];
    }
}

-(void)unbindFromAllActions
{
    NSArray *actions = [NSArray arrayWithArray:_actions];
    for (int i = 0; i < actions.count; i++)
        [self unbindFromAction:(FLAction*) actions[i]];
}

-(void)dispatch
{   
    for (int i = 0; i < _actions.count; i++) {
        FLAction *action = (FLAction *) _actions[i];
        [action run];
    }
    
    for (int i = 0; i < _actions.count; i++) {
        FLAction *action = (FLAction *) _actions[i];
        if ([actionsToUnbindFromOnDispatch containsObject:action])
            [self unbindFromAction:action];
    }
    
    if (_shouldUnbindActionsOnDispatch)
        [self unbindFromAllActions];
    
    _onDispatch(nil);
}

-(void)dispatchWithData:(id)data
{
    BOOL (^shouldActionRun) (FLAction *, NSUInteger) = ^(FLAction *action, NSUInteger i) {
        return (BOOL) (i < conditionalActionData.count
            && conditionalActionData[i]
            && _eventDataComparator(data, conditionalActionData[i]));
    };
    
    for (int i = 0; i < _actions.count; i++)
    {
        FLAction *action = (FLAction *) _actions[i];
        if (shouldActionRun(action, i))
            [action run];
    }
    
    for (int i = 0; i < _actions.count; i++)
    {
        FLAction *action = (FLAction *) _actions[i];
        if (shouldActionRun(action, i) && [actionsToUnbindFromOnDispatch containsObject:action])
            [self unbindFromAction:action];
    }
    
    if (_shouldUnbindActionsOnDispatch)
        [self unbindFromAllActions];
    
    _onDispatch(data);
}

-(void)dispatchAfterTime:(NSTimeInterval)seconds
{
    [self performSelector:@selector(dispatch) withObject:self afterDelay:seconds];
}

-(void)dealloc
{
    [_onDispatch release];
    [self unbindFromAllActions];
    [_actions release];
    [_name release];
    [conditionalActionData release];
    [actionsToUnbindFromOnDispatch release];
    [super dealloc];
}

@end