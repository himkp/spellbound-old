//
//  FLGlobal.h
//  Spellbound
//
//  Created by Himanshu Kapoor on 12/20/
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "FLActions.h"


#ifdef UI_USER_INTERFACE_IDIOM
    #define DEVICE_IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    #define DEVICE_IS_IPHONE (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
#else
    #define DEVICE_IS_IPAD (false)
    #define DEVICE_IS_IPHONE (true)
#endif

#define DEVICE_SUPPORTS_RETINA_DISPLAY CC_CONTENT_SCALE_FACTOR() == 2

#define FLGetGraphicsFile(filename)                                     \
    [NSString stringWithFormat:@"Graphics/%@%@/%@",                     \
              DEVICE_IS_IPAD ? @"iPad" : @"iPhone",                     \
              DEVICE_SUPPORTS_RETINA_DISPLAY ? @"Retina" : @"Regular",  \
              filename]

#define FLAddSpriteFramesFromPlist(filename)                            \
    [[CCSpriteFrameCache sharedSpriteFrameCache]                        \
      addSpriteFramesWithFile:FLGetGraphicsFile(filename)]

#define FLRemoveSpriteFramesFromPlist(filename)                         \
    [CCSpriteFrameCache sharedSpriteFrameCache]                         \
      removeSpriteFramesFromFile:FLGetGraphicsFile(filename)]

#define TESTFLIGHT_APP_TOKEN @"af2be688-7fb2-4e22-9f7a-6cfc9565eb95"

typedef enum
{
    FLNo = NO,
    FLYes = YES,
    FLMaybe = -1
} FLUncertainBoolean;

typedef enum {
    kFLAnimationActionGrowAndShrink = 0,
    kFLAnimationActionShrinkAndGrow,
    kFLAnimationActionShrink,
    kFLAnimationActionGrow,
    kFLAnimationActionRestoreSize
} FLAnimationAction;

typedef enum {
    kFLAnchorPointTopLeft = 0,
    kFLAnchorPointBottomLeft,
    kFLAnchorPointBottomRight,
    kFLAnchorPointTopRight,
    kFLAnchorPointCenter,
    kFLAnchorPointTopCenter,
    kFLAnchorPointBottomCenter,
    kFLAnchorPointCenterRight,
    kFLAnchorPointCenterLeft
} FLAnchorPoint;

typedef enum {
    kFLScreenCornerTopLeft = 0,
    kFLScreenCornerBottomLeft,
    kFLScreenCornerBottomRight,
    kFLScreenCornerTopRight
} FLScreenCorner;

@interface FL : NSObject

+(CGPoint) translatePoint:(CGPoint)point relativeTo:(FLScreenCorner)corner;
+(CGPoint) translatePoint:(CGPoint)point;

+(BOOL) doesSprite:(CCNode*)sp containPoint:(CGPoint)pt;

+(void) with:(CCNode*)sprite runAnimationAction:(FLAnimationAction)action;
+(void) with:(CCNode*) sprite setPosition: (CGPoint) position andAnchorPoint: (FLAnchorPoint) anchorPoint fromCorner:(FLScreenCorner)corner;
+(void) with:(CCNode*)sprite setPosition:(CGPoint)position andAnchorPoint:(FLAnchorPoint)anchorPoint;
+(void) with:(CCNode*)sprite setPosition:(CGPoint)position;
+(void) with:(CCNode*)sprite adjustAnchorPoint:(CGPoint)point;
+(void) with:(CCNode*)sprite adjustAnchorPointInPoints:(CGPoint)point;
@end

@interface NSString (fl_substring_search)
- (unsigned) countOccurencesOf: (NSString *)subString;
- (unsigned) indexOf: (NSString*)needle;
@end
